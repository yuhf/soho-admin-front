#!/bin/bash

script_dir=$(dirname "$0")
cd $script_dir
cd ../../

DEMO_DOCKER='docker -H ssh://root@43.153.34.211'

# 修改配置文件；配置接口地址
echo 'API_PREFIX=https://api.demo.soho.work' > .env.local
npm run build
$DEMO_DOCKER stop soho-admin-front
$DEMO_DOCKER rm soho-admin-front
$DEMO_DOCKER build -t soho-admin-front .
$DEMO_DOCKER run -d --name soho-admin-front -p 82:80 soho-admin-front
# 恢复本地配置
echo 'API_PREFIX=http://localhost:6677' > .env.local
