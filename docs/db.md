根据DB生成页面
============

字段注释举例：

    支持客户端类型;1: web,2: wap,3:app,4:微信公众号,5:微信小程序;frontType:select

规则

- 以英文状态“;”为隔断
- 第一段为字段名
- 第二段为选项（可为空； 选项为kv形式，以 ":" 分隔）
- 第三段为扩展数据； frontType 前端编辑页展示组件类型，可选 select,checkbox
- 扩展数据中 min 输入最小值
- 扩展数据中 max 输入最大值
- 字段默认值；会提现在表单新建
- 字段是否为空； 会提现在表单是否强制输入
- [Mysql数据类型](https://www.cnblogs.com/pikatao/p/14226783.html)

example:

- 外表下拉框关联
![img.png](img.png)


    //下拉框关联外表； 对应的外表为 pay_info, key: id, value: title
    支付方式ID;;frontType:select,foreign:pay_info.id~title

