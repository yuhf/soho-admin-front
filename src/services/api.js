// let file = require("../utils/file")
// import file from '../utils/file'

//let prefix="http://192.168.0.7:6677"
// let prefix="http://127.0.0.1:6677"
let prefix="https://api.demo.soho.work"
if(process.env.API_PREFIX) {
  prefix=process.env.API_PREFIX
}

console.log("============================================")
// file.scanDir("services/apis")

// eslint-disable-next-line import/no-anonymous-default-export
export default {
  // queryRouteList: `/api/v1/routes`,
  queryRouteList: `GET ${prefix}/admin/adminResource/routes`,  //获取菜单接口
  queryUserInfo: `${prefix}/admin/adminUser/user`,
  logoutUser: `GET ${prefix}/logout`,
  loginUser: `POST ${prefix}/login`,
  loginConfig: `GET ${prefix}/login/config`, //获取登陆相关配置

  //首页查询相关接口
  queryDashboardNumberCard: `GET ${prefix}/admin/adminDashboard/index`,

  //管理用户相关接口
  loginCaptcha: `${prefix}/captcha`, //获取验证码
  queryUser: `GET ${prefix}/admin/adminUser`, //查询用户详情
  queryUserOptions: `GET ${prefix}/admin/adminUser/userOptions`,
  queryAdminUserOptions: `GET ${prefix}/admin/adminUser/userOptions`,
  queryUserDetails: `GET ${prefix}/admin/adminUser`,
  querySelfUserDetails: `GET ${prefix}/admin/adminUser/myself`,
  updateSelfUser: `PUT ${prefix}/admin/adminUser/myself`,
  queryUserList: `GET ${prefix}/admin/adminUser/list`,
  updateUser: `PUT ${prefix}/admin/adminUser`,
  createUser: `POST ${prefix}/admin/adminUser`,
  createUserAvatar: `${prefix}/admin/adminUser/upload-avatar`,
  removeUser: `DELETE ${prefix}/admin/adminUser/:id`,
  removeUserList: `POST /api/v1/users/delete`,

  queryPostList: '/api/v1/posts',
  queryDashboard: '/api/v1/dashboard',

  //管理用户登录相关api信息
  queryAdminUserLoginLogList: `GET ${prefix}/admin/adminUserLoginLog/list`,
  updateAdminUserLoginLog: `PUT ${prefix}/admin/adminUserLoginLog`,
  createAdminUserLoginLog: `POST ${prefix}/admin/adminUserLoginLog`,
  deleteAdminUserLoginLog: `DELETE ${prefix}/admin/adminUserLoginLog/:ids`,
  queryAdminUserLoginLogDetails: `GET ${prefix}/admin/adminUserLoginLog/:id`,

  //角色相关接口
  queryRoleList: `GET ${prefix}/admin/adminRole/list`,
  queryRoleOptions: `GET ${prefix}/admin/adminRole/option-list`,
  removeRole: `DELETE ${prefix}/admin/adminRole/:id`,
  updateRole: `PUT ${prefix}/admin/adminRole`,
  createRole: `POST ${prefix}/admin/adminRole`,

  //资源接口
  queryResourceTree: `GET ${prefix}/admin/adminResource/tree`,
  queryResourceDetails: `GET ${prefix}/admin/adminResource`,
  queryRoleResourceIds: `GET ${prefix}/admin/adminRole/roleResources`,
  updateResource: `PUT ${prefix}/admin/adminResource`,
  createResource: `POST ${prefix}/admin/adminResource`,
  deleteResource: `DELETE ${prefix}/admin/adminResource`,
  syncResource: `GET ${prefix}/admin/adminResource/admin-resource/sync`,

  //配置文件相关接口
  createConfig: `POST ${prefix}/admin/adminConfig`,
  updateConfig: `PUT ${prefix}/admin/adminConfig`,
  queryConfig: `GET ${prefix}/admin/adminConfig/:id`,
  deleteConfig: `DELETE ${prefix}/admin/adminConfig/:id`,
  queryConfigList: `GET ${prefix}/admin/adminConfig/list`,
  queryConfigAdminCommon: `GET ${prefix}/admin/adminConfig/common`,
  queryConfigGroupList: `GET ${prefix}/admin/adminConfigGroup/list`,

  //系统通知相关接口
  listNotification: `GET ${prefix}/admin/adminNotification/list`,
  createNotification: `POST ${prefix}/admin/adminNotification`,
  updateNotification: `PUT ${prefix}/admin/adminNotification`,
  deleteNotification: `DELETE ${prefix}/admin/adminNotification/:ids`,
  myNotification: `GET ${prefix}/admin/adminNotification/myNotification`,
  readNotifications: `GET ${prefix}/admin/adminNotification/read/:ids`,
  readAllNotifications: `GET ${prefix}/admin/adminNotification/readAll`,

  //短信模板
  queryAdminSmsTemplateList: `GET ${prefix}/admin/adminSmsTemplate/list`,
  updateAdminSmsTemplate: `PUT ${prefix}/admin/adminSmsTemplate`,
  createAdminSmsTemplate: `POST ${prefix}/admin/adminSmsTemplate`,
  deleteAdminSmsTemplate: `DELETE ${prefix}/admin/adminSmsTemplate/:ids`,
  queryAdminSmsTemplateDetails: `GET ${prefix}/admin/adminSmsTemplate/:id`,
  //email模板
  queryAdminEmailTemplateList: `GET ${prefix}/admin/adminEmailTemplate/list`,
  updateAdminEmailTemplate: `PUT ${prefix}/admin/adminEmailTemplate`,
  createAdminEmailTemplate: `POST ${prefix}/admin/adminEmailTemplate`,
  deleteAdminEmailTemplate: `DELETE ${prefix}/admin/adminEmailTemplate/:ids`,
  queryAdminEmailTemplateDetails: `GET ${prefix}/admin/adminEmailTemplate/:id`,
  //发版相关接口
  queryAdminReleaseList: `GET ${prefix}/admin/adminRelease/list`,
  updateAdminRelease: `PUT ${prefix}/admin/adminRelease`,
  createAdminRelease: `POST ${prefix}/admin/adminRelease`,
  createAdminReleaseFile: `${prefix}/admin/adminRelease/upload-file`, //上传版本文件
  deleteAdminRelease: `DELETE ${prefix}/admin/adminRelease/:ids`,
  queryAdminReleaseDetails: `GET ${prefix}/admin/adminRelease/:id`,
  queryPlatformTypeEnumOption: `GET ${prefix}/admin/adminRelease/queryPlatformTypeEnumOption`, //获取枚举选项

  //审批流相关接口
  queryApprovalProcessList: `GET ${prefix}/admin/approvalProcess/list`,
  queryApprovalProcessOptions: `GET ${prefix}/admin/approvalProcess/options`,
  queryApprovalProcessDetails: `GET ${prefix}/admin/approvalProcess/:id`,
  updateApprovalProcess: `PUT ${prefix}/admin/approvalProcess`,
  createApprovalProcess: `POST ${prefix}/admin/approvalProcess`,
  deleteApprovalProcess: `DELETE ${prefix}/admin/approvalProcess/:ids`,

  queryApprovalProcessOrderList: `GET ${prefix}/admin/approvalProcessOrder/list`,
  updateApprovalProcessOrder: `GET ${prefix}/admin/approvalProcessOrder`,
  createApprovalProcessOrder: `POST ${prefix}/admin/approvalProcessOrder`,
  deleteApprovalProcessOrder: `GET ${prefix}/admin/approvalProcessOrder`,

  queryApprovalProcessMyOrderList: `GET ${prefix}/admin/approvalProcessOrder/myList`,
  updateApprovalProcessMyOrderApproval: `POST ${prefix}/admin/approvalProcessOrder/approval`,

  //计划任务
  queryAdminJobList: `GET ${prefix}/admin/adminJob/list`,
  updateAdminJob: `PUT ${prefix}/admin/adminJob`,
  createAdminJob: `POST ${prefix}/admin/adminJob`,
  deleteAdminJob: `DELETE ${prefix}/admin/adminJob/:ids`,
  runAdminJob: `GET ${prefix}/admin/adminJob/run/:id`,

  //内容模块接口
  queryAdminContentList: `GET ${prefix}/admin/adminContent/list`,
  updateAdminContent: `PUT ${prefix}/admin/adminContent`,
  createAdminContent: `POST ${prefix}/admin/adminContent`,
  createAdminContentImage: `${prefix}/admin/adminContent/upload`,
  deleteAdminContent: `DELETE ${prefix}/admin/adminContent/:ids`,
  //内容模块分类接口
  queryAdminContentCategoryList: `GET ${prefix}/admin/adminContentCategory/list`,
  queryAdminContentCategoryDetails: `GET ${prefix}/admin/adminContentCategory`,
  queryAdminContentCategoryOptions: `GET ${prefix}/admin/adminContentCategory/options`,
  updateAdminContentCategory: `PUT ${prefix}/admin/adminContentCategory`,
  createAdminContentCategory: `POST ${prefix}/admin/adminContentCategory`,
  deleteAdminContentCategory: `DELETE ${prefix}/admin/adminContentCategory/:ids`,
  queryAdminContentCategoryTree: `GET ${prefix}/admin/adminContentCategory/tree`,

  //老板相关api
  queryTlRobamUserList: `GET ${prefix}/admin/tlRobamUser/list`,
  queryTlRobamUserListOption: `GET ${prefix}/admin/tlRobamUser/list-option`,
  createTlRobamUser: `POST ${prefix}/admin/tlRobamUser`,
  updateTlRobamUser: `PUT ${prefix}/admin/tlRobamUser`,
  deleteTlRobamUser: `DELETE ${prefix}/admin/tlRobamUser/:ids`,

  queryTlDevMoniterList: `GET ${prefix}/admin/tlDevMoniter/list`,
  queryTlDevMoniterErrorsList: `GET ${prefix}/admin/tlDevMoniter/errors-list`,

  queryTlDictDevList: `GET ${prefix}/admin/tlDictDev/list`,
  createTlDictDev: `POST ${prefix}/admin/tlDictDev`,
  updateTlDictDev: `PUT ${prefix}/admin/tlDictDev`,
  deleteTlDictDev: `DELETE ${prefix}/admin/tlDictDev/:ids`,

  queryTlConsumablesList: `GET ${prefix}/admin/tlConsumables/list`,

  queryTlBatchPrnList: `GET ${prefix}/admin/tlBatchPrn/list`,
  createTlBatchPrnList: `POST ${prefix}/admin/tlBatchPrn/addList`,

  //lot
  queryLotModelList: `GET ${prefix}/admin/lotModel/list`,
  queryLotModelOptions: `GET ${prefix}/admin/lotModel/options`,
  queryLotModelDetails: `GET ${prefix}/admin/lotModel/:id`,
  createLotModel: `POST ${prefix}/admin/lotModel`,
  updateLotModel: `PUT ${prefix}/admin/lotModel`,
  deleteLotModel: `DELETE ${prefix}/admin/lotModel/:ids`,

  queryLotProductList: `GET ${prefix}/admin/lotProduct/list`,
  queryLotProductDetails: `GET ${prefix}/admin/lotProduct/:id`,
  createLotProduct: `POST ${prefix}/admin/lotProduct`,
  updateLotProduct: `PUT ${prefix}/admin/lotProduct`,
  deleteLotProduct: `DELETE ${prefix}/admin/lotProduct/:ids`,

  //pay info 相关api信息
  queryPayInfoList: `GET ${prefix}/admin/payInfo/list`,
  queryPayInfoOptions: `GET ${prefix}/admin/payInfo/options`,
  updatePayInfo: `PUT ${prefix}/admin/payInfo`,
  createPayInfo: `POST ${prefix}/admin/payInfo`,
  deletePayInfo: `DELETE ${prefix}/admin/payInfo/:ids`,
  queryPayInfoDetails: `GET ${prefix}/admin/payInfo/:id`,

  //pay order 相关api信息
  queryPayOrderList: `GET ${prefix}/admin/payOrder/list`,
  updatePayOrder: `PUT ${prefix}/admin/payOrder`,
  createPayOrder: `POST ${prefix}/admin/payOrder`,
  deletePayOrder: `DELETE ${prefix}/admin/payOrder/:ids`,
  queryPayOrderDetails: `GET ${prefix}/admin/payOrder/:id`,

  //example
  queryExampleList: `GET ${prefix}/admin/example/list`,
  updateExample: `PUT ${prefix}/admin/example`,
  applyExample: `PUT ${prefix}/admin/example/apply`,
  createExample: `POST ${prefix}/admin/example`,
  deleteExample: `DELETE ${prefix}/admin/example/:ids`,
  queryExampleDetails: `GET ${prefix}/admin/example/:id`,

  queryExampleCategoryList: `GET ${prefix}/admin/exampleCategory/list`,
  //queryPayInfoOptions: `GET ${prefix}/admin/exampleCategory/options`,
  queryExampleCategoryTree: `GET ${prefix}/admin/exampleCategory/tree`,
  updateExampleCategory: `PUT ${prefix}/admin/exampleCategory`,
  createExampleCategory: `POST ${prefix}/admin/exampleCategory`,
  deleteExampleCategory: `DELETE ${prefix}/admin/exampleCategory/:ids`,
  queryExampleCategoryDetails: `GET ${prefix}/admin/exampleCategory/:id`,

  queryExampleOptionList: `GET ${prefix}/admin/exampleOption/list`,
  updateExampleOption: `PUT ${prefix}/admin/exampleOption`,
  createExampleOption: `POST ${prefix}/admin/exampleOption`,
  deleteExampleOption: `DELETE ${prefix}/admin/exampleOption/:ids`,
  queryExampleOptionDetails: `GET ${prefix}/admin/exampleOption/:id`,
  queryExampleOptionOptions: `GET ${prefix}/admin/exampleOption/options`,

  //用户信息相关api信息
  queryUserInfoList: `GET ${prefix}/admin/userInfo/list`,
  updateUserInfo: `PUT ${prefix}/admin/userInfo`,
  createUserInfo: `POST ${prefix}/admin/userInfo`,
  deleteUserInfo: `DELETE ${prefix}/admin/userInfo/:ids`,
  queryUserInfoDetails: `GET ${prefix}/admin/userInfo/:id`,

  //自动化相关接口
  queryCodeTableList: `GET ${prefix}/admin/codeTable/list`,
  queryCodeTableOptions: `GET ${prefix}/admin/codeTable/options`,
  queryCodeTableDbTables: `GET ${prefix}/admin/codeTable/getDbTables`,
  queryCodeTableSql: `GET ${prefix}/admin/codeTable/sql`,
  queryCodeTableExec: `GET ${prefix}/admin/codeTable/exec`,
  queryCodeTableCodeFile: `GET ${prefix}/admin/codeTable/codeFile`,
  queryCodeTableSaveCode: `POST ${prefix}/admin/codeTable/saveCode`,
  queryCodeTableCodes: `POST ${prefix}/admin/codeTable/codes`,
  queryCodeTableSaveTables: `GET ${prefix}/admin/codeTable/saveTables`, //保存从数据库导入数据结构
  updateCodeTable: `PUT ${prefix}/admin/codeTable`,
  createCodeTable: `POST ${prefix}/admin/codeTable`,
  deleteCodeTable: `DELETE ${prefix}/admin/codeTable/:ids`,
  queryCodeTableDetails: `GET ${prefix}/admin/codeTable/:id`,

  queryCodeTableColumnList: `GET ${prefix}/admin/codeTableColumn/list`,
  updateCodeTableColumn: `PUT ${prefix}/admin/codeTableColumn`,
  createCodeTableColumn: `POST ${prefix}/admin/codeTableColumn`,
  deleteCodeTableColumn: `DELETE ${prefix}/admin/codeTableColumn/:ids`,
  queryCodeTableColumnDetails: `GET ${prefix}/admin/codeTableColumn/:id`,

  //代码生成模板借口
  queryCodeTableTemplateList: `GET ${prefix}/admin/codeTableTemplate/list`,
  queryCodeTableTemplateOptions: `GET ${prefix}/admin/codeTableTemplate/options`,
  updateCodeTableTemplate: `PUT ${prefix}/admin/codeTableTemplate`,
  createCodeTableTemplate: `POST ${prefix}/admin/codeTableTemplate`,
  deleteCodeTableTemplate: `DELETE ${prefix}/admin/codeTableTemplate/:ids`,
  queryCodeTableTemplateDetails: `GET ${prefix}/admin/codeTableTemplate/:id`,
  queryCodeTableTemplateRunTest: `POST ${prefix}/admin/codeTableTemplate/runTest`,
  //模板分组
  queryCodeTableTemplateGroupList: `GET ${prefix}/admin/codeTableTemplateGroup/list`,
  queryCodeTableTemplateGroupOptions: `GET ${prefix}/admin/codeTableTemplateGroup/options`,
  queryCodeTableTemplateGroupBaseDirTree: `GET ${prefix}/admin/codeTableTemplateGroup/getBaseDirTree`,
  updateCodeTableTemplateGroup: `PUT ${prefix}/admin/codeTableTemplateGroup`,
  createCodeTableTemplateGroup: `POST ${prefix}/admin/codeTableTemplateGroup`,
  deleteCodeTableTemplateGroup: `DELETE ${prefix}/admin/codeTableTemplateGroup/:ids`,
  queryCodeTableTemplateGroupDetails: `GET ${prefix}/admin/codeTableTemplateGroup/:id`,

  //groovy 相关接口
  queryGroovyGroupList: `GET ${prefix}/admin/groovyGroup/list`,
  updateGroovyGroup: `PUT ${prefix}/admin/groovyGroup`,
  createGroovyGroup: `POST ${prefix}/admin/groovyGroup`,
  deleteGroovyGroup: `DELETE ${prefix}/admin/groovyGroup/:ids`,
  queryGroovyGroupDetails: `GET ${prefix}/admin/groovyGroup/:id`,
  queryGroovyGroupOptions: `GET ${prefix}/admin/groovyGroup/options`,

  queryGroovyInfoList: `GET ${prefix}/admin/groovyInfo/list`,
  updateGroovyInfo: `PUT ${prefix}/admin/groovyInfo`,
  createGroovyInfo: `POST ${prefix}/admin/groovyInfo`,
  testGroovyInfo: `POST ${prefix}/admin/groovyInfo/test`,
  deleteGroovyInfo: `DELETE ${prefix}/admin/groovyInfo/:ids`,
  queryGroovyInfoDetails: `GET ${prefix}/admin/groovyInfo/:id`,

  //聊天相关接口
  queryChatCustomerServiceList: `GET ${prefix}/admin/chatCustomerService/list`,
  updateChatCustomerService: `PUT ${prefix}/admin/chatCustomerService`,
  createChatCustomerService: `POST ${prefix}/admin/chatCustomerService`,
  deleteChatCustomerService: `DELETE ${prefix}/admin/chatCustomerService/:ids`,
  queryChatCustomerServiceDetails: `GET ${prefix}/admin/chatCustomerService/:id`,

  queryChatSessionList: `GET ${prefix}/admin/chatSession/list`,
  updateChatSession: `PUT ${prefix}/admin/chatSession`,
  createChatSession: `POST ${prefix}/admin/chatSession`,
  deleteChatSession: `DELETE ${prefix}/admin/chatSession/:ids`,
  queryChatSessionDetails: `GET ${prefix}/admin/chatSession/:id`,
  sendChatSessionMessage: `POST ${prefix}/admin/chatSession/sendMessage`, //发送会话消息

  queryChatSessionUserList: `GET ${prefix}/admin/chatSessionUser/list`,
  updateChatSessionUser: `PUT ${prefix}/admin/chatSessionUser`,
  createChatSessionUser: `POST ${prefix}/admin/chatSessionUser`,
  deleteChatSessionUser: `DELETE ${prefix}/admin/chatSessionUser/:ids`,
  queryChatSessionUserDetails: `GET ${prefix}/admin/chatSessionUser/:id`,

  queryChatUserList: `GET ${prefix}/admin/chatUser/list`,
  queryChatUserToken: `GET ${prefix}/admin/chatUser/token`,
  updateChatUser: `PUT ${prefix}/admin/chatUser`,
  createChatUser: `POST ${prefix}/admin/chatUser`,
  deleteChatUser: `DELETE ${prefix}/admin/chatUser/:ids`,
  queryChatUserDetails: `GET ${prefix}/admin/chatUser/:id`,

  queryChatSessionMessageList: `GET ${prefix}/admin/chatSessionMessage/list`,
  updateChatSessionMessage: `PUT ${prefix}/admin/chatSessionMessage`,
  createChatSessionMessage: `POST ${prefix}/admin/chatSessionMessage`,
  deleteChatSessionMessage: `DELETE ${prefix}/admin/chatSessionMessage/:ids`,
  queryChatSessionMessageDetails: `GET ${prefix}/admin/chatSessionMessage/:id`,

  queryChatSessionMessageUserList: `GET ${prefix}/admin/chatSessionMessageUser/list`,
  updateChatSessionMessageUser: `PUT ${prefix}/admin/chatSessionMessageUser`,
  createChatSessionMessageUser: `POST ${prefix}/admin/chatSessionMessageUser`,
  deleteChatSessionMessageUser: `DELETE ${prefix}/admin/chatSessionMessageUser/:ids`,
  queryChatSessionMessageUserDetails: `GET ${prefix}/admin/chatSessionMessageUser/:id`,

  queryChatUserFriendList: `GET ${prefix}/admin/chatUserFriend/list`,
  updateChatUserFriend: `PUT ${prefix}/admin/chatUserFriend`,
  createChatUserFriend: `POST ${prefix}/admin/chatUserFriend`,
  deleteChatUserFriend: `DELETE ${prefix}/admin/chatUserFriend/:ids`,
  queryChatUserFriendDetails: `GET ${prefix}/admin/chatUserFriend/:id`,
  //chat notice
  queryChatUserNoticeList: `GET ${prefix}/admin/chatUserNotice/list`,
  updateChatUserNotice: `PUT ${prefix}/admin/chatUserNotice`,
  createChatUserNotice: `POST ${prefix}/admin/chatUserNotice`,
  deleteChatUserNotice: `DELETE ${prefix}/admin/chatUserNotice/:ids`,
  queryChatUserNoticeDetails: `GET ${prefix}/admin/chatUserNotice/:id`,
  //friend apply
  queryChatUserFriendApplyList: `GET ${prefix}/admin/chatUserFriendApply/list`,
  updateChatUserFriendApply: `PUT ${prefix}/admin/chatUserFriendApply`,
  createChatUserFriendApply: `POST ${prefix}/admin/chatUserFriendApply`,
  deleteChatUserFriendApply: `DELETE ${prefix}/admin/chatUserFriendApply/:ids`,
  queryChatUserFriendApplyDetails: `GET ${prefix}/admin/chatUserFriendApply/:id`,
  //群组相关api
  queryChatGroupList: `GET ${prefix}/admin/chatGroup/list`,
  updateChatGroup: `PUT ${prefix}/admin/chatGroup`,
  createChatGroup: `POST ${prefix}/admin/chatGroup`,
  deleteChatGroup: `DELETE ${prefix}/admin/chatGroup/:ids`,
  queryChatGroupDetails: `GET ${prefix}/admin/chatGroup/:id`,

  queryChatGroupUserList: `GET ${prefix}/admin/chatGroupUser/list`,
  updateChatGroupUser: `PUT ${prefix}/admin/chatGroupUser`,
  createChatGroupUser: `POST ${prefix}/admin/chatGroupUser`,
  deleteChatGroupUser: `DELETE ${prefix}/admin/chatGroupUser/:ids`,
  queryChatGroupUserDetails: `GET ${prefix}/admin/chatGroupUser/:id`,

  queryChatUserEmoteList: `GET ${prefix}/admin/chatUserEmote/list`,
  updateChatUserEmote: `PUT ${prefix}/admin/chatUserEmote`,
  createChatUserEmote: `POST ${prefix}/admin/chatUserEmote`,
  deleteChatUserEmote: `DELETE ${prefix}/admin/chatUserEmote/:ids`,
  queryChatUserEmoteDetails: `GET ${prefix}/admin/chatUserEmote/:id`,

  // 字典
  queryAdminDictList: `GET ${prefix}/admin/adminDict/list`,
  updateAdminDict: `PUT ${prefix}/admin/adminDict`,
  createAdminDict: `POST ${prefix}/admin/adminDict`,
  deleteAdminDict: `DELETE ${prefix}/admin/adminDict/:ids`,
  queryAdminDictDetails: `GET ${prefix}/admin/adminDict/:id`,
  queryAdminDictOptions: `GET ${prefix}/admin/adminDict/options`,

  //文件上传管理
  queryUploadFileList: `GET ${prefix}/admin/uploadFile/list`,
  updateUploadFile: `PUT ${prefix}/admin/uploadFile`,
  createUploadFile: `POST ${prefix}/admin/uploadFile`,
  deleteUploadFile: `DELETE ${prefix}/admin/uploadFile/:ids`,
  queryUploadFileDetails: `GET ${prefix}/admin/uploadFile/:id`,
}
