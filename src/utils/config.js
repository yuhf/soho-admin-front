module.exports = {
  siteName: 'Soho Admin',
  copyright: 'Soho admin  ©2021 fang',
  logoPath: '/logo.svg',
  // apiPrefix: '/api/v1',
  // apiPrefix: 'http://localhost:6677',
  apiPrefix: '',
  fixedHeader: true, // sticky primary layout header

  /* Layout configuration, specify which layout to use for route. */
  layouts: [
    {
      name: 'primary',
      include: [/.*/],
      exclude: [/(\/(en|zh))*\/login/, /(\/(en|zh))*\/demo/],
    },
  ],

  /* I18n configuration, `languages` and `defaultLanguage` are required currently. */
  i18n: {
    /* Countrys flags: https://www.flaticon.com/packs/countrys-flags */
    languages: [
      {
        key: 'en',
        title: 'English',
        flag: '/america.svg',
      },
      {
        key: 'zh',
        title: '中文',
        flag: '/china.svg',
      },
    ],
    defaultLanguage: 'zh',
  },
}
