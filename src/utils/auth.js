import store from 'store'

/**
 * 获取当前登录的用户
 *
 * @returns {*}
 */
export function getCurrentUser() {
  return store.get("user")
}
