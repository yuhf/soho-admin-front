/**
 * tree 相关的函数
 */


function loopName(id, tree) {
    if(tree.key == id) {
      return tree.title;
    }
    if(tree.children) {
      for (let i = 0; i < tree.children.length; i++) {
        var childrenNode = tree.children[i]
        var res = loopName(id, childrenNode)
        if(res) {
          if(tree.key == 0) {
            return res;
          } else {
            return tree.title + ' / ' + res;
          }

        }
      }
    }
}

//获取制定ID列表页显示
export function getNavigationTreeTitle(id, tree) {
    var navigationTitle = loopName(id, {key:0, value: 0, title: '顶级分类', children: tree})
    if(navigationTitle) {
      return navigationTitle
    }
    return "未知"
}

/**
 * 添加根节点
 *
 * @param tree
 * @returns {[{children, title: string, value: number, key: number}]}
 */
export function addRootNode(tree) {
  return [{key:0, value: 0, title: '根', children: tree}]
}
