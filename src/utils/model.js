import modelExtend from 'dva-model-extend'

export const model = {
  reducers: {
    updateState(state, { payload }) {
      return {
        ...state,
        ...payload,
      }
    },
  },
}

export const pageModel = modelExtend(model, {
  state: {
    options: {},
    list: [],
    pagination: {
      showSizeChanger: true,
      showQuickJumper: true,
      current: 1,
      total: 0,
      pageSize: 20,
    },
  },

  reducers: {
    querySuccess(state, { payload }) {
      const { list, pagination } = payload
      return {
        ...state,
        list,
        pagination: {
          ...state.pagination,
          ...pagination,
        },
      }
    },
    queryOptions(state, { payload }) {
      return {
        ...state,
        options: {
          ...state.options,
          ...payload
        }
      }
    },
    queryOptionValueLabel(state, { payload }) {
      console.log("传递进来的数据", payload)
      let options = {};
      //处理keyvalue数据
      Object.keys(payload).forEach((key) => {
        let tmpOption = [];
        console.log(payload[key])
        console.log(typeof payload[key]);
        for(const k in payload[key]) {
          let item = payload[key][k]
          tmpOption[item.value] = item.label;
        }
        options[key] = tmpOption;
      })
      console.log("请求到的option", options)
      return {
        ...state,
        options: {
          ...state.options,
          ...options
        },
        optionValueLabel: {
          ...state.optionValueLabel,
          ...payload
        }
      }
    },
    //查询tree选项成功
    querySuccessTree(state, { payload }) {
      return {
        ...state,
        trees: {
          ...state.trees,
          ...payload
        }
      }
    },
  },
})
