import React, { Component,forwardRef } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { Trans,t } from "@lingui/macro"
import { Button, Row, Col, DatePicker, Form, Input, Cascader, Select,Space } from 'antd'
import styles from './index.less'


export default forwardRef((props, ref) => {
  const {formProps, onAdd,onSubmit=()=>{},onReset=null, children,actions=null,initialValues={}} = props;

  const handleReset = () => {
    const fields = ref.current.getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    ref.current.setFieldsValue(fields)
    onSubmit();
  }

  return <Form ref={ref} className={styles.sohoFilterForm} name="control-ref" initialValues={initialValues} {...formProps}>
    <div style={{display: 'flex',flexWrap: 'wrap'}} >

      {children}

      <div style={{display: 'inline-block',marginLeft:'30px', flex: 1,minWidth: '160px',width: 'auto',whiteSpace: "nowrap"}} >
        <div style={{display:'inline-block'}}>
          {/*<Row type="flex" align="middle" justify="space-between">*/}
          <Space>
            <Button
              type="primary" htmlType="submit"
              className="margin-right"
              onClick={onSubmit}
            >
              <Trans>Search</Trans>
            </Button>
            <Button onClick={onReset || handleReset}>
              <Trans>Reset</Trans>
            </Button>

          </Space>
          {/*</Row>*/}
        </div>

        <div style={{display:'inline-block', float: 'right'}}>
          <Space>
            {onAdd && <Button type="ghost" onClick={onAdd}>
              <Trans>Create</Trans>
            </Button>}
            {actions}
          </Space>
        </div>
      </div>
    </div>
  </Form>
});
