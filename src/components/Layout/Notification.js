import React, {PureComponent} from 'react';
import { Modal } from 'antd';

class Notification extends PureComponent {
  render() {
    const {item, notificationVisible, onNotificationsRead, onNotificationCancel} = this.props
    return (
        <>
          <Modal title={item.title} visible={notificationVisible} onOk={()=>onNotificationsRead(item)} onCancel={onNotificationCancel}>
            {item.content}
          </Modal>
        </>
      )
  }
}

export default Notification;
