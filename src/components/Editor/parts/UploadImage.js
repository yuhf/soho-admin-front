import React, { useState } from 'react'
import {Modal} from "antd";
import { InboxOutlined } from  '@ant-design/icons'
import { UploadProps } from 'antd';
import { message, Upload } from 'antd';
import store from "store";
import api from '../../../services/api'

const {createAdminContentImage} = api
const { Dragger } = Upload;



//图片上传弹窗
export const UploadImage = props => {

  const {onOk, onCancel} = props

  const draggerProps = {
    name: 'thumbnail',
    multiple: true,
    action: createAdminContentImage,
    headers: {Authorization:store.get('token')},
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        console.log(info.file, info.fileList);
      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
        //console.log(info.file.response.payload)
        onOk(info.file.response.payload)
      } else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
    showUploadList:false,
  };

  return <Modal onCancel={props.onCancel} onOk={false} footer={null} open={props.open} >
    <Dragger {...draggerProps}>
      <p className="ant-upload-drag-icon">
        <InboxOutlined />
      </p>
      <p className="ant-upload-text">Click or drag file to this area to upload</p>
      <p className="ant-upload-hint">
        Support for a single or bulk upload. Strictly prohibit from uploading company data or other
        band files
      </p>
    </Dragger>
  </Modal>
}
