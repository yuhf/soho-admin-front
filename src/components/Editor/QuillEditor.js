import React from 'react'
import PropTypes from 'prop-types'
import ReactQuill from "react-quill";
import {UploadImage} from "./parts/UploadImage";

/*
 * Editor component with custom toolbar and content containers
 */
class QuillEditor extends React.PureComponent {
  refs = React.createRef(null)


  constructor(props) {
    super(props);
    this.state = { editorHtml: '', openUploadImage: false };
    this.handleChange = this.handleChange.bind(this);
    this.reactQuillRef = null;
    this.cursorPosition = null; //记录光标位置
  }

  handleChange(html) {
    this.setState({ editorHtml: html });
  }

  handleOpenImageUpload() {
    let quill = this.reactQuillRef.getEditor();//获取到编辑器本身
    if(quill.getSelection()) {
      const cursorPosition = quill.getSelection().index;//获取当前光标位置
      this.cursorPosition = cursorPosition
      this.setState({openUploadImage: true})
    }
  }

  onUploadImageCancel = () => {
    this.setState({openUploadImage: false})
  }

  onUploadImageOk = (src) => {
    let quill = this.reactQuillRef.getEditor();//获取到编辑器本身
    this.setState({openUploadImage: false})
    quill.insertEmbed(this.cursorPosition, "image", src);//插入图片
    quill.setSelection(this.cursorPosition + 1);//光标位置加1
    this.cursorPosition = null;
  }

  options = {
    toolbar: {
      container: [
        [{'font': []}],
        // [{'header': [1, 2, false]}]
        [{'size': ['small', false, 'large', 'huge']}],
        [{ 'header': 1 }, { 'header': 2 }],
        [{'color': []}, {'background': []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        ['blockquote', 'code-block'],
        [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
        ['link', 'image'],
        ['clean']
      ],
      handlers: {
        image: () =>  {
          this.handleOpenImageUpload()
        },
      },
    },
  };

  render() {
    return (
      <div className="text-editor">
        {/*<CustomToolbar />*/}
        <ReactQuill
          ref={(el) => {this.reactQuillRef = el}}
          value={this.props.value}
          onChange={this.props.onChange}
          // placeholder={this.props.placeholder}
          modules={this.options}
        />
        <UploadImage onCancel={this.onUploadImageCancel} onOk={this.onUploadImageOk} open={this.state.openUploadImage} />
      </div>
    );
  }
}

/*
 * Quill modules to attach to editor
 * See http://quilljs.com/docs/modules/ for complete options
 */
QuillEditor.modules = {
  toolbar: {
  //  container: '#toolbar',
    container: [
        [{'font': []}],
      // [{'header': [1, 2, false]}]
      [{'size': ['small', false, 'large', 'huge']}],
      [{ 'header': 1 }, { 'header': 2 }],
      [{'color': []}, {'background': []}],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      ['blockquote', 'code-block'],
      [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
      ['link', 'image'],
      ['clean']
    ],
    handlers: {
      image: QuillEditor.handleOpenImageUpload,
    },
  },
};

/*
 * Quill editor formats
 * See http://quilljs.com/docs/formats/
 */
QuillEditor.formats = [
  'header',
  'font',
  'size',
  'bold',
  'italic',
  'underline',
  'strike',
  'blockquote',
  'list',
  'bullet',
  'indent',
  'link',
  'image',
  'color',
];

/*
 * PropType validation
 */
QuillEditor.propTypes = {
  placeholder: PropTypes.string,
};

export default QuillEditor;
