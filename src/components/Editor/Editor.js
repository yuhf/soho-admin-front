import React from 'react'
import { Editor } from 'react-draft-wysiwyg'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import styles from './Editor.less'
import 'react-quill/dist/quill.snow.css';

export const DraftEditor = props => {
  let editorClass = props.editorClassName
  if(editorClass == null) {
    editorClass = styles.editor
  }
  return (
    <Editor
      toolbarClassName={styles.toolbar}
      wrapperClassName={styles.wrapper}
      editorClassName={editorClass}
      {...props}
    />
  )
}


export default DraftEditor
