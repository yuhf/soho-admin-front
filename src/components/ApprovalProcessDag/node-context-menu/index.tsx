import React, {useRef} from 'react'
import {
  CopyOutlined,
  DeleteOutlined,
} from '@ant-design/icons'
import { Menu } from '@antv/x6-react-components'
import styles from './index.less'

interface Props {
  experimentId: string
  appRef: any
  data: any
  node: any
  onAddNode: any
  isDisplay: boolean
}

export const NodeContextMenu: React.FC<Props> = (props) => {
  const { experimentId, data, appRef, node, onAddNode,onDelNode,onConsoleMenu, isDisplay=false } = props
  const containerRef = useRef<HTMLDivElement>(null as any)

  let left =  node==null ? 100 : node.getData().x;
  let top = node==null ? 100 : node.getData().y;
  top = top+80
  left = left + 150

  return (
    <div
      ref={containerRef}
      className={isDisplay ? styles.graphContextMenu : styles.graphContextMenuNon}
      style={{ left, top }}
    >
      <Menu hasIcon={false}>
        <Menu.Item onClick={() => onAddNode(node)} icon={<CopyOutlined />} text="添加下一审批人" />
        <Menu.Item onClick={() => onDelNode(node)} icon={<DeleteOutlined />} text="删除" />
        <Menu.Item onClick={() => onConsoleMenu()} icon={<DeleteOutlined />} text="取消" />
      </Menu>
    </div>
  )
}
