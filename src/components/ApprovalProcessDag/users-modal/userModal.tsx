import React, { useState } from 'react';
import {Modal, Button, Form, Input, Select} from 'antd';
import {t} from "@lingui/macro";
import TextArea from "antd/es/input/TextArea";
const FormItem = Form.Item

interface Props{
  isModalVisible: boolean,
  onOk: any,
  onCancel: any
  userOptions: any
  currentNode: any
}

const UserModal:React.FC<Props>  = (props) => {
  const {isModalVisible, onOk,onCancel, userOptions, item={}, currentNode} = props
  let formRef = React.createRef()

  const handleOk = () => {
    formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          userOptions: userOptions,
          node: currentNode,
        }
        onOk(data)
      })
    // onOk({});
  };

  const handleCancel = () => {
    // setIsModalVisible(false);
    onCancel();
  };

  const children = [];
  for (let i = 0; i < userOptions.length; i++) {
    children.push(<Option key={userOptions[i].id}>{userOptions[i].username}</Option>);
  }

  return (
    <>
      <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
        <Form ref={formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

          {/* eslint-disable-next-line react/jsx-no-undef */}
          <FormItem name='adminUserIds' rules={[{ required: true }]}
                    label={t`Admin user`} hasFeedback >
            <Select
              mode="multiple"
              allowClear
              style={{ width: '100%' }}
              placeholder="Please select"
              defaultValue={[]}
              // onChange={handleChange}
            >
              {children}
            </Select>
          </FormItem>

        </Form>
      </Modal>
    </>
  );
};

export default UserModal;
