import React from 'react'
import {Button, Divider, Popover} from "antd";
import iconMap from "../../utils/iconMap";
import { Input, Space } from 'antd';
import {t} from "@lingui/macro";
const { Search } = Input;

class IconSelect extends React.PureComponent {
  iconList = null


  constructor(props) {
    super(props);
    //初始化图标信息
    let iconList = []
    for(let key in iconMap) {
      // console.log(key)
      let item = <Button shape="circle" size={"large"} onClick={(e) => this.onSelectIcon(e, key)} value={key} icon={iconMap[key]} />
      // item.height = 40
      iconList.push(item)
    }
    this.iconList = iconList
    this.state = {value: this.props?.value, open: false}
  }

  onSearch = (value) => {
    this.setState({open: true})
  }

  onSelectIcon = (e, key) => {
    this.props.onChange(key)
    this.setState({value: key, open: false})
  }

  onCancel = () => {
    this.setState({open: false})
  }

  render() {

    const content = (
      <div>
        {this.iconList}
        <Divider></Divider>
        <Button onClick={this.onCancel}  style={{ width: '100%' }} >{t`Cancel`}</Button>
      </div>
    );

    return <>

      <Popover content={content} open={this.state.open} onMouseOut={this.onCancel} title="Icon" overlayStyle={{maxWidth: 2200}}>
        <Search
          prefix={this.props.value && iconMap[this.props.value]}
          value={this.state.value}
          placeholder="input icon name"
          allowClear
          enterButton="Select Icon"
          size="large"
          onSearch={this.onSearch}
        />
      </Popover>
    </>;
  }
}


export default IconSelect;
