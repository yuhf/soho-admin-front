import React from 'react';
import CodeMirror from '@uiw/react-codemirror';
import { loadLanguage, langNames, langs } from '@uiw/codemirror-extensions-langs';
loadLanguage('groovy');

// langs.groovy();

const CodeEditor = props => {

  return (
    <CodeMirror
      height="400px"
      extensions={[langs.groovy()]}
      {...props}
      />
  )
}

export default CodeEditor;
