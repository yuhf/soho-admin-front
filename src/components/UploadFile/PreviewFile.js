import React, {PureComponent} from 'react';
import {MenuOutlined,FileOutlined,FileTextOutlined,FileExcelOutlined,FileGifOutlined,FileJpgOutlined,FileImageOutlined,FilePdfOutlined,FilePptOutlined,FileUnknownOutlined,FileWordOutlined
  ,FileZipOutlined,FileMarkdownOutlined} from '@ant-design/icons'
import {Image} from 'antd'

const iconStyle = {fontSize:'40px'}
//文件组件
const fileTypes = {
  txt: (props) => { return <FileTextOutlined {...props} style={{...iconStyle, ...props.style}} />},
  xsl: (props) => { return <FileExcelOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  exe: (props) => { return <FileExcelOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  gif: (props) => { return <FileGifOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  jpg: (props) => { return <FileJpgOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  jpeg: (props) => { return <FileJpgOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  png: (props) => { return <FileImageOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  image: (props) => { return <FileImageOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  md: (props) => { return <FileMarkdownOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  pdf: (props) => { return <FilePdfOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  ppt: (props) => { return <FilePptOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  unknown: (props) => { return <FileUnknownOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  doc: (props) => { return <FileWordOutlined  {...props} style={{...iconStyle, ...props.style}} />},
  zip: (props) => { return <FileZipOutlined  {...props} style={{...iconStyle, ...props.style}} />},
}

/**
 * 文件图标
 *
 * @param props
 */
export default (props) => {
  const {extension,fileType,url} = props
  if(fileType.indexOf('image') == 0) {
    let {onClick, ...params} = props
    //图片
    return <Image src={url} style={{...props.style}} {...params} />
  }
  let node = fileTypes[extension];
  if(node == null) {
    node = fileTypes['unknown']
  }
  return node({...props});
}
