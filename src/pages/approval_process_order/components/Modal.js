import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ApprovalProcessOrderModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          key: item.key,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form, ...modalProps } = this.props
    return (

      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

         <FormItem name='id' rules={[{ required: true }]}
            label={t`id`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='approvalProcessId' rules={[{ required: true }]}
            label={t`approvalProcessId`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='outNo' rules={[{ required: true }]}
            label={t`outNo`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='status' rules={[{ required: true }]}
            label={t`status`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='serialNumber' rules={[{ required: true }]}
            label={t`serialNumber`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='createdTime' rules={[{ required: true }]}
            label={t`createdTime`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

ApprovalProcessOrderModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default ApprovalProcessOrderModal

