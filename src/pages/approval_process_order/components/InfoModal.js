import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Input,
  InputNumber,
  Radio,
  Modal,
  Select,
  Upload,
  message,
  Space,
  Col,
  Row,
  Card,
  Descriptions
} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import { Timeline } from 'antd';
import { SmileOutlined } from '@ant-design/icons';

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ApprovalProcessOrderInfoModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk,onCancel } = this.props
    onCancel();
    // this.formRef.current.validateFields()
    //   .then(values => {
    //     const data = {
    //       ...values,
    //       key: item.key,
    //     }
    //     onOk(data)
    //   })
    //   .catch(errorInfo => {
    //     console.log(errorInfo)
    //   })
  }

  render() {
    const { item = {}, onOk, form, userOptions=[], ...modalProps } = this.props
    const contentList = JSON.parse(item.content ? item.content : '[]')
    const contentItemList = [];
    let users = {}
    userOptions.forEach(user=>{
      users[user.id] = user.username;
    })
    // contentItemList.push(<><Row><Col span={8}>审批类型</Col><Col span={16}>{item.approvalProcessName}</Col></Row></>)
    contentItemList.push(<Descriptions.Item label={'审批类型'} >{item.approvalProcessName}</Descriptions.Item>)
    contentItemList.push(<Descriptions.Item label={'创建时间'} >{item.createdTime}</Descriptions.Item>)
    contentList.forEach((d)=>{
      contentItemList.push(<Descriptions.Item label={d.title} >{d.content}</Descriptions.Item>)
    });

    //计算节点信息
    let nodeList = [];
    let nodes = item.nodes ? item.nodes : [];
    nodes.forEach(node=>{
      let color = 'gray';
      switch (node.status) {
        case 30:
        case 40:
          color = "green";
          break;
        case 20:
          color = "red";
          break;
        case 10:
          color = "blue";
          break;
        default:
          color = "gray";
          break;
      }
      nodeList.push(<Timeline.Item color={color}>
        <p>{node.approvalTime} {users[node.userId]} {node.statusName}</p>
        <p>{node.reply}</p>
      </Timeline.Item>)
    })

    return (

      <Modal {...modalProps} onOk={this.handleOk}>

        <Descriptions title="">
          {contentItemList}
        </Descriptions>



        <Timeline>
          <Timeline.Item color="green">{item.createdTime} {users[item.appleUserId]} 提交审批 </Timeline.Item>
          {nodeList}
        </Timeline>

      </Modal>
    )
  }
}

ApprovalProcessOrderInfoModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
  userOptions: PropTypes.any
}

export default ApprovalProcessOrderInfoModal

