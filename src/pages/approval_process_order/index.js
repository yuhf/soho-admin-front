import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import ApprovalProcessOrderModal from "./components/Modal";
import Filter from "./components/Filter"
import ApprovalProcessOrderInfoModal from "./components/InfoModal";

@connect(({ approval_process_order, loading }) => ({ approval_process_order, loading }))
class ApprovalProcessOrder extends PureComponent {
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { approval_process_order, loading, location, dispatch } = this.props
    const { list, pagination } = approval_process_order
    const { query, pathname } = location
    return {
      pagination,
      dataSource: list,
      loading: loading.effects['approval_process_order/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'approval_process_order/delete',
          payload: id,
        }).then(() => {
          this.handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'approval_process_order/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, approval_process_order, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds,userOptions } = approval_process_order

    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`approval_process_order/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      userOptions:userOptions,
      title: `${
         // modalType === 'create' ? t`Create` : t`Update`
        t`Approval Process Order Info`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `approval_process_order/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'approval_process_order/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch } = this.props
    const {query, pathname} = location
    return {
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'approval_process_order/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        {/*<ApprovalProcessOrderModal {...this.modalProps} />*/}
        <ApprovalProcessOrderInfoModal {...this.modalProps} />
      </Page>
    )
  }
}

ApprovalProcessOrder.propTypes = {
  approval_process_order: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default ApprovalProcessOrder

