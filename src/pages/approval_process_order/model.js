import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const {queryUserOptions, queryApprovalProcessOrderList, updateApprovalProcessOrder, createApprovalProcessOrder,deleteApprovalProcessOrder } = api

export default modelExtend(pageModel, {
  namespace: 'approval_process_order',
  state: {
    currentItem: {},
    pageNum: 1,
    pageSize: 20,
    modalVisible: false,
    modalType: 'create',
    userOptions: {}
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/approval_process_order').exec(location.pathname)) {
          dispatch({
            type: 'queryUserOptions',
          })
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              pageNum: 1,
              status: 2,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *queryUserOptions({ payload }, { call, put }) {
      const data = yield call(queryUserOptions, payload)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            userOptions: data.payload,
          },
        })
      } else {
        throw data
      }
    },
    *query({ payload }, { call, put }) {
      const data = yield call(queryApprovalProcessOrderList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createApprovalProcessOrder, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const id = yield select(({ user }) => user.currentItem.id)
      const newUser = { ...payload, id }
      const data = yield call(updateApprovalProcessOrder, newUser)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteApprovalProcessOrder, {ids:payload})
      if (!data.success) {
        throw data
      }
    }
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
