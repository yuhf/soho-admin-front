import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import OpenAppModal from "./components/Modal";
import Filter from "./components/Filter"


@connect(({ open_app, loading }) => ({ open_app, loading }))
class OpenApp extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { open_app, loading, location, dispatch } = this.props
    const { list, pagination } = open_app
    const { query, pathname } = location
    return {
      options: open_app.options,
      trees: open_app.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['open_app/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'open_app/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'open_app/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onApplyItem(item) {
        dispatch({
          type: 'open_app/showModal',
          payload: {
            modalType: 'apply',
            currentItem: item,
          }
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'open_app/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, open_app, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = open_app

    return {
      options: open_app.options,
      trees: open_app.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`open_app/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
          modalType === 'create' ? t`Create` : (modalType === 'update' ? t`Update` : t`Apply`)
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `open_app/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'open_app/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, open_app} = this.props
    const {query,pathname} = location
    return {
      options: open_app.options,
      trees: open_app.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'open_app/showModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <OpenAppModal {...this.modalProps} />
      </Page>
    )
  }
}

OpenApp.propTypes = {
  open_app: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default OpenApp
