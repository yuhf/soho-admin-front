import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryChatSessionMessageUserList, updateChatSessionMessageUser, createChatSessionMessageUser,deleteChatSessionMessageUser } = api
/**
 *
   //相关api信息
 queryChatSessionMessageUserList: `GET ${prefix}/admin/chatSessionMessageUser/list`,
 updateChatSessionMessageUser: `PUT ${prefix}/admin/chatSessionMessageUser`,
 createChatSessionMessageUser: `POST ${prefix}/admin/chatSessionMessageUser`,
 deleteChatSessionMessageUser: `DELETE ${prefix}/admin/chatSessionMessageUser/:ids`,
 queryChatSessionMessageUserDetails: `GET ${prefix}/admin/chatSessionMessageUser/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'chat_session_message_user',
  state: {
    options: {
      isRead: {
        0: "否",
        1: "是"
      }
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/chat_session_message_user').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryChatSessionMessageUserList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createChatSessionMessageUser, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateChatSessionMessageUser, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteChatSessionMessageUser, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
