import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Input,
  InputNumber,
  Radio,
  Modal,
  Select,
  Upload,
  message,
  Checkbox,
  DatePicker,
  TreeSelect,
  Switch
} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import QuillEditor from "../../../components/Editor/QuillEditor";
import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ChatSessionMessageUserModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
          isRead: values.isRead ? 1: 0,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props
    let isRead = item?.isRead == 1 ? true : false;
    let initData = { ...item, isRead: isRead};
    //初始化时间处理
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
         <FormItem  name='messageId' rules={[{ required: 0 }]}            label={t`Message Id`} hasFeedback {...formItemLayout}>
            <InputNumber step="1"  style={{width:200}}             disabled={true}
 />
          </FormItem>
         <FormItem  name='uid' rules={[{ required: 0 }]}            label={t`Uid`} hasFeedback {...formItemLayout}>
            <InputNumber step="1"  style={{width:200}}             disabled={true}
 />
          </FormItem>
         <FormItem  name='isRead' rules={[{ required: 0 }]}     valuePropName="checked"       label={t`Is Read`} hasFeedback {...formItemLayout}>
            <Switch  disabled={false}  />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

ChatSessionMessageUserModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default ChatSessionMessageUserModal
