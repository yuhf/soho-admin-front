import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryGroovyGroupList, updateGroovyGroup, createGroovyGroup,deleteGroovyGroup } = api
/**
 *
   //相关api信息
 queryGroovyGroupList: `GET ${prefix}/admin/groovyGroup/list`,
 updateGroovyGroup: `PUT ${prefix}/admin/groovyGroup`,
 createGroovyGroup: `POST ${prefix}/admin/groovyGroup`,
 deleteGroovyGroup: `DELETE ${prefix}/admin/groovyGroup/:ids`,
 queryGroovyGroupDetails: `GET ${prefix}/admin/groovyGroup/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'groovy_group',
  state: {
    options: {
      status: {
        "1": "正常",
        "2": "禁用"
      }
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/groovy_group').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryGroovyGroupList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createGroovyGroup, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateGroovyGroup, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteGroovyGroup, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
