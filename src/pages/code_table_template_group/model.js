import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryCodeTableTemplateGroupList, updateCodeTableTemplateGroup, createCodeTableTemplateGroup,deleteCodeTableTemplateGroup } = api
/**
 *
   //相关api信息
 queryCodeTableTemplateGroupList: `GET ${prefix}/admin/codeTableTemplateGroup/list`,
 updateCodeTableTemplateGroup: `PUT ${prefix}/admin/codeTableTemplateGroup`,
 createCodeTableTemplateGroup: `POST ${prefix}/admin/codeTableTemplateGroup`,
 deleteCodeTableTemplateGroup: `DELETE ${prefix}/admin/codeTableTemplateGroup/:ids`,
 queryCodeTableTemplateGroupDetails: `GET ${prefix}/admin/codeTableTemplateGroup/:id`,

原始数据解析信息
name: id   capitalName: Id capitalKeyName: Id   type: long   frontType: number     frontMax: 99999999999       frontStep:      dbType: int       dbUnsigned: false       comment: ID     length: 11       scale: 0     frontLength: 12     defaultValue: null       isNotNull: true     specification: int(11)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: title   capitalName: Title capitalKeyName: Title   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 标题     length: 255       scale: 0     frontLength: 255     defaultValue: null       isNotNull: false     specification: varchar(255)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: name   capitalName: Name capitalKeyName: Name   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 名称     length: 45       scale: 0     frontLength: 45     defaultValue: null       isNotNull: false     specification: varchar(45)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: mainFunction   capitalName: Main Function capitalKeyName: MainFunction   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 该组入口函数     length: 45       scale: 0     frontLength: 45     defaultValue: null       isNotNull: false     specification: varchar(45)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: explain   capitalName: Explain capitalKeyName: Explain   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 说明     length: 1000       scale: 0     frontLength: 1000     defaultValue: null       isNotNull: false     specification: varchar(1000)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: updatedTime   capitalName: Updated Time capitalKeyName: UpdatedTime   type: java.sql.Timestamp   frontType: text     frontMax: null       frontStep:      dbType: datetime       dbUnsigned: false       comment: 更新时间     length: -1       scale: 0     frontLength: -1     defaultValue: null       isNotNull: false     specification: datetime     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: createdTime   capitalName: Created Time capitalKeyName: CreatedTime   type: java.sql.Timestamp   frontType: text     frontMax: null       frontStep:      dbType: datetime       dbUnsigned: false       comment: 创建时间     length: -1       scale: 0     frontLength: -1     defaultValue: null       isNotNull: false     specification: datetime     dbForeignName:      capitalForeignName:        javaForeignName:         *
 */
export default modelExtend(pageModel, {
  namespace: 'code_table_template_group',
  state: {
    options: {
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/code_table_template_group').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryCodeTableTemplateGroupList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createCodeTableTemplateGroup, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateCodeTableTemplateGroup, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteCodeTableTemplateGroup, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
