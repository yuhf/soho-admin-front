import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import draftToHtml from "draftjs-to-html";
import draft from "draft-js";import DraftEditor from "../../../components/Editor";import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const {ContentState} = draft
const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class CodeTableTemplateGroupModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          //时间值处理
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props

    let initData = {...item};
    //初始化时间处理
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">

         <FormItem  name='title' rules={[{ required: false }]}
            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input maxLength={255} />
          </FormItem>
         <FormItem  name='name' rules={[{ required: false }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input maxLength={45} />
          </FormItem>
         <FormItem  name='mainFunction' rules={[{ required: false }]}
            label={t`Main Function`} hasFeedback {...formItemLayout}>
            <Input maxLength={45} />
          </FormItem>
         <FormItem  name='basePath' rules={[{ required: false }]}
            label={t`Base Path`} hasFeedback {...formItemLayout}>
            <Input maxLength={1000} />
          </FormItem>
         <FormItem  name='explain' rules={[{ required: false }]}
            label={t`Explain`} hasFeedback {...formItemLayout}>
            <Input maxLength={1000} />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

CodeTableTemplateGroupModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default CodeTableTemplateGroupModal

