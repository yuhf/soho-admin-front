import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import ApprovalProcessMyOrderModal from "./components/Modal";
import Filter from "./components/Filter"
import ApprovalProcessOrderInfoModal from "./components/InfoModal";
import {Button} from "antd";

@connect(({ approval_process_myorder, loading }) => ({ approval_process_myorder, loading }))
class ApprovalProcessMyOrder extends PureComponent {
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        type: key,
      }),
    })
  }

  get listProps() {
    const { approval_process_myorder, loading, location, dispatch } = this.props
    const { list, pagination,userOptions } = approval_process_myorder
    const { query, pathname } = location
    return {
      pagination,
      dataSource: list,
      userOptions: userOptions,
      loading: loading.effects['approval_process_myorder/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'approval_process_myorder/delete',
          payload: id,
        }).then(() => {
          this.handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'approval_process_myorder/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, approval_process_myorder, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds,userOptions } = approval_process_myorder

    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`approval_process_myorder/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      userOptions:userOptions,
      title: `${
         // modalType === 'create' ? t`Create` : t`Update`
        t`Approval Process Order Info`
      }`,
      centered: true,
      onApproval: data => {
        const { pathname } = this.props.location
        dispatch({
          type: `approval_process_myorder/approval`,
          payload: data,
        }).then(() => {


          history.push({
            pathname,
            search: stringify({
            }),
          })
        })
      },
      onOkCreate() {
        dispatch({
          type: 'approval_process_myorder/showCreateModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
      onCancelCreate() {
        dispatch({
          type: 'approval_process_myorder/hideCreateModal',
        })
      },
      onCancel() {
        dispatch({
          type: 'approval_process_myorder/hideModal',
        })
      },
    }
  }
  get modalCreateProps() {
    const { dispatch, approval_process_myorder, loading } = this.props
    const { currentItem, modalCreateVisible,approvalProcessOptions, modalType,resourceTree,resourceIds,userOptions,currentApprovalProcess } = approval_process_myorder
    const { pathname,query } = this.props.location
    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalCreateVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`approval_process_myorder/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      userOptions:userOptions,
      approvalProcessOptions:approvalProcessOptions,
      currentApprovalProcess: currentApprovalProcess,
      title: `${
        t`Create Approval Process Order`
      }`,
      centered: true,
      onOkCreate(data) {
        dispatch({
          type: 'approval_process_myorder/create',
          payload: {
            ...data
          },
        }).then(()=>{
          dispatch({
            type: 'approval_process_myorder/hideCreateModal',
          })
          history.push({
            pathname,
            search: stringify(query),
          })
        })
      },
      onCancel() {
        dispatch({
          type: 'approval_process_myorder/hideCreateModal',
        })
      },
      //查询审批单详情
      onQueryApprovalProcess(payload) {
        dispatch({
          type: 'approval_process_myorder/queryApprovalProcess',
          payload: payload
        })
      }
    }
  }


  get filterProps() {
    const { location, dispatch } = this.props
    const {query, pathname} = location
    return {
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'approval_process_myorder/showCreateModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List onClickTab={this.handleTabClick} {...this.listProps} />
        <ApprovalProcessMyOrderModal {...this.modalCreateProps} />
        <ApprovalProcessOrderInfoModal {...this.modalProps} />
      </Page>
    )
  }
}

ApprovalProcessMyOrder.propTypes = {
  approval_process_myorder: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default ApprovalProcessMyOrder

