import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const {queryUserOptions, queryApprovalProcessMyOrderList, updateApprovalProcessOrder, createApprovalProcessOrder,deleteApprovalProcessOrder
        ,updateApprovalProcessMyOrderApproval, queryApprovalProcessOptions, queryApprovalProcessDetails} = api

export default modelExtend(pageModel, {
  namespace: 'approval_process_myorder',
  state: {
    currentItem: {},
    pageNum: 1,
    pageSize: 20,
    modalVisible: false,
    modalCreateVisible: false,
    modalType: 'create',
    userOptions: {},
    approvalProcessOptions: [], //审批单数据
    type: null, //请求列表的类型 null 不做限制  1 我的审批单  2  我审批的  3 包含我审批的
    currentApprovalProcess: {},
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/approval_process_myorder').exec(location.pathname)) {
          dispatch({
            type: 'queryUserOptions',
          })
          dispatch({
            type: 'queryApprovalProcessOptions',
          })
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              // type: this.state.type,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *queryUserOptions({ payload }, { call, put }) {
      const data = yield call(queryUserOptions, payload)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            userOptions: data.payload,
          },
        })
      } else {
        throw data
      }
    },
    *queryApprovalProcessOptions({payload}, {call,put}) {
      const data = yield call(queryApprovalProcessOptions)
      if(data.success) {
        yield put({
          type: 'updateState',
          payload: {
            approvalProcessOptions: data.payload
          }
        })
      }
    },
    *query({ payload }, { call, put }) {
      const data = yield call(queryApprovalProcessMyOrderList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createApprovalProcessOrder, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const id = yield select(({ user }) => user.currentItem.id)
      const newUser = { ...payload, id }
      const data = yield call(updateApprovalProcessOrder, newUser)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteApprovalProcessOrder, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
    *approval({ payload }, { select, call, put }) {
      const data = yield  call(updateApprovalProcessMyOrderApproval, payload)
      if(!data.success) {
        throw data
      } else {
        yield put({ type: 'hideModal' })
      }
    },
    //显示创建申请单的时候需要元数据， 请求审批流获取信息
    *queryApprovalProcess({payload}, {call,put}) {
      const data = yield call(queryApprovalProcessDetails, payload)
      if(data.success) {
        yield put({type: 'updateState', payload:{currentApprovalProcess: data.payload}})
      }
    }
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
    showCreateModal(state, { payload }) {
      return { ...state, ...payload, modalCreateVisible: true }
    },

    hideCreateModal(state) {
      return { ...state, modalCreateVisible: false }
    },
  },
})
