import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message, TimePicker} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import TextArea from "antd/es/input/TextArea";
import moment from 'moment';
import { DatePicker, Space } from 'antd';
import QuillEditor from "../../../components/Editor/QuillEditor";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ApprovalProcessMyOrderModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOkCreate,currentApprovalProcess } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        let data = {}
        let items = []
        let contentItems = {}
        data["approvalProcessId"] = values.approvalProcessId
        if(currentApprovalProcess.metadata) {
          items = JSON.parse(currentApprovalProcess.metadata)
        }
        items.forEach(d=>{
          let name = "content." + d.key
          if(d.type == 'datetime') {
            contentItems[d.key] = values[name].format('YYYY-MM-DD 00:00:00')
          } else {
            contentItems[d.key] = values[name]
          }
        })
        data['content'] = contentItems
        console.log("========================")
        console.log(values)
        console.log(data)

        onOkCreate(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  handleCancel = () => {
    const {onCreateCancel} = this.props
    onCreateCancel()
  }

  //查询更新审批流
  handleSelectChange = (value) => {
    const {onQueryApprovalProcess} = this.props
    onQueryApprovalProcess({id:value})
  }

  render() {
    const { item = {}, onOk, form,modalCreateVisible,approvalProcessOptions,currentApprovalProcess, ...modalProps } = this.props
    let options = [];
    approvalProcessOptions.forEach(item=>{
      options.push(<Option value={item.id}>{item.name}</Option>)
    });
    //生成单元表单
    let items = []
    if(currentApprovalProcess.metadata) {
      items = JSON.parse(currentApprovalProcess.metadata)
    }
    let formItems = []
    for (let i = 0; i < items.length; i++) {
      let item = items[i]
      let name = "content."+item.key
      let tips = item.tips == null ? '' : item.tips
      if(item.type == 'datetime') {
        //时间选项
        formItems.push(<FormItem name={name} rules={[{ required: false }]}
                                 tooltip={tips}  label={item.title} hasFeedback {...formItemLayout}>
            <DatePicker  showTime format="YYYY-MM-DD HH:mm:ss" />
        </FormItem>)
      } else if(item.type == 'textarea') {
        //默认 input
        formItems.push(<FormItem name={name} rules={[{ required: false }]}
                                 tooltip={tips}  label={item.title} hasFeedback {...formItemLayout}>
          <TextArea />
        </FormItem>)
      } else if(item.type == 'editor') {
        //默认 input
        formItems.push(<FormItem name={name} rules={[{ required: false }]}
                               tooltip={tips}  label={item.title} hasFeedback {...formItemLayout}>
          <QuillEditor />
        </FormItem>)
      } else {
        //默认 input
        formItems.push(<FormItem name={name} rules={[{ required: false }]}
                               tooltip={tips}  label={item.title} hasFeedback {...formItemLayout}>
          <Input />
        </FormItem>)
      }

    }

    return (

      <Modal {...modalProps} modaVisible={modalCreateVisible} onOk={this.handleOk} width={1400}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

         <FormItem name='approvalProcessId' rules={[{ required: true }]}
            label={t`Approval Process`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Select a approval process`}
             optionFilterProp="children"
             onChange={this.handleSelectChange}
             filterOption={(input, option) =>
               option.children.toLowerCase().includes(input.toLowerCase())
             }
           > {options} </Select>
          </FormItem>
          {formItems}
        </Form>
      </Modal>
    )
  }
}

ApprovalProcessMyOrderModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
  approvalProcessOptions: PropTypes.array
}

export default ApprovalProcessMyOrderModal

