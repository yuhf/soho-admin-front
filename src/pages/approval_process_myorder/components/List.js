import React, { PureComponent } from 'react'
import {Table, Avatar, Modal, Tabs} from 'antd'
import {t, Trans} from "@lingui/macro"
import { Ellipsis } from 'components'
import {DropOption} from "../../../components";
import PropTypes from "prop-types";
const { confirm } = Modal
const { TabPane } = Tabs;

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const { onDeleteItem, onEditItem,userOptions } = this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    }
  }

  handleClickTab = (key) => {
    const {onClickTab} = this.props;
    onClickTab(key)
  }

  render() {
    const { ...tableProps } = this.props
    const {userOptions } = this.props
    const columns = [

      {
        title: t`Id`,
        dataIndex: 'id',
      },
      {
        title: t`Out No`,
        dataIndex: 'outNo',
      },
      // {
      //   title: t`Approval Process Id`,
      //   dataIndex: 'approvalProcessId',
      // },
      {
        title: t`Approval Process Name`,
        dataIndex: 'approvalProcessName',
      },
      {
        title: t`Apply User`,
        dataIndex: 'applyUserId',
        render: (text, recode) => {
          let userName = null;
          userOptions.forEach(item=>{
            if(recode.applyUserId == item.id) {
              userName = item.username;
            }
          })
          return userName
        },
      },

      {
        title: t`Status Name`,
        dataIndex: 'statusName',
      },
      {
        title: t`Apply Status Name`,
        dataIndex: 'applyStatusName',
      },
      {
        title: t`Created Time`,
        dataIndex: 'createdTime',
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                { key: '1', name: t`Details` },
                // { key: '2', name: t`Delete` },
              ]}
            />
          )
        },
      },
    ]

    return (
      <Tabs defaultActiveKey="1" onChange={this.handleClickTab}>
        <TabPane tab="所有审批单" key="0">
          <Table
            {...tableProps}
            pagination={{
              ...tableProps.pagination,
              showTotal: total => t`Total ${total} Items`,
            }}
            bordered
            scroll={{ x: 1200 }}
            columns={columns}
            simple
            rowKey={record => record.id}
          />
        </TabPane>
        <TabPane tab="我的申请" key="1">
          <Table
            {...tableProps}
            pagination={{
              ...tableProps.pagination,
              showTotal: total => t`Total ${total} Items`,
            }}
            bordered
            scroll={{ x: 1200 }}
            columns={columns}
            simple
            rowKey={record => record.id}
          />
        </TabPane>
        <TabPane tab="已审批" key="2">
          <Table
            {...tableProps}
            pagination={{
              ...tableProps.pagination,
              showTotal: total => t`Total ${total} Items`,
            }}
            bordered
            scroll={{ x: 1200 }}
            columns={columns}
            simple
            rowKey={record => record.id}
          />
        </TabPane>
        <TabPane tab={"待审批"} key="3">
          <Table
            {...tableProps}
            pagination={{
              ...tableProps.pagination,
              showTotal: total => t`Total ${total} Items`,
            }}
            bordered
            scroll={{ x: 1200 }}
            columns={columns}
            simple
            rowKey={record => record.id}
          />
        </TabPane>
      </Tabs>
    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
  onClickTab: PropTypes.func,
}

export default List

