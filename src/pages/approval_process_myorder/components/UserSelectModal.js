import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Modal,
  Select,
  Button
} from 'antd'
const FormItem = Form.Item

class UserSelectModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };



  handleOk = () => {
    const { onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        onOk({userId: values.userId});
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  handleCancel = () => {
    const { onCancel } = this.props
    onCancel();
  }

  render() {
    const { item = {}, userOptions=[], ...modalProps } = this.props
    const selectOptions = [];
    userOptions.forEach(user=>{
      selectOptions.push(<Select.Option value={user.id}>{user.username}</Select.Option>)
    })

    return (
      <Modal width={'400px'} {...modalProps}
             footer={[
               <Button key="back"  onClick={()=>this.handleOk()} >
                 确认
               </Button>,
               <Button onClick={this.handleCancel}>
                 取消
               </Button>,
             ]}
      >

        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">
          <FormItem name='userId' rules={[{ required: true }]}>
            <Select>
              {selectOptions}
            </Select>
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

UserSelectModal.propTypes = {
  onApproval: PropTypes.func,
  userOptions: PropTypes.any
}

export default UserSelectModal
