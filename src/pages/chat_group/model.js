import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryChatGroupList, updateChatGroup, createChatGroup,deleteChatGroup } = api
/**
 *
   //相关api信息
 queryChatGroupList: `GET ${prefix}/admin/chatGroup/list`,
 updateChatGroup: `PUT ${prefix}/admin/chatGroup`,
 createChatGroup: `POST ${prefix}/admin/chatGroup`,
 deleteChatGroup: `DELETE ${prefix}/admin/chatGroup/:ids`,
 queryChatGroupDetails: `GET ${prefix}/admin/chatGroup/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'chat_group',
  state: {
    options: {
      type: {
           '2': '群聊',
           '3': '群组',
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/chat_group').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryChatGroupList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createChatGroup, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateChatGroup, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteChatGroup, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})