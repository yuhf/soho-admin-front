import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import QuillEditor from "../../../components/Editor/QuillEditor";
import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ChatGroupModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
          avatar:((values.avatar?.fileList || values.avatar)?.map(value => {
          if(value.url) return value.url;
          if(value.response.payload) return value.response.payload;
         }) || []).join(";"),
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props
    const typeOptionData = Object.keys(options.type).map((k,index)=>{
      return {value: parseInt(k), label: options.type[k]}
    })
    let initData = {
                    ...item};
    //初始化时间处理
   initData['avatar'] = (initData.avatar?.split(';').map((item, key) => {
        return {
          uid: -key,
          key: item,
          name: item,
          url: item
        }
      }) || []);
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
         <FormItem  name='title' rules={[{ required: 0 }]}            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={120} />
          </FormItem>
         <FormItem  name='type' rules={[{ required: 0 }]}            label={t`Type`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Type`}
             optionFilterProp="children"
             options={typeOptionData}
             disabled={false}
             />
          </FormItem>
         <FormItem  name='masterChatUid' rules={[{ required: 0 }]}            label={t`Master Chat Uid`} hasFeedback {...formItemLayout}>
            <InputNumber
 max={11} step="1"  style={{width:200}}             disabled={false}
 />
          </FormItem>
         <FormItem  name='avatar' rules={[{ required: 0 }]}             valuePropName={"defaultFileList"} label={t`Avatar`} hasFeedback {...formItemLayout}>
           <Upload listType="picture-card"
                   // defaultFileList={defaultFileList}
                   name="avatar"
                   action={createUserAvatar}
                   disabled={false}
                   // onChange={this.handleChange}
                   headers={{Authorization:store.get('token')}}
                   maxCount={1}>
             <div>
               <PlusOutlined />
               <div style={{ marginTop: 8 }}>Upload</div>
             </div>
           </Upload>
          </FormItem>
         <FormItem  name='introduction' rules={[{ required: 0 }]}            label={t`Introduction`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={500} />
          </FormItem>
         <FormItem  name='proclamation' rules={[{ required: 0 }]}            label={t`Proclamation`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={500} />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

ChatGroupModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default ChatGroupModal
