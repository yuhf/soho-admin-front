import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import draftToHtml from "draftjs-to-html";
import draft from "draft-js";
import DraftEditor from "../../../components/Editor";
import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const {ContentState} = draft
const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ExampleCategoryModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
          img:((values.img?.fileList || values.img)?.map(value => {
          if(value.url) return value.url;
          if(value.response.payload) return value.response.payload;
         }) || []).join(";"),
          onlyDate: values.onlyDate ? moment(values.onlyDate).format(dateFormat) : null,
          payDatetime: values.payDatetime ? moment(values.payDatetime).format(datetimeFormat) : null,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props
    let initData = {
...item};
    //初始化时间处理
   initData['img'] = (initData.img?.split(';').map((item, key) => {
        return {
          uid: -key,
          key: item,
          name: item,
          url: item
        }
      }) || []);
    if(item.onlyDate) initData['onlyDate'] = moment(item.onlyDate,dateFormat)
    if(item.payDatetime) initData['payDatetime'] = moment(item.payDatetime,datetimeFormat)
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
         <FormItem  name='img' rules={[{ required: 0 }]}             valuePropName={"defaultFileList"} label={t`Img`} hasFeedback {...formItemLayout}>
           <Upload listType="picture-card"
                   // defaultFileList={defaultFileList}
                   name="avatar"
                   action={createUserAvatar}
                   // onChange={this.handleChange}
                   headers={{Authorization:store.get('token')}}
                   maxCount={3}>
             <div>
               <PlusOutlined />
               <div style={{ marginTop: 8 }}>Upload</div>
             </div>
           </Upload>
          </FormItem>
         <FormItem  name='onlyDate' rules={[{ required: 0 }]}            label={t`Only Date`} hasFeedback {...formItemLayout}>
            <DatePicker format={dateFormat} showNow showTime={{ format: dateFormat }} />
          </FormItem>
         <FormItem  name='parentId' rules={[{ required: 0 }]}            label={t`Parent Category`} hasFeedback {...formItemLayout}>
       <TreeSelect
             showSearch
             style={{ width: '100%' }}
             // value={value}
             dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
             placeholder="Please select"
             allowClear
             treeDefaultExpandAll
             // onChange={onChange}
             treeData={addRootNode(trees?.parentId)}
           />
          </FormItem>
         <FormItem  name='payDatetime' rules={[{ required: 0 }]}            label={t`Pay Datetime`} hasFeedback {...formItemLayout}>
            <DatePicker format={datetimeFormat} showNow showTime={{ format: datetimeFormat }} />
          </FormItem>
         <FormItem  name='title' rules={[{ required: 0 }]}            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input maxLength={145} />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

ExampleCategoryModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default ExampleCategoryModal
