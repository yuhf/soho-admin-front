import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryExampleCategoryList, updateExampleCategory, createExampleCategory,deleteExampleCategory,queryExampleCategoryTree } = api
/**
 *
   //相关api信息
 queryExampleCategoryList: `GET ${prefix}/admin/exampleCategory/list`,
 updateExampleCategory: `PUT ${prefix}/admin/exampleCategory`,
 createExampleCategory: `POST ${prefix}/admin/exampleCategory`,
 deleteExampleCategory: `DELETE ${prefix}/admin/exampleCategory/:ids`,
 queryExampleCategoryDetails: `GET ${prefix}/admin/exampleCategory/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'example_category',
  state: {
    options: {    },
    trees: {
           parentId: [],
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/example_category').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
          dispatch({
            type: 'queryExampleCategoryTree',
            payload: {},
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryExampleCategoryList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createExampleCategory, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateExampleCategory, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteExampleCategory, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
   *queryExampleCategoryTree({payload}, {call, put}) {
      const data = yield call(queryExampleCategoryTree, payload);
      if(data.success) {
        yield put({
          type: 'querySuccessTree',
          payload: {
            parentId: data.payload,
          }
        })
      } else {
        throw data;
      }
   },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})