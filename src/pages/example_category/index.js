import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import ExampleCategoryModal from "./components/Modal";
import Filter from "./components/Filter"

import SohoTree from "./components/SohoTree";

@connect(({ example_category, loading }) => ({ example_category, loading }))
class ExampleCategory extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { example_category, loading, location, dispatch } = this.props
    const { list, pagination } = example_category
    const { query, pathname } = location
    return {
      options: example_category.options,
      trees: example_category.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['example_category/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'example_category/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'example_category/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'example_category/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, example_category, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = example_category

    return {
      options: example_category.options,
      trees: example_category.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`example_category/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `example_category/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'example_category/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, example_category} = this.props
    const {query,pathname} = location
    return {
      options: example_category.options,
      trees: example_category.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'example_category/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

<SohoTree {...this.listProps} />
        <ExampleCategoryModal {...this.modalProps} />
      </Page>
    )
  }
}

ExampleCategory.propTypes = {
  example_category: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default ExampleCategory
