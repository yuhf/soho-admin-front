import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'umi'
import {Button, Row, Input, Form, Image, Col, Alert} from 'antd'
import { GlobalFooter } from 'components'
import { GithubOutlined } from '@ant-design/icons'
import { t, Trans } from "@lingui/macro"
import { setLocale } from 'utils'
import config from 'utils/config'
import styles from './index.less'
import api from "../../services/api";

const FormItem = Form.Item

@connect(({ loading, dispatch,login }) => ({ loading, dispatch, login }))
class Login extends PureComponent {
  state = {
      src: api.loginCaptcha + "?v=" + Date.now()
    }
  render() {

    const src = this.state.src;
    const { dispatch, loading, login } = this.props
    const { message } = login
    const handleOk = values => {
      dispatch({ type: 'login/login', payload: values })

    }

    const clickCaptcha = e => {
      console.log(e)
      this.setState({src: api.loginCaptcha + "?v=" + Date.now()})
      //e.render();
    }

    let footerLinks = [
      {
        key: 'github',
        title: <GithubOutlined />,
        href: 'https://github.com/zuiidea/antd-admin',
        blankTarget: true,
      },
    ]

    if (config.i18n) {
      footerLinks = footerLinks.concat(
        config.i18n.languages.map(item => ({
          key: item.key,
          title: (
            <span onClick={setLocale.bind(null, item.key)}>{item.title}</span>
          ),
        }))
      )
    }

    let Message = ""
    console.log(message)
    if(message != null && message != '') {
      Message = <Alert style={{marginBottom: '5px', height: '20px'}} message={message} type="error" />
    }

    return (
      <>
        <Fragment>
          <div className={styles.form}>
            <div className={styles.logo}>
              <img alt="logo" src={config.logoPath} />
              <span>{config.siteName}</span>
            </div>
            {Message}

            <Form
              onFinish={handleOk}
            >
              <FormItem name="username"
                        rules={[{ required: true }]} hasFeedback>
                <Input
                  placeholder={t`Username`}
                />
              </FormItem>
              <Trans id="Password" render={({translation}) => (
                <FormItem name="password" rules={[{ required: true }]} hasFeedback>
                  <Input type='password' placeholder={translation} required />
                </FormItem>)}
              />
              {login.config && login.config.useCaptcha &&
                <FormItem name="captcha"
                          rules={[{required: true}]} hasFeedback>
                  <Row>
                    <Col span={12}>
                      <Input
                        placeholder={t`Captcha`}
                      />
                    </Col>
                    <Col span={1}/>
                    <Col span={8}>
                      <Image onClick={clickCaptcha} preview={false} src={src}/>
                    </Col>
                    <Col span={3}/>
                  </Row>
                </FormItem>
              }

              <Row>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading.effects.login}
                >
                  <Trans>Sign in</Trans>
                </Button>
                <p>
                <span className="margin-right">
                  <Trans>Username</Trans>
                  ：guest
                </span>
                  <span>
                  <Trans>Password</Trans>
                  ：guest
                </span>
                </p>
              </Row>
            </Form>
          </div>
          <div className={styles.footer}>
            <GlobalFooter links={footerLinks} copyright={config.copyright} />
          </div>
        </Fragment>


      </>

    )
  }
}

Login.propTypes = {
  form: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
  login: PropTypes.object,
}

export default Login
