import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryPayOrderList, updatePayOrder, createPayOrder,deletePayOrder,queryPayInfoOptions } = api
/**
 *
   //相关api信息
 queryPayOrderList: `GET ${prefix}/admin/payOrder/list`,
 updatePayOrder: `PUT ${prefix}/admin/payOrder`,
 createPayOrder: `POST ${prefix}/admin/payOrder`,
 deletePayOrder: `DELETE ${prefix}/admin/payOrder/:ids`,
 queryPayOrderDetails: `GET ${prefix}/admin/payOrder/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'pay_order',
  state: {
    options: {
      payId: {
      },
      status: {
           '1': '待支付',
           '30': '支付失败',
           '20': '支付成功',
           '10': '已扫码',
      },
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/pay_order').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
          dispatch({
            type: 'queryPayIdOptions',
            payload: {},
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryPayOrderList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createPayOrder, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updatePayOrder, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deletePayOrder, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
    *queryPayIdOptions({ payload }, { call, put }) {
      const data = yield call(queryPayInfoOptions, payload)
      if (data.success) {
        yield put({
          type: 'queryOptions',
          payload: {
              payId: data.payload
          },
        })
      } else {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
