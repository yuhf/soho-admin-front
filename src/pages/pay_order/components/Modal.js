import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
const {createUserAvatar} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const dateTimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class PayOrderModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          //时间值处理
          payedTime: values.payedTime ? moment(values.payedTime).format(dateTimeFormat) : null,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options, ...modalProps } = this.props

    const payIdOptionData = Object.keys(options.payId).map((k,index)=>{
      return {value: parseInt(k), label: options.payId[k]}
    })
    const statusOptionData = Object.keys(options.status).map((k,index)=>{
      return {value: parseInt(k), label: options.status[k]}
    })
    let initData = {status: 1,...item};
    //初始化时间处理
    if(item.payedTime) initData['payedTime'] = moment(item.payedTime,dateTimeFormat)
    return (

      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">

         <FormItem  ftable='PayInfo' name='payId' rules={[{ required: true }]}
            label={t`Pay Id`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Pay Id`}
             optionFilterProp="children"
             options={payIdOptionData}
             />
          </FormItem>
         <FormItem  ftable='null' name='orderNo' rules={[{ required: true }]}
            label={t`Order No`} hasFeedback {...formItemLayout}>
            <Input maxLength={128} />
          </FormItem>
         <FormItem  ftable='null' name='trackingNo' rules={[{ required: true }]}
            label={t`Tracking No`} hasFeedback {...formItemLayout}>
            <Input maxLength={128} />
          </FormItem>
         <FormItem  ftable='null' name='transactionId' rules={[{ required: false }]}
            label={t`Transaction Id`} hasFeedback {...formItemLayout}>
            <Input maxLength={128} />
          </FormItem>
         <FormItem  ftable='null' name='amount' rules={[{ required: true }]}
            label={t`Amount`} hasFeedback {...formItemLayout}>
            <InputNumber max={9999999.99} step="0.01"  style={{width:200}} />
          </FormItem>
         <FormItem  ftable='null' name='status' rules={[{ required: true }]}
            label={t`Status`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Status`}
             optionFilterProp="children"
             options={statusOptionData}
             />
          </FormItem>
         <FormItem  ftable='null' name='payedTime' rules={[{ required: false }]}
            label={t`Payed Time`} hasFeedback {...formItemLayout}>
            <DatePicker format={dateTimeFormat} showNow showTime={{ format: dateTimeFormat }} />
          </FormItem>
         <FormItem  ftable='null' name='notifyUrl' rules={[{ required: false }]}
            label={t`Notify Url`} hasFeedback {...formItemLayout}>
            <Input maxLength={500} />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

PayOrderModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default PayOrderModal

