import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
const {createUserAvatar} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const dateTimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class PayInfoModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          //时间值处理
          accountImg:((values.accountImg?.fileList || values.accountImg)?.map(value => {
          if(value.url) return value.url;
          if(value.response.payload) return value.response.payload;
         }) || []).join(";"),
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options, ...modalProps } = this.props

    const clientTypeOptionData = Object.keys(options.clientType).map((k,index)=>{
      return {value: k, label: options.clientType[k]}
    })
    const statusOptionData = Object.keys(options.status).map((k,index)=>{
      return {value: parseInt(k), label: options.status[k]}
    })
    const adapterNameOptionData = Object.keys(options.adapterName).map((k,index)=>{
      return {value: k, label: options.adapterName[k]}
    })
    let initData = {platform: 'soho',...item};
    //初始化时间处理
   initData['accountImg'] = (initData.accountImg?.split(';').map((item, key) => {
        return {
          uid: -key,
          key: item,
          name: item,
          url: item
        }
      }) || []);
    return (

      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">

         <FormItem name='title' rules={[{ required: true, min: 3, max: 46 }]}
            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='name' rules={[{ required: true, min: 0, max: 45 }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='accountAppId' rules={[{ required: false, min: 0, max: 45 }]}
            label={t`Account App Id`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='accountId' rules={[{ required: true, min: 0, max: 45 }]}
            label={t`Account Id`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='accountPrivateKey' rules={[{ required: true, min: 0, max: 6000 }]}
            label={t`Account Private Key`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='accountSerialNumber' rules={[{ required: false, min: 0, max: 128 }]}
            label={t`Account Serial Number`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='accountPublicKey' rules={[{ required: true, min: 0, max: 6000 }]}
            label={t`Account Public Key`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='accountImg' rules={[{ required: true }]}
             valuePropName={"defaultFileList"} label={t`Account Img`} hasFeedback {...formItemLayout}>
           <Upload listType="picture-card"
                   // defaultFileList={defaultFileList}
                   name="avatar"
                   action={createUserAvatar}
                   // onChange={this.handleChange}
                   headers={{Authorization:store.get('token')}}
                   maxCount={1}>
             <div>
               <PlusOutlined />
               <div style={{ marginTop: 8 }}>Upload</div>
             </div>
           </Upload>
          </FormItem>
         <FormItem name='clientType' rules={[{ required: true }]}
            label={t`Client Type`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Client Type`}
             optionFilterProp="children"
             options={clientTypeOptionData}
             />
          </FormItem>
         <FormItem name='platform' rules={[{ required: true, min: 0, max: 45 }]}
            label={t`Platform`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='status' rules={[{ required: true }]}
            label={t`Status`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Status`}
             optionFilterProp="children"
             options={statusOptionData}
             />
          </FormItem>
         <FormItem name='adapterName' rules={[{ required: true }]}
            label={t`Adapter Name`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Adapter Name`}
             optionFilterProp="children"
             options={adapterNameOptionData}
             />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

PayInfoModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default PayInfoModal

