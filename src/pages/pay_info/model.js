import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryPayInfoList, updatePayInfo, createPayInfo,deletePayInfo } = api
/**
 *
   //相关api信息
 queryPayInfoList: `GET ${prefix}/admin/payInfo/list`,
 updatePayInfo: `PUT ${prefix}/admin/payInfo`,
 createPayInfo: `POST ${prefix}/admin/payInfo`,
 deletePayInfo: `DELETE ${prefix}/admin/payInfo/:ids`,
 queryPayInfoDetails: `GET ${prefix}/admin/payInfo/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'pay_info',
  state: {
    options: {
      clientType: {
           '1': ' web',
           '2': ' wap',
           '3': 'app',
           '4': '微信公众号',
           '5': '微信小程序',
      },
      status: {
           '0': '禁用',
           '1': '启用',
      },
      adapterName: {
           'wechat_h5': 'wechat_h5',
           'alipay_wap': 'alipay_wap',
           'wechat_jsapi': 'wechat_jsapi',
           'alipay_web': 'alipay_web',
           'wechat_app': 'wechat_app',
           'wechat_native': 'wechat_native',
      },
    }
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/pay_info').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryPayInfoList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createPayInfo, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updatePayInfo, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deletePayInfo, {ids:payload})
      if (!data.success) {
        throw data
      }
    }
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
