import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select} from 'antd'
import {t, Trans} from "@lingui/macro"
import {  } from 'antd';
import IconSelect from "../../../components/Icon/IconSelect";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class TreeNodeModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: []
  };

  onChange = value => {
    // console.log("]]]]]]]]]]]]]]]]")
    // console.log(value)
    // this.setState({ value });
  };

  handleOk = () => {
    const { item = {}, onOk } = this.props
    //
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          key: item.key,
        }
        if(item.id) {
          data.id = item.id
        }
        if(item.breadcrumbParentId) {
          data.breadcrumbParentId = item.breadcrumbParentId
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form, ...modalProps } = this.props
    return (
      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">
          <FormItem name='name' rules={[{ required: true }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          <FormItem name='route' rules={[{ required: true }]}
            label={t`Route`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          <FormItem name='visible' rules={[{ required: true }]}
            label={t`Visible`} hasFeedback {...formItemLayout}>
            <Radio.Group>
              <Radio value={0}>
                {t`No Display`}
              </Radio>
              <Radio value={1}>
                {t`Display`}
              </Radio>
            </Radio.Group>
          </FormItem>
          <FormItem name={'type'} rules={[{required:true}]} label={t`Type`} hasFeedback {...formItemLayout} >
            <Radio.Group>
              <Radio value={1}>
                前端资源
              </Radio>
              <Radio value={2}>
                后端资源
              </Radio>
            </Radio.Group>
          </FormItem>
          <FormItem name='remarks' rules={[{ required: false }]}
            label={t`Remarks`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          <FormItem name='sort' rules={[{ required: false }]}
            label={t`Sort`} hasFeedback {...formItemLayout}>
            <InputNumber defaultValue={100} min={0} max={1000}/>
          </FormItem>
          <FormItem name='zhName' label={t`China name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          <FormItem name="iconName" rules={[{ required: true}]}
            label={t`Icon name`} hasFeedback {...formItemLayout}>
            {/*<Input />*/}
            <IconSelect />
          </FormItem>

        </Form>
      </Modal>
    )
  }
}

TreeNodeModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default TreeNodeModal
