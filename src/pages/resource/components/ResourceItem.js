import {PureComponent} from "react";
import {Button, Col, Row, Modal} from "antd";
import {AppstoreAddOutlined, DeleteOutlined, EditOutlined} from "@ant-design/icons";
import {t} from "@lingui/macro";

const { confirm } = Modal

/**
 * 资源单元
 */
class ResourceItem extends PureComponent {
  handleWriteClick = (record, e) => {
    // const { onDeleteItem, onEditItem } = this.props
    console.log("///////////////////////////")
    // console.log(record)
     console.log(e)
    console.log(this.props)
    // if (e.key === '1') {
    //   // onEditItem(record)
    // } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
         // onDeleteItem(record.id)
        },
      })
    // }
  }
  render() {
  const {title, item,onDelete, onUpdate, onCreate} = this.props;
    return <Row>
      <Col span={16}>{title}</Col>
      <Col span={1}><Button type="text" itemID={item.id} type={"create"} onClick={e=>onCreate(item.id)}  block><AppstoreAddOutlined /></Button></Col>
      <Col span={1}><Button type="text" itemID={item.id} type={"edit"} onClick={e=>onUpdate(item)}  block><EditOutlined /></Button></Col>
      <Col span={1}><Button type="text" itemID={item.id} type={"delete"} onClick={e=>onDelete(item)} block><DeleteOutlined /></Button></Col>
    </Row>;
  }
}

export default ResourceItem;
