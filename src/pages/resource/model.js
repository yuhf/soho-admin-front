import modelExtend from "dva-model-extend";
import {pageModel} from "utils/model";
import {pathToRegexp} from "path-to-regexp";
import api from 'api'
import {getLocale} from "../../utils";

const {queryResourceTree, queryResourceDetails, updateResource, createResource, deleteResource, syncResource} = api

export default modelExtend(pageModel, {
  namespace: 'resource',

  state: {
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/resource').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    //查询资源树
    *query({ payload = {} }, { call, put }) {
      let language = getLocale()
      const data = yield call(queryResourceTree, {...payload, language})
      if (data.success) {
        yield put({
          type: 'queryTreeSuccess',
          payload: {
            tree: data.payload,
          },
        })
      }
    },

    // eslint-disable-next-line no-empty-pattern
    *queryDetails({payload}, {call, put}) {
      let detailsData = {}
      let item = payload.item
      //eslint-disable-next-line no-undef
      if(item.id) {
        // eslint-disable-next-line no-undef
        const data = yield call(queryResourceDetails, {id:item.id})
        if(data.success) {
          detailsData = data.payload;
        }
      } else {
        detailsData.parentId = item.parentId;
      }
      yield put({
        type: "showModal",
        payload: {
          currentItem: detailsData,
          modalType: payload.modalType
        }
      });
    },

    //更新资源
    *update({payload}, {call, put}) {
      const resource = yield call(updateResource, payload)
      if(resource.success) {
        yield put({
          type: "hideModal",
        })
      } else {
        throw resource
      }
    },

    //创建资源
    *create({payload}, {call, put}) {
      const resource = yield call(createResource, payload)
      if(resource.success) {
        yield put({
          type: "hideModal",
        })
      } else {
        throw resource
      }
    },

    //删除资源
    *delete({payload}, {call, put}) {
      const data = yield call(deleteResource, {id:payload.id})
      if(data.success) {
        //nothing todo
      } else {
        throw data
      }
    },
    //sync menu
    *syncResource({payload}, {call, put}) {
      const data = yield  call(syncResource, {})
      if(data.success) {
        return data;
      } else {
        throw data;
      }
    }
  },

  reducers: {
    queryTreeSuccess(state, { payload }) {
      const { tree } = payload
      return {
        ...state,
        tree,
      }
    },

    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },

});
