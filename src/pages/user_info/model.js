import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryUserInfoList, updateUserInfo, createUserInfo,deleteUserInfo } = api
/**
 *
   //相关api信息
 queryUserInfoList: `GET ${prefix}/admin/userInfo/list`,
 updateUserInfo: `PUT ${prefix}/admin/userInfo`,
 createUserInfo: `POST ${prefix}/admin/userInfo`,
 deleteUserInfo: `DELETE ${prefix}/admin/userInfo/:ids`,
 queryUserInfoDetails: `GET ${prefix}/admin/userInfo/:id`,

原始数据解析信息
name: id   capitalName: Id capitalKeyName: Id   type: long   frontType: number     frontMax: 99999999999       frontStep:      dbType: int       dbUnsigned: false       comment: ID     length: 11       scale: 0     frontLength: 12     defaultValue: null       isNotNull: true     specification: int(11)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: username   capitalName: Username capitalKeyName: Username   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 用户名     length: 45       scale: 0     frontLength: 45     defaultValue: null       isNotNull: false     specification: varchar(45)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: email   capitalName: Email capitalKeyName: Email   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 邮箱     length: 255       scale: 0     frontLength: 255     defaultValue: null       isNotNull: false     specification: varchar(255)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: phone   capitalName: Phone capitalKeyName: Phone   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 手机号     length: 15       scale: 0     frontLength: 15     defaultValue: null       isNotNull: false     specification: varchar(15)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: password   capitalName: Password capitalKeyName: Password   type: String   frontType: password     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 密码;;frontType:password,ignoreInList:true     length: 255       scale: 0     frontLength: 255     defaultValue: null       isNotNull: false     specification: varchar(255)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: avatar   capitalName: Avatar capitalKeyName: Avatar   type: String   frontType: upload     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 头像;;frontType:upload,uploadCount:1,ignoreInList:true     length: 255       scale: 0     frontLength: 255     defaultValue: null       isNotNull: false     specification: varchar(255)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: status   capitalName: Status capitalKeyName: Status   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 状态;0:禁用,1:活跃;frontType:select     length: 4       scale: 0     frontLength: 4     defaultValue: 1       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: age   capitalName: Age capitalKeyName: Age   type: long   frontType: number     frontMax: 9999       frontStep:      dbType: tinyint       dbUnsigned: true       comment: 年龄;;frontName:年龄     length: 4       scale: 0     frontLength: 5     defaultValue: null       isNotNull: false     specification: tinyint(4) unsigned     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: sex   capitalName: Sex capitalKeyName: Sex   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 性别;0:女,1:男;frontType:select,frontName:性别     length: 4       scale: 0     frontLength: 4     defaultValue: null       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: updatedTime   capitalName: Updated Time capitalKeyName: UpdatedTime   type: java.sql.Timestamp   frontType: text     frontMax: null       frontStep:      dbType: datetime       dbUnsigned: false       comment: 更新时间     length: -1       scale: 0     frontLength: -1     defaultValue: null       isNotNull: false     specification: datetime     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: createdTime   capitalName: Created Time capitalKeyName: CreatedTime   type: java.sql.Timestamp   frontType: upload     frontMax: null       frontStep:      dbType: datetime       dbUnsigned: false       comment: 头像;;frontType:upload,uploadCount:1     length: -1       scale: 0     frontLength: -1     defaultValue: null       isNotNull: false     specification: datetime     dbForeignName:      capitalForeignName:        javaForeignName:         *
 */
export default modelExtend(pageModel, {
  namespace: 'user_info',
  state: {
    options: {
      status: {
           '0': '禁用',
           '1': '活跃',
      },
      sex: {
           '0': '女',
           '1': '男',
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/user_info').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryUserInfoList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createUserInfo, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateUserInfo, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteUserInfo, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
