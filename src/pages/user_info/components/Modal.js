import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import draftToHtml from "draftjs-to-html";
import draft from "draft-js";import DraftEditor from "../../../components/Editor";import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const {ContentState} = draft
const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class UserInfoModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          //时间值处理
          avatar:((values.avatar?.fileList || values.avatar)?.map(value => {
          if(value.url) return value.url;
          if(value.response.payload) return value.response.payload;
         }) || []).join(";"),
          createdTime:((values.createdTime?.fileList || values.createdTime)?.map(value => {
          if(value.url) return value.url;
          if(value.response.payload) return value.response.payload;
         }) || []).join(";"),
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props

    const statusOptionData = Object.keys(options.status).map((k,index)=>{
      return {value: parseInt(k), label: options.status[k]}
    })
    const sexOptionData = Object.keys(options.sex).map((k,index)=>{
      return {value: parseInt(k), label: options.sex[k]}
    })
    let initData = {status: 1,...item};
    //初始化时间处理
   initData['avatar'] = (initData.avatar?.split(';').map((item, key) => {
        return {
          uid: -key,
          key: item,
          name: item,
          url: item
        }
      }) || []);
   initData['createdTime'] = (initData.createdTime?.split(';').map((item, key) => {
        return {
          uid: -key,
          key: item,
          name: item,
          url: item
        }
      }) || []);
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">

         <FormItem  name='username' rules={[{ required: false }]}
            label={t`Username`} hasFeedback {...formItemLayout}>
            <Input maxLength={45} />
          </FormItem>
         <FormItem  name='email' rules={[{ required: false }]}
            label={t`Email`} hasFeedback {...formItemLayout}>
            <Input maxLength={255} />
          </FormItem>
         <FormItem  name='phone' rules={[{ required: false }]}
            label={t`Phone`} hasFeedback {...formItemLayout}>
            <Input maxLength={15} />
          </FormItem>
         <FormItem  name='password' rules={[{ required: false }]}
            label={t`Password`} hasFeedback {...formItemLayout}>
            <Input.Password />
          </FormItem>
         <FormItem  name='avatar' rules={[{ required: false }]}
             valuePropName={"defaultFileList"} label={t`Avatar`} hasFeedback {...formItemLayout}>
           <Upload listType="picture-card"
                   // defaultFileList={defaultFileList}
                   name="avatar"
                   action={createUserAvatar}
                   // onChange={this.handleChange}
                   headers={{Authorization:store.get('token')}}
                   maxCount={1}>
             <div>
               <PlusOutlined />
               <div style={{ marginTop: 8 }}>Upload</div>
             </div>
           </Upload>
          </FormItem>
         <FormItem  name='status' rules={[{ required: false }]}
            label={t`Status`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Status`}
             optionFilterProp="children"
             options={statusOptionData}
             />
          </FormItem>
         <FormItem  name='age' rules={[{ required: false }]}
            label={t`年龄`} hasFeedback {...formItemLayout}>
            <InputNumber max={9999}  style={{width:200}} />
          </FormItem>
         <FormItem  name='sex' rules={[{ required: false }]}
            label={t`性别`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Sex`}
             optionFilterProp="children"
             options={sexOptionData}
             />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

UserInfoModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default UserInfoModal

