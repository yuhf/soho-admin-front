import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import UserInfoModal from "./components/Modal";
import Filter from "./components/Filter"


@connect(({ user_info, loading }) => ({ user_info, loading }))
class UserInfo extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { user_info, loading, location, dispatch } = this.props
    const { list, pagination } = user_info
    const { query, pathname } = location
    return {
      options: user_info.options,
      trees: user_info.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['user_info/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'user_info/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'user_info/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'example_category/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, user_info, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = user_info

    return {
      options: user_info.options,
      trees: user_info.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`user_info/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `user_info/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'user_info/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, user_info} = this.props
    const {query,pathname} = location
    return {
      options: user_info.options,
      trees: user_info.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'user_info/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <UserInfoModal {...this.modalProps} />
      </Page>
    )
  }
}

UserInfo.propTypes = {
  user_info: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default UserInfo

