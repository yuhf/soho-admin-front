import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import LotProductModal from "./components/Modal";
import Filter from "./components/Filter"

@connect(({ lot_product, loading }) => ({ lot_product, loading }))
class LotProduct extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }

  get listProps() {
    const { lot_product, loading, location, dispatch } = this.props
    const { list, pagination,mapModel } = lot_product
    const { query, pathname } = location
    return {
      mapModel,
      pagination,
      dataSource: list,
      loading: loading.effects['lot_product/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'lot_product/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'lot_product/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, lot_product, loading } = this.props
    const { currentItem, modalVisible, modalType,lotModelOptions } = lot_product

    return {
      lotModelOptions,
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`lot_product/${modalType}`],
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `lot_product/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'lot_product/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch,lot_product } = this.props
    const {query,pathname} = location
    const {lotModelOptions} = lot_product
    return {
      lotModelOptions,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'lot_product/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <LotProductModal {...this.modalProps} />
      </Page>
    )
  }
}

LotProduct.propTypes = {
  lot_product: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default LotProduct

