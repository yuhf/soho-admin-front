import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class AdminContentCategoryModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, parentId, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          parentId: parentId
        }

        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {},parentId, onOk, form, ...modalProps } = this.props
    return (

      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

         <FormItem name='name' rules={[{ required: true }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='description' rules={[{ required: true }]}
            label={t`Description`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='keyword' rules={[{ required: true }]}
            label={t`Keyword`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='content' rules={[{ required: true }]}
            label={t`Content`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='order' rules={[{ required: true }]}
            label={t`Order`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          <FormItem name='isDisplay' rules={[{ required: true }]}
                    label={t`Is Display`} hasFeedback {...formItemLayout}>
            <Radio.Group>
              <Radio value={0}>
                {t`No`}
              </Radio>
              <Radio value={1}>
                {t`Yes`}
              </Radio>
            </Radio.Group>
          </FormItem>

        </Form>
      </Modal>
    )
  }
}

AdminContentCategoryModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default AdminContentCategoryModal

