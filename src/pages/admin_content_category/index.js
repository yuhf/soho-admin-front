import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import AdminContentCategoryModal from "./components/Modal";
import ResourceItem from "./components/ResourceItem";
import {Modal, Tree} from "antd";
const { confirm } = Modal

@connect(({ admin_content_category, loading }) => ({ admin_content_category, loading }))
class AdminContentCategory extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get treeProps() {
    const { admin_content_category, loading, location, dispatch } = this.props
    const { list, pagination } = admin_content_category
    const { query, pathname } = location
    return {
      pagination,
      dataSource: list,
      loading: loading.effects['admin_content_category/queryCategoryTree'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        let athis = this;
        confirm({
          title: t`Are you sure delete this record?`,
          onOk() {
            dispatch({
              type: 'admin_content_category/delete',
              payload: id,
            }).then(() => {
              athis.handleTabClick({
                page:
                  list.length === 1 && pagination.current > 1
                    ? pagination.current - 1
                    : pagination.current,
              })
            })
          },
        })

      },
      onEditItem(item) {
        //queryDetails
        dispatch({
          type: 'admin_content_category/queryDetails',
          payload: {
            id: item.id,
          },
        }).then(()=>{
          dispatch({
            type: 'admin_content_category/showModal',
            payload: {
              modalType: 'update',
              currentItem: item,
              parentId: null
            },
          })
        })

      },

      onAddItem(id) {
        dispatch({
          type: 'admin_content_category/showModal',
          payload: {
            parentId: id,
            modalType: 'create',
          },
        })
      },

    }
  }

  get modalProps() {
    const { dispatch, admin_content_category, loading } = this.props
    const { currentItem, item,parentId, modalVisible, modalType,resourceTree,resourceIds } = admin_content_category

    return {
      item: modalType === 'create' ? {} : item,
      parentId,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_content_category/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `admin_content_category/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_content_category/hideModal',
        })
      },
    }
  }

  //获取递归树
  getTreeNode = (item, actions) => {
    //递归获取数据
    function getTree(child) {
      if(child == null) {
        return {}
      }
      const cheildrenArray = [];
      if(child && child.children && child.children.length>0) {
        const children = child.children
        for (let i = 0; i < children.length; i++) {
          let son = getTree(children[i])
          if(son) {
            cheildrenArray.push(son)
          }
        }
      }
      return {
        title: <ResourceItem {...actions} item={{id:child && child.key !=null ? child.key : null}} title={child.title} />,
        key: child && child.key !=null ? child.key : null,
        blockNode: true,
        children: cheildrenArray,
      };
    }

    return getTree(item);
  };

  render() {
    const {admin_content_category} = this.props;
    const {categoryTree} = admin_content_category;
    const allTree = this.getTreeNode({"key":0, children: categoryTree, "title":"顶级栏目"},this.treeProps)

    return (
      <Page inner>
        <Tree
          defaultExpandAll={true}
          checkable
          blockNode={true}
          autoExpandParent={true}
          defaultExpandedKeys={[]}
          defaultSelectedKeys={[]}
          defaultCheckedKeys={[]}
          onSelect={this.onSelect}
          onCheck={this.onCheck}
          treeData={[allTree]}
        />
        <AdminContentCategoryModal {...this.modalProps} />
      </Page>
    )
  }
}

AdminContentCategory.propTypes = {
  admin_content_category: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default AdminContentCategory

