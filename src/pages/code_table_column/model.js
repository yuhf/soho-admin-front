import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryCodeTableColumnList, updateCodeTableColumn, createCodeTableColumn,deleteCodeTableColumn,queryCodeTableOptions } = api
/**
 *
   //相关api信息
 queryCodeTableColumnList: `GET ${prefix}/admin/codeTableColumn/list`,
 updateCodeTableColumn: `PUT ${prefix}/admin/codeTableColumn`,
 createCodeTableColumn: `POST ${prefix}/admin/codeTableColumn`,
 deleteCodeTableColumn: `DELETE ${prefix}/admin/codeTableColumn/:ids`,
 queryCodeTableColumnDetails: `GET ${prefix}/admin/codeTableColumn/:id`,

原始数据解析信息
name: id   capitalName: Id capitalKeyName: Id   type: long   frontType: number     frontMax: 99999999999       frontStep:      dbType: int       dbUnsigned: false       comment: null     length: 11       scale: 0     frontLength: 12     defaultValue: null       isNotNull: true     specification: int(11)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: tableId   capitalName: Table Id capitalKeyName: TableId   type: long   frontType: select     frontMax: null       frontStep:      dbType: int       dbUnsigned: false       comment: 表ID;;frontType:select,foreign:code_talbe.id~name,frontName:Table Name,isFilter:true     length: 11       scale: 0     frontLength: 11     defaultValue: null       isNotNull: false     specification: int(11)     dbForeignName: code_talbe     capitalForeignName: CodeTalbe       javaForeignName: codeTalbe       原始数据解析信息
name: name   capitalName: Name capitalKeyName: Name   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 表名;;isFilter:true     length: 45       scale: 0     frontLength: 45     defaultValue: null       isNotNull: false     specification: varchar(45)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: title   capitalName: Title capitalKeyName: Title   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 表标题;;isFilter:true     length: 45       scale: 0     frontLength: 45     defaultValue: null       isNotNull: false     specification: varchar(45)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: dataType   capitalName: Data Type capitalKeyName: DataType   type: String   frontType: select     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 数据类型;tinyint,smallint,mediumint,int,bigint,float,double,char,varchar,tinytext,text,mediumtext,longtext,date,time,datetime,timestamp,decimal;frontType:select,isFilter:true     length: 45       scale: 0     frontLength: 45     defaultValue: null       isNotNull: false     specification: varchar(45)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: isPk   capitalName: Is Pk capitalKeyName: IsPk   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 是否主键;0:否,1:是;frontType:select     length: 4       scale: 0     frontLength: 4     defaultValue: null       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: isNotNull   capitalName: Is Not Null capitalKeyName: IsNotNull   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 是否不为空;0:否,1:是;frontType:select     length: 4       scale: 0     frontLength: 4     defaultValue: null       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: isUnique   capitalName: Is Unique capitalKeyName: IsUnique   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 是否无符号;0:否,1:是;frontType:select     length: 4       scale: 0     frontLength: 4     defaultValue: null       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: isAutoIncrement   capitalName: Is Auto Increment capitalKeyName: IsAutoIncrement   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 是否自增;0:否,1:是;frontType:select     length: 4       scale: 0     frontLength: 4     defaultValue: null       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: isZeroFill   capitalName: Is Zero Fill capitalKeyName: IsZeroFill   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 是否0填充;0:否,1:是;frontType:select     length: 4       scale: 0     frontLength: 4     defaultValue: null       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: defaultValue   capitalName: Default Value capitalKeyName: DefaultValue   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 默认值     length: 1000       scale: 0     frontLength: 1000     defaultValue: null       isNotNull: false     specification: varchar(1000)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: length   capitalName: Length capitalKeyName: Length   type: long   frontType: number     frontMax: 99999999999       frontStep:      dbType: int       dbUnsigned: false       comment: 长度     length: 11       scale: 0     frontLength: 12     defaultValue: null       isNotNull: false     specification: int(11)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: scale   capitalName: Scale capitalKeyName: Scale   type: long   frontType: number     frontMax: 99999999999       frontStep:      dbType: int       dbUnsigned: false       comment: 小数点位数     length: 11       scale: 0     frontLength: 12     defaultValue: null       isNotNull: false     specification: int(11)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: comment   capitalName: Comment capitalKeyName: Comment   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 字段注释     length: 3000       scale: 0     frontLength: 3000     defaultValue: null       isNotNull: false     specification: varchar(3000)     dbForeignName:      capitalForeignName:        javaForeignName:         *
 */
export default modelExtend(pageModel, {
  namespace: 'code_table_column',
  state: {
    options: {
      tableId: {
      },
      dataType: {
           'longtext': 'longtext',
           'date': 'date',
           'mediumint': 'mediumint',
           'mediumtext': 'mediumtext',
           'double': 'double',
           'tinytext': 'tinytext',
           'varchar': 'varchar',
           'tinyint': 'tinyint',
           'float': 'float',
           'int': 'int',
           'smallint': 'smallint',
           'datetime': 'datetime',
           'char': 'char',
           'text': 'text',
           'time': 'time',
           'decimal': 'decimal',
           'bigint': 'bigint',
           'timestamp': 'timestamp',
      },
      isPk: {
           '0': '否',
           '1': '是',
      },
      isNotNull: {
           '0': '否',
           '1': '是',
      },
      isUnique: {
           '0': '否',
           '1': '是',
      },
      isAutoIncrement: {
           '0': '否',
           '1': '是',
      },
      isZeroFill: {
           '0': '否',
           '1': '是',
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/code_table_column').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
          dispatch({
            type: 'queryTableIdOptions',
            payload: {},
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryCodeTableColumnList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createCodeTableColumn, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateCodeTableColumn, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteCodeTableColumn, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
    *queryTableIdOptions({ payload }, { call, put }) {
      const data = yield call(queryCodeTableOptions, payload)
      if (data.success) {
        yield put({
          type: 'queryOptions',
          payload: {
              tableId: data.payload
          },
        })
      } else {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
