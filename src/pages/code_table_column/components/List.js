import React, { PureComponent } from 'react'
import {Table, Avatar, Modal, Tooltip, Typography} from 'antd'
import {t, Trans} from "@lingui/macro"
import { Ellipsis } from 'components'
import {DropOption} from "../../../components";
import PropTypes from "prop-types";
import {getNavigationTreeTitle} from "../../../utils/tree";
const { confirm } = Modal
const { Text } = Typography;

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const { onDeleteItem, onEditItem } = this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    }
  }

  render() {
    const { options,trees,...tableProps } = this.props
    const columns = [

      {
        title: t`Id`,
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: t`Table Name`,
        dataIndex: 'tableId',
        key: 'tableId',
        render: (text, recode) => {
          let map = options.tableId;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Name`,
        dataIndex: 'name',
        key: 'name',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Title`,
        dataIndex: 'title',
        key: 'title',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Data Type`,
        dataIndex: 'dataType',
        key: 'dataType',
        render: (text, recode) => {
          let map = options.dataType;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Is Pk`,
        dataIndex: 'isPk',
        key: 'isPk',
        render: (text, recode) => {
          let map = options.isPk;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Is Not Null`,
        dataIndex: 'isNotNull',
        key: 'isNotNull',
        render: (text, recode) => {
          let map = options.isNotNull;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Is Unique`,
        dataIndex: 'isUnique',
        key: 'isUnique',
        render: (text, recode) => {
          let map = options.isUnique;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Is Auto Increment`,
        dataIndex: 'isAutoIncrement',
        key: 'isAutoIncrement',
        render: (text, recode) => {
          let map = options.isAutoIncrement;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Is Zero Fill`,
        dataIndex: 'isZeroFill',
        key: 'isZeroFill',
        render: (text, recode) => {
          let map = options.isZeroFill;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Default Value`,
        dataIndex: 'defaultValue',
        key: 'defaultValue',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Length`,
        dataIndex: 'length',
        key: 'length',
      },
      {
        title: t`Scale`,
        dataIndex: 'scale',
        key: 'scale',
      },
      {
        title: t`Comment`,
        dataIndex: 'comment',
        key: 'comment',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                { key: '1', name: t`Update` },
                { key: '2', name: t`Delete` },
              ]}
            />
          )
        },
      },
    ]

    return (
      <Table
        {...tableProps}
        pagination={{
          ...tableProps.pagination,
          showTotal: (total)=>{return t`Total ` + total + t` Items`},
        }}
        bordered
        scroll={{ x: 1200 }}
        columns={columns}
        simple
        rowKey={record => record.id}
      />
    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
}

export default List

