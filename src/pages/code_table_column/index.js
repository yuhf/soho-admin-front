import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import CodeTableColumnModal from "./components/Modal";
import Filter from "./components/Filter"


@connect(({ code_table_column, loading }) => ({ code_table_column, loading }))
class CodeTableColumn extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { code_table_column, loading, location, dispatch } = this.props
    const { list, pagination } = code_table_column
    const { query, pathname } = location
    return {
      options: code_table_column.options,
      trees: code_table_column.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['code_table_column/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'code_table_column/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'code_table_column/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'example_category/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, code_table_column, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = code_table_column

    return {
      options: code_table_column.options,
      trees: code_table_column.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`code_table_column/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `code_table_column/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'code_table_column/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, code_table_column} = this.props
    const {query,pathname} = location
    return {
      options: code_table_column.options,
      trees: code_table_column.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'code_table_column/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <CodeTableColumnModal {...this.modalProps} />
      </Page>
    )
  }
}

CodeTableColumn.propTypes = {
  code_table_column: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default CodeTableColumn

