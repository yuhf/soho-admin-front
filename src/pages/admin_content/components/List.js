import React, { PureComponent } from 'react'
import {Table, Avatar, Modal, Badge} from 'antd'
import {t, Trans} from "@lingui/macro"
import { Ellipsis } from 'components'
import {DropOption} from "../../../components";
import PropTypes from "prop-types";
import {DraftEditor, QuillEditor} from "../../../components/Editor";
const { confirm } = Modal

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const { onDeleteItem, onEditItem } = this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    }
  }

  render() {
    const { categoryOptions,...tableProps } = this.props
    const columns = [

      {
        title: t`Id`,
        dataIndex: 'id',
      },
      {
        title: t`Title`,
        dataIndex: 'title',
      },
      {
        title: t`Keywords`,
        dataIndex: 'keywords',
      },
      {
        title: t`Category`,
        dataIndex: 'categoryId',
        render: (text, recode) => {
          return categoryOptions[recode.categoryId] == null ? "" : categoryOptions[recode.categoryId]
        }
      },
      // {
      //   title: t`User Id`,
      //   dataIndex: 'userId',
      // },
      {
        title: t`Status`,
        dataIndex: 'status',
        render: (text,recode) => {
          if(recode.status == 1) {
            return <Badge status="success" />
          } else {
            return <Badge status="default" />
          }
        }
      },
      {
        title: t`Is Top`,
        dataIndex: 'isTop',
        render: (text,recode) => {
          if(recode.isTop == 1) {
            return <Badge status="success" />
          } else {
            return <Badge status="default" />
          }
        }
      },
      {
        title: t`Updated Time`,
        dataIndex: 'updatedTime',
      },
      {
        title: t`Created Time`,
        dataIndex: 'createdTime',
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                { key: '1', name: t`Update` },
                { key: '2', name: t`Delete` },
              ]}
            />
          )
        },
      },
    ]

    return (
      <>
        <Table
          {...tableProps}
          pagination={{
            ...tableProps.pagination,
            showTotal: total => t`Total ${total} Items`,
          }}
          bordered
          scroll={{ x: 1200 }}
          columns={columns}
          simple
          rowKey={record => record.id}
        />

      </>

    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
}

export default List

