import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import AdminContentModal from "./components/Modal";
import Filter from "./components/Filter"

@connect(({ admin_content, loading }) => ({ admin_content, loading }))
class AdminContent extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { admin_content, loading, location, dispatch } = this.props
    const { list, pagination,categoryOptions } = admin_content
    const { query, pathname } = location
    return {
      pagination,
      categoryOptions,
      dataSource: list,
      loading: loading.effects['admin_content/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'admin_content/delete',
          payload: id,
        }).then(() => {
          this.handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'admin_content/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, admin_content, loading } = this.props
    const { currentItem, modalVisible, modalType,categoryTree,resourceIds } = admin_content

    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_content/${modalType}`],
      categoryTree: categoryTree,
      resourceIds: resourceIds,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `admin_content/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_content/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch,admin_content } = this.props
    const {categoryTree} = admin_content
    const {query,pathname} = location

    return {
      filter: {
        ...query,
      },
      categoryTree: categoryTree,
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'admin_content/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <AdminContentModal {...this.modalProps} />
      </Page>
    )
  }
}

AdminContent.propTypes = {
  admin_content: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default AdminContent

