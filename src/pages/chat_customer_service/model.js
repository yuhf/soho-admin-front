import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryChatCustomerServiceList, updateChatCustomerService, createChatCustomerService,deleteChatCustomerService } = api
/**
 *
   //相关api信息
 queryChatCustomerServiceList: `GET ${prefix}/admin/chatCustomerService/list`,
 updateChatCustomerService: `PUT ${prefix}/admin/chatCustomerService`,
 createChatCustomerService: `POST ${prefix}/admin/chatCustomerService`,
 deleteChatCustomerService: `DELETE ${prefix}/admin/chatCustomerService/:ids`,
 queryChatCustomerServiceDetails: `GET ${prefix}/admin/chatCustomerService/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'chat_customer_service',
  state: {
    options: {
      status: {
           '1': '下线',
           '2': '活跃',
           '3': '禁用',
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/chat_customer_service').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryChatCustomerServiceList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createChatCustomerService, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateChatCustomerService, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteChatCustomerService, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})