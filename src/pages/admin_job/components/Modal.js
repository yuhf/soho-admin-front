import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message, Switch} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class AdminJobModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          status: values.status ? 1 : 0,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form, ...modalProps } = this.props
    return (

      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item,status:item.status==1?true:false }} layout="horizontal">

         <FormItem name='name' rules={[{ required: true }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='canConcurrency' rules={[{ required: true }]}
            label={t`Can Concurrency`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='cmd' rules={[{ required: true }]}
            label={t`Cmd`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='status' rules={[{ required: true }]}
                   valuePropName="checked"
            label={t`Status`} hasFeedback {...formItemLayout}>
           <Switch checkedChildren="开启" unCheckedChildren="关闭" />
          </FormItem>
         <FormItem name='cron' rules={[{ required: true }]}
            label={t`cron`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

AdminJobModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default AdminJobModal

