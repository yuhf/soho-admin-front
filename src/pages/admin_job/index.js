import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import AdminJobModal from "./components/Modal";
import Filter from "./components/Filter"

@connect(({ admin_job, loading }) => ({ admin_job, loading }))
class AdminJob extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
      }),
    })
  }

  get listProps() {
    const { admin_job, loading, location, dispatch } = this.props
    const { list, pagination } = admin_job
    const { query, pathname } = location
    return {
      pagination,
      dataSource: list,
      loading: loading.effects['admin_job/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'admin_job/delete',
          payload: id,
        }).then(() => {
          this.handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onRunItem: id => {
      dispatch({
        type: 'admin_job/run',
        payload: id,
      }).then(() => {
        this.handleTabClick({
          page:
            list.length === 1 && pagination.current > 1
              ? pagination.current - 1
              : pagination.current,
        })
      })
    },
      onEditItem(item) {
        dispatch({
          type: 'admin_job/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, admin_job, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = admin_job

    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_job/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `admin_job/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_job/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch } = this.props
    const {query,pathname} = location
    return {
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'admin_job/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <AdminJobModal {...this.modalProps} />
      </Page>
    )
  }
}

AdminJob.propTypes = {
  admin_job: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default AdminJob

