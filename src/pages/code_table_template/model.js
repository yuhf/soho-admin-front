import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryCodeTableTemplateList, updateCodeTableTemplate, createCodeTableTemplate,deleteCodeTableTemplate,queryCodeTableTemplateRunTest,queryCodeTableOptions,queryCodeTableTemplateGroupOptions } = api
/**
 *
   //相关api信息
 queryCodeTableTemplateList: `GET ${prefix}/admin/codeTableTemplate/list`,
 updateCodeTableTemplate: `PUT ${prefix}/admin/codeTableTemplate`,
 createCodeTableTemplate: `POST ${prefix}/admin/codeTableTemplate`,
 deleteCodeTableTemplate: `DELETE ${prefix}/admin/codeTableTemplate/:ids`,
 queryCodeTableTemplateDetails: `GET ${prefix}/admin/codeTableTemplate/:id`,

原始数据解析信息
name: id   capitalName: Id capitalKeyName: Id   type: long   frontType: number     frontMax: 99999999999       frontStep:      dbType: int       dbUnsigned: false       comment: null     length: 11       scale: 0     frontLength: 12     defaultValue: null       isNotNull: true     specification: int(11)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: title   capitalName: Title capitalKeyName: Title   type: String   frontType: text     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 标题     length: 255       scale: 0     frontLength: 255     defaultValue: null       isNotNull: false     specification: varchar(255)     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: status   capitalName: Status capitalKeyName: Status   type: long   frontType: select     frontMax: null       frontStep:      dbType: tinyint       dbUnsigned: false       comment: 状态;0:禁用,1:活跃;frontType:select     length: 4       scale: 0     frontLength: 4     defaultValue: null       isNotNull: false     specification: tinyint(4)     dbForeignName: null     capitalForeignName:        javaForeignName:        原始数据解析信息
name: code   capitalName: Code capitalKeyName: Code   type: String   frontType: textArea     frontMax: null       frontStep:      dbType: text       dbUnsigned: false       comment: 代码;;frontType:textArea     length: -1       scale: 0     frontLength: -1     defaultValue: null       isNotNull: false     specification: text     dbForeignName:      capitalForeignName:        javaForeignName:        原始数据解析信息
name: createdTime   capitalName: Created Time capitalKeyName: CreatedTime   type: java.sql.Timestamp   frontType: text     frontMax: null       frontStep:      dbType: datetime       dbUnsigned: false       comment: null     length: -1       scale: 0     frontLength: -1     defaultValue: null       isNotNull: false     specification: datetime     dbForeignName:      capitalForeignName:        javaForeignName:         *
 */
export default modelExtend(pageModel, {
  namespace: 'code_table_template',
  state: {
    options: {
      status: {
           '0': '禁用',
           '1': '活跃',
      },
      tableId: {}
    },
    kv: {
      groupId: {}
    },
    trees: {
    },
    //run test result
    runTestResult: "",
    showRunTest: false,
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/code_table_template').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })

          dispatch({
            type: 'queryTableIdOptions',
            payload: {},
          })
          dispatch({
            type: 'queryTableTemplateGroupOptions',
            payload: {},
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryCodeTableTemplateList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
    *queryTableIdOptions({ payload }, { call, put }) {
      const data = yield call(queryCodeTableOptions, payload)
      if (data.success) {
        yield put({
          type: 'queryOptions',
          payload: {
            tableId: data.payload
          },
        })
      } else {
        throw data
      }
    },
    *queryTableTemplateGroupOptions({ payload }, { call, put }) {
      const data = yield call(queryCodeTableTemplateGroupOptions, payload)
      if (data.success) {
        yield put({
          type: 'queryOptions',
          payload: {
            groupId: data.payload
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createCodeTableTemplate, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateCodeTableTemplate, payload)
      if (data.success) {
        //yield put({ type: 'hideModal' })
        return data;
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteCodeTableTemplate, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
    //执行代名测试
    *runTest({payload}, {select, call, put}) {
      const data = yield call(queryCodeTableTemplateRunTest, {...payload})
      if(data.success) {
        yield put({type: 'updateState', payload: {runTestResult: data.payload, showRunTest: true}})
      }
    }
  },
  reducers: {
    //隐藏执行对话框
    hideRunTest(state, {payload}) {
      return { ...state, ...payload, showRunTest: false}
    },
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
