import React, { PureComponent } from 'react'
import {Table, Avatar, Modal, Tooltip, Typography} from 'antd'
import {t, Trans} from "@lingui/macro"
import { Ellipsis } from 'components'
import {DropOption} from "../../../components";
import PropTypes from "prop-types";
import {getNavigationTreeTitle} from "../../../utils/tree";
const { confirm } = Modal
const { Text } = Typography;

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const { onDeleteItem, onEditItem } = this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    }
  }

  render() {
    const { options, trees,...tableProps } = this.props
    //生成kv数据
    let groupIdKv = {}
    for (let i = 0; i < options.groupId.length; i++) {
      let item = options.groupId[i]
      groupIdKv[item.value] = item.label;
    }
    const columns = [

      {
        title: t`Id`,
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: t`Name`,
        dataIndex: 'name',
        key: 'name',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Group Name`,
        dataIndex: 'groupId',
        key: 'groupId',
        render:(text, recode) => {
          return groupIdKv[parseInt(text)] ? groupIdKv[text]: '未知'
        }
      },
      {
        title: t`Title`,
        dataIndex: 'title',
        key: 'title',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Status`,
        dataIndex: 'status',
        key: 'status',
        render: (text, recode) => {
          let map = options.status;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Code`,
        dataIndex: 'code',
        key: 'code',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Created Time`,
        dataIndex: 'createdTime',
        key: 'createdTime',
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                { key: '1', name: t`Update` },
                { key: '2', name: t`Delete` },
              ]}
            />
          )
        },
      },
    ]

    return (
      <Table
        {...tableProps}
        pagination={{
          ...tableProps.pagination,
          showTotal: (total)=>{return t`Total ` + total + t` Items`},
        }}
        bordered
        scroll={{ x: 1200 }}
        columns={columns}
        simple
        rowKey={record => record.id}
      />
    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
}

export default List

