import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Input,
  InputNumber,
  Radio,
  Modal,
  Select,
  Upload,
  message,
  Checkbox,
  DatePicker,
  TreeSelect,
  Button
} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import draftToHtml from "draftjs-to-html";
import draft from "draft-js";import DraftEditor from "../../../components/Editor";import {addRootNode} from "../../../utils/tree";
import CodeEditor from "../../../components/CodeEditor/CodeEditor";
const {createUserAvatar, createAdminContentImage} = api
const { confirm } = Modal
const {ContentState} = draft
const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 3,
  },
  wrapperCol: {
    span: 19,
  },
}

class CodeTableTemplateModal extends PureComponent {
  formRef = React.createRef()

  state = {
    open: false,
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          //时间值处理
        }
        onOk(data, true)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  //只保存
  handleOnlySave = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          //时间值处理
        }
        onOk(data, false)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  handleCancel = () => {
    const { onCancel } = this.props
    confirm({
      title: t`Are you sure close this editor`,
      onOk() {
        onCancel();
      }
    })
  }

  handleRun = () => {
    const {item = {}, onRunTest} = this.props
    this.formRef.current.validateFields()
      .then(values=>{
        console.log(values)
        onRunTest(values)
      }).catch(errorInfo => {
        console.log(errorInfo);
    })
  }

  render() {
    const { item = {}, onOk, onCancel, form,options,trees, ...modalProps } = this.props
    const tableIdOptionData = Object.keys(options.tableId).map((k,index)=>{
      return {value: parseInt(k), label: options.tableId[k]}
    })
    const groupIdOptionData = options.groupId
    const statusOptionData = Object.keys(options.status).map((k,index)=>{
      return {value: parseInt(k), label: options.status[k]}
    })

    let initData = {tableId: parseInt(Object.keys(options.tableId)[0]), ...item};
    //初始化时间处理
    return (

      <Modal {...modalProps} onCancel={this.handleCancel} onOk={this.handleOk} width={1300}
                             footer={[
                               <Button key='cancel' onClick={(event) => this.handleCancel()} >取消</Button>,
                               <Button key='continue' onClick={this.handleRun}>测试运行</Button>,
                               <Button key='continue' onClick={this.handleOnlySave}>保存&编辑</Button>,
                               <Button key='confirm' type="primary" onClick={this.handleOk}>确定</Button>
                             ]}

      >
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">

         <FormItem  name='name' rules={[{ required: true }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input maxLength={255} />
          </FormItem>
         <FormItem  name='title' rules={[{ required: true }]}
            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input maxLength={255} />
          </FormItem>
          <FormItem name='groupId' rules={[{required: false}]}
                    label={t`Group Name`} hasFeedback {...formItemLayout}>
            <Select
              showSearch
              placeholder={t`Group Name`}
              options={groupIdOptionData}
              />
          </FormItem>
         <FormItem  name='status' rules={[{ required: true }]}
            label={t`Status`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Status`}
             optionFilterProp="children"
             options={statusOptionData}
             />
          </FormItem>
         <FormItem  name='code' rules={[{ required: false }]}
                    // valuePropName={"value"}
            label={t`Code`} hasFeedback {...formItemLayout}>
            {/*<TextArea />*/}
           <CodeEditor />
          </FormItem>
          <FormItem name='tableId' rules={[{ required: false}]}
                    tooltip={"该字段只在执行测试的时候有用"}
                    label={t`Table Name`}
                    hasFeedback {...formItemLayout}
                    >
            <Select
              showSearch
              placeholder={t`Table Id`}
              optionFilterProp="children"
              options={tableIdOptionData}
            />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

CodeTableTemplateModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default CodeTableTemplateModal

