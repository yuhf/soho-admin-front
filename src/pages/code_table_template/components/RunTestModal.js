import React, { PureComponent } from 'react'
import {Modal} from "antd";
import CodeEditor from "../../../components/CodeEditor/CodeEditor";
import PropTypes from 'prop-types'


class RunTestModal extends PureComponent {
  render() {
    console.log(this.props)

    return <Modal {...this.props}
        width={"100%"}
        // height={1300}
      >

      <CodeEditor value={this.props.runTestResult} />

    </Modal>
  }
}

RunTestModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
  runTestResult: PropTypes.string,
}

export default RunTestModal;
