import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import CodeTableTemplateModal from "./components/Modal";
import Filter from "./components/Filter"
import RunTestModal from "./components/RunTestModal";
import {notification} from "antd";


@connect(({ code_table_template, loading }) => ({ code_table_template, loading }))
class CodeTableTemplate extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { code_table_template, loading, location, dispatch } = this.props
    const { list, pagination } = code_table_template
    const { query, pathname } = location
    return {
      options: code_table_template.options,
      trees: code_table_template.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['code_table_template/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'code_table_template/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'code_table_template/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'example_category/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, code_table_template, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = code_table_template

    return {
      options: code_table_template.options,
      trees: code_table_template.trees,
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`code_table_template/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: (data, isClose) => {
        dispatch({
          type: `code_table_template/${modalType}`,
          payload: data,
        }).then(() => {
          notification.success({message: '保存成功'})
          if(isClose) {
            dispatch({
              type: 'code_table_template/hideModal',
              payload: {}
            })
            this.handleRefresh()
          }
        })
      },
      onCancel() {
        dispatch({
          type: 'code_table_template/hideModal',
        })
      },
      //运行测试
      onRunTest: data => {
        console.log("run request....")
        dispatch({
          type: 'code_table_template/runTest',
          payload: data,
        })
      },
    }
  }

  //执行测试对话框
  get runTestProps() {
    const { dispatch, code_table_template, loading } = this.props
    const { currentItem, showRunTest, modalType,resourceTree,resourceIds } = code_table_template

    return {
      options: code_table_template.options,
      trees: code_table_template.trees,
      runTestResult: code_table_template.runTestResult,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: showRunTest,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`code_table_template/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
        modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        // dispatch({
        //   type: `code_table_template/${modalType}`,
        //   payload: data,
        // }).then(() => {
        //   this.handleRefresh()
        // })

        dispatch({
          type: 'code_table_template/hideRunTest',
        })
      },
      onCancel() {
        dispatch({
          type: 'code_table_template/hideRunTest',
        })
      },
      //运行测试
      onRunTest: data => {
        console.log("run request....")
        dispatch({
          type: 'code_table_template/runTest',
          payload: data,
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, code_table_template} = this.props
    const {query,pathname} = location
    return {
      options: code_table_template.options,
      trees: code_table_template.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'code_table_template/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <CodeTableTemplateModal {...this.modalProps} />
        <RunTestModal {...this.runTestProps} />
      </Page>
    )
  }
}

CodeTableTemplate.propTypes = {
  code_table_template: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default CodeTableTemplate

