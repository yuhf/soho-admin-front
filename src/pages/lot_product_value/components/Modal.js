import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class LotProductValueModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          key: item.key,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form, ...modalProps } = this.props
    return (

      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

         <FormItem name='id' rules={[{ required: true }]}
            label={t`id`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='modelItemId' rules={[{ required: true }]}
            label={t`modelItemId`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='productId' rules={[{ required: true }]}
            label={t`productId`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='title' rules={[{ required: true }]}
            label={t`title`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='value' rules={[{ required: true }]}
            label={t`value`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='givenValue' rules={[{ required: true }]}
            label={t`givenValue`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='unit' rules={[{ required: true }]}
            label={t`unit`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='type' rules={[{ required: true }]}
            label={t`type`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='order' rules={[{ required: true }]}
            label={t`order`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='createdTime' rules={[{ required: true }]}
            label={t`createdTime`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='updatedTime' rules={[{ required: true }]}
            label={t`updatedTime`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

LotProductValueModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default LotProductValueModal

