import modelExtend from 'dva-model-extend'
import api from 'api'
import {pageModel} from 'utils/model'
import {message} from "antd";

const {pathToRegexp} = require("path-to-regexp")
const {queryAdminDictList, updateAdminDict, createAdminDict, deleteAdminDict} = api
/**
 *
 //相关api信息
 queryAdminDictList: `GET ${prefix}/admin/adminDict/list`,
 updateAdminDict: `PUT ${prefix}/admin/adminDict`,
 createAdminDict: `POST ${prefix}/admin/adminDict`,
 deleteAdminDict: `DELETE ${prefix}/admin/adminDict/:ids`,
 queryAdminDictDetails: `GET ${prefix}/admin/adminDict/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'admin_dict',
  state: {
    options: {},
    trees: {},
    childrenListModelVisible: false,
    childrenAddModelVisible: false,
    childrenList: [],
    parent: null,
  },

  subscriptions: {
    setup({dispatch, history}) {
      history.listen(location => {
        if (pathToRegexp('/admin_dict').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    * query({payload}, {call, put}) {
      const data = yield call(queryAdminDictList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
    * create({payload}, {call, put}) {
      const data = yield call(createAdminDict, payload)
      if (data.success && data.code === 2000) {
        yield put({type: 'hideModal'})
      } else {
        message.error(data.msg, 3)
        throw data
      }
    },

    * update({payload}, {select, call, put}) {
      const data = yield call(updateAdminDict, payload)
      if (data.success) {
        yield put({type: 'hideModal'})
      } else {
        throw data
      }
    },
    * delete({payload}, {select, call, put}) {
      const data = yield call(deleteAdminDict, {ids: payload})
      if (!data.success) {
        throw data
      }
    },
    * queryChildrenList({payload}, {select, call, put}) {
      const data = yield call(queryAdminDictList, payload)
      if (data.success) {
        yield put({
          type: 'setChildrenList',
          payload: {
            childrenList: data.payload.list,
          },
        })
      } else {
        throw data;
      }
    }
  },
  reducers: {
    showModal(state, {payload}) {
      return {...state, ...payload, modalVisible: true}
    },

    hideModal(state) {
      return {...state, modalVisible: false}
    },

    showChildrenListModal(state, {payload}) {
      return {...state, ...payload, childrenListModelVisible: true}
    },
    hideChildrenListModal(state) {
      return {...state, childrenListModelVisible: false}
    },

    setChildrenList(state, {payload}) {
      return {...state, ...payload}
    },
    setParent(state, {payload}) {
      return {...state, ...payload}
    },

    showChildrenAddModal(state, {payload}) {
      return {...state, ...payload, childrenAddModelVisible: true}
    },
    hideChildrenAddModal(state) {
      return {...state, childrenAddModelVisible: false}
    },
  },
})
