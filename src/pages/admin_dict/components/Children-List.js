import React from 'react'
import {Button, Modal, Row, Table, Typography} from 'antd'
import {t, Trans} from "@lingui/macro";
import {DropOption} from "../../../components";

const {Text} = Typography;
const {confirm} = Modal;

const ChildrenList = (props) => {

  const handleMenuClick = (record, e) => {
    const {onDeleteItem, onEditItem} = props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    }
  }


  const columns = [
    {
      title: t`Id`,
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: t`Dict Code`,
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: t`Dict Key`,
      dataIndex: 'dictKey',
      key: 'dictKey',
    },
    {
      title: t`Dict Value`,
      dataIndex: 'dictValue',
      key: 'dictValue',
      render: (text, recode) => {
        if (text != null && text.length > 60) {
          const start = text.slice(0, 120).trim();
          return <Text style={{maxWidth: '100%'}} ellipsis={"..."}>{start}</Text>
        } else {
          return text
        }
      }
    },
    {
      title: t`Remark`,
      dataIndex: 'remark',
      key: 'remark',
      render: (text, recode) => {
        if (text != null && text.length > 60) {
          const start = text.slice(0, 120).trim();
          return <Text style={{maxWidth: '100%'}} ellipsis={"..."}>{start}</Text>
        } else {
          return text
        }
      }
    },
    {
      title: t`Updated Time`,
      dataIndex: 'updatedTime',
      key: 'updatedTime',
    },
    {
      title: t`Created Time`,
      dataIndex: 'createdTime',
      key: 'createdTime',
    },
    {
      title: <Trans>Operation</Trans>,
      key: 'operation',
      fixed: 'right',
      width: '8%',
      render: (text, record) => {
        return (
          <DropOption
            onMenuClick={e => handleMenuClick(record, e)}
            menuOptions={[
              {key: '1', name: t`Update`},
              {key: '2', name: t`Delete`},
            ]}
          />
        )
      },
    },
  ]

  return (
    <Modal {...props} width={1300}>
      <Row style={{margin: "20px" }}>
        <Button type="ghost" onClick={props.addDict}>
          <Trans>Create</Trans>
        </Button>
      </Row>

        {<Table
          dataSource={props.list}
          bordered
          scroll={{x: 1200}}
          columns={columns}
          simple
          rowKey={record => record.id}
        />}

    </Modal>
  )
}

export default ChildrenList
