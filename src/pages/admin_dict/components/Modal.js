import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Modal} from 'antd'
import {t} from "@lingui/macro"

const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class AdminDictModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const {item = {}, onOk} = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const {item = {}, onOk, form, options, trees, ...modalProps} = this.props
    let initData = {
      ...item
    };

    initData['parentId'] = 0;
    initData['dictKey'] = -1;

    //初始化时间处理
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{...initData}} layout="horizontal">
          <FormItem name='parentId' hidden={true} rules={[{required: 0}]} label={t`Parent Id`}
                    hasFeedback {...formItemLayout}>
            <Input disabled={false} maxLength={50}/>
          </FormItem>
          <FormItem name='code' rules={[{required: 1}]} label={t`Dict Code`} hasFeedback {...formItemLayout}>
            <Input disabled={false} maxLength={50}/>
          </FormItem>
          <FormItem name='dictKey' hidden={true} rules={[{required: 0}]} label={t`Dict Key`}
                    hasFeedback {...formItemLayout}>
            <InputNumber style={{width: 200}} disabled={false}/>
          </FormItem>
          <FormItem name='dictValue' rules={[{required: 2}]} label={t`Dict Value`} hasFeedback {...formItemLayout}>
            <Input disabled={false} maxLength={50}/>
          </FormItem>
          <FormItem name='sort' rules={[{required: 1}]} label={t`Sort`} hasFeedback {...formItemLayout}>
            <InputNumber max={999999} step="1" style={{width: 200}} disabled={false}/>
          </FormItem>
          <FormItem name='remark' rules={[{required: 0}]} label={t`Remark`} hasFeedback {...formItemLayout}>
            <Input disabled={false}
                   maxLength={255}/>
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

AdminDictModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default AdminDictModal
