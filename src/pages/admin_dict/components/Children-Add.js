import React from 'react'
import {Form, Input, InputNumber, Modal} from 'antd'
import {t} from "@lingui/macro";

const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const ChildrenAdd = props => {
  // 表单
  // const [form] = Form.useForm()
  const formRef = React.createRef()

  // OK方法
  const handleOk = () => {
    formRef.current.validateFields()
      .then(values => {
        let data = {...values, id: props.item.id}
        props.onOk(data)
      })
      .catch(err => {
        console.log(err)
      })
  }

  let initData;
  if (props.modalType === 'create') {
    // 新增
    initData = {
      parentId: props.parent?.id,
      code: props.parent?.code
    }
  } else {
    // 修改
    initData = {...props.item}
  }

  return (
    <Modal {...props} onOk={handleOk} width={1300}>
      <Form ref={formRef} preserve={false} initialValues={{...initData}} layout="horizontal">
        <FormItem name='parentId' hidden={true} rules={[{required: 1}]} label={t`Parent Id`}
                  hasFeedback {...formItemLayout}>
          <Input disabled={false} maxLength={50}/>
        </FormItem>
        <FormItem name='code' rules={[{required: 1}]} label={t`Dict Code`} hasFeedback {...formItemLayout}>
          <Input disabled={true} maxLength={50}/>
        </FormItem>
        <FormItem name='dictKey' rules={[{required: 1}]} label={t`Dict Key`}
                  hasFeedback {...formItemLayout}>
          <InputNumber style={{width: 200}} disabled={false}/>
        </FormItem>
        <FormItem name='dictValue' rules={[{required: 1}]} label={t`Dict Value`} hasFeedback {...formItemLayout}>
          <Input disabled={false} maxLength={50}/>
        </FormItem>
        <FormItem name='sort' rules={[{required: 1}]} label={t`Sort`} hasFeedback {...formItemLayout}>
          <InputNumber max={999999} step="1" style={{width: 200}} disabled={false}/>
        </FormItem>
        <FormItem name='remark' rules={[{required: 0}]} label={t`Remark`} hasFeedback {...formItemLayout}>
          <Input disabled={false}
                 maxLength={255}/>
        </FormItem>
      </Form>
    </Modal>
  )

}

export default ChildrenAdd
