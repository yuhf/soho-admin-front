import React, {Component} from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import {t, Trans} from "@lingui/macro"
import {Button, Col, DatePicker, Form, Input, Row} from 'antd'
import FilterForm from '../../../components/FilterForm'

class Filter extends Component {
  formRef = React.createRef()

  handleFields = fields => {
    const {betweenCreatedTime} = fields
    if (betweenCreatedTime && betweenCreatedTime.length) {
      fields.betweenCreatedTime = [
        moment(betweenCreatedTime[0]).format('YYYY-MM-DD'),
        moment(betweenCreatedTime[1]).format('YYYY-MM-DD'),
      ]
    }
    return fields
  }

  handleSubmit = () => {
    const {onFilterChange} = this.props
    const values = this.formRef.current.getFieldsValue()
    const fields = this.handleFields(values)
    onFilterChange(fields)
  }

  render() {
    const {onAdd, filter, options, trees} = this.props

    let initialCreateTime = []
    if (filter['betweenCreatedTime[0]']) {
      initialCreateTime[0] = moment(filter['betweenCreatedTime[0]'])
    }
    if (filter['betweenCreatedTime[1]']) {
      initialCreateTime[1] = moment(filter['betweenCreatedTime[1]'])
    }

    return (
      <FilterForm ref={this.formRef} onAdd={onAdd} onSubmit={this.handleSubmit} formProps={{initialValues: { ...filter, betweenCreatedTime: initialCreateTime }}} >
            <Form.Item name="code">
              <Input
                placeholder={t`Dict Code`}
                onSearch={this.handleSubmit}
              />
            </Form.Item>
            <Form.Item name="dictValue">
              <Input
                placeholder={t`Dict Value`}
                onSearch={this.handleSubmit}
              />
            </Form.Item>
      </FilterForm>
    )
  }
}

Filter.propTypes = {
  onAdd: PropTypes.func,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func,
}

export default Filter
