import React, {PureComponent} from 'react'
import {Modal, Table, Typography} from 'antd'
import {t, Trans} from "@lingui/macro"
import {DropOption} from "../../../components";
import PropTypes from "prop-types";

const {confirm} = Modal
const {Text} = Typography;

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const {onDeleteItem, onEditItem} = this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    } else if (e.key === '3') {
      const {showChildrenListModal} = this.props
      showChildrenListModal(record)
    }
  }

  // 弹窗展示子字典
  showDictChildrenWin = (record) => {
    const {showChildrenListModal} = this.props
    showChildrenListModal(record)
  }


  render() {
    const {options, trees, ...tableProps} = this.props
    const columns = [
      {
        title: t`Id`,
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: t`Dict Code`,
        dataIndex: 'code',
        key: 'code',
        render: (text, recode) => {
          return <a target="_blank" onClick={() => this.showDictChildrenWin(recode)}>{text}</a>;
        }
      },
      {
        title: t`Dict Value`,
        dataIndex: 'dictValue',
        key: 'dictValue',
        render: (text, recode) => {
          if (text != null && text.length > 60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{maxWidth: '100%'}} ellipsis={"..."}>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Remark`,
        dataIndex: 'remark',
        key: 'remark',
        render: (text, recode) => {
          if (text != null && text.length > 60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{maxWidth: '100%'}} ellipsis={"..."}>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Updated Time`,
        dataIndex: 'updatedTime',
        key: 'updatedTime',
      },
      {
        title: t`Created Time`,
        dataIndex: 'createdTime',
        key: 'createdTime',
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                {key: '3', name: '字典配置'},
                {key: '1', name: t`Update`},
                {key: '2', name: t`Delete`},
              ]}
            />
          )
        },
      },
    ]

    return (
      <Table
        {...tableProps}
        pagination={{
          ...tableProps.pagination,
          showTotal: (total) => {
            return t`Total ` + total + t` Items`
          },
        }}
        bordered
        scroll={{x: 1200}}
        columns={columns}
        simple
        rowKey={record => record.id}
      />
    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
}

export default List
