import React, {PureComponent} from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import {stringify} from 'qs'
import {t} from "@lingui/macro"
import {Page} from 'components'
import List from './components/List'
import AdminDictModal from "./components/Modal";
import Filter from "./components/Filter"
import ChildrenList from "./components/Children-List";
import ChildrenAdd from "./components/Children-Add";

@connect(({admin_dict, loading}) => ({admin_dict, loading}))
class AdminDict extends PureComponent {
  handleRefresh = newQuery => {
    const {location} = this.props
    const {query, pathname} = location
    if (newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        {arrayFormat: 'repeat'}
      ),
    })
  }
  handleTabClick = key => {
    const {pathname} = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const {admin_dict, loading, location, dispatch} = this.props
    const {list, pagination} = admin_dict
    const {query, pathname} = location
    return {
      dispatch: dispatch,
      options: admin_dict.options,
      trees: admin_dict.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['admin_dict/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'admin_dict/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'admin_dict/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onApplyItem(item) {
        dispatch({
          type: 'admin_dict/showModal',
          payload: {
            modalType: 'apply',
            currentItem: item,
          }
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'admin_dict/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },
      showChildrenListModal(record) {
        // 1.查询子数据
        dispatch({
          type: 'admin_dict/queryChildrenList',
          payload: {parentId: record.id},
        })

        // 2.显示子列表
        dispatch({
          type: 'admin_dict/showChildrenListModal',
          payload: record,
        })

        // 3.设置父ID
        dispatch({
          type: 'admin_dict/setParent',
          payload: {parent: record},
        })
      },
    }
  }

  get modalProps() {
    const {dispatch, admin_dict, loading} = this.props
    const {currentItem, modalVisible, modalType, resourceTree, resourceIds} = admin_dict

    return {
      options: admin_dict.options,
      trees: admin_dict.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_dict/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
        modalType === 'create' ? t`Create` : (modalType === 'update' ? t`Update` : t`Apply`)
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `admin_dict/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_dict/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const {location, dispatch, admin_dict} = this.props
    const {query, pathname} = location
    return {
      options: admin_dict.options,
      trees: admin_dict.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'admin_dict/showModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
    }
  }

  get childrenList() {
    const {dispatch, admin_dict, loading} = this.props
    const {
      currentItem,
      childrenListModelVisible,
      modalType,
      resourceTree,
      resourceIds,
      childrenList,
      parent
    } = admin_dict

    return {
      list: childrenList,
      item: modalType === 'create' ? {} : currentItem,
      visible: childrenListModelVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_dict/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `列表`,
      centered: true,
      onOk() {
        dispatch({
          type: 'admin_dict/hideChildrenListModal',
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_dict/hideChildrenListModal',
        })
      },
      addDict() {
        dispatch({
          type: 'admin_dict/showChildrenAddModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'admin_dict/showChildrenAddModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'admin_dict/delete',
          payload: id,
        }).then(() => {
          // 刷新子列表
          dispatch({
            type: 'admin_dict/queryChildrenList',
            payload: {parentId: parent.id}
          })
        })
      },
    }
  }

  get childrenAdd() {
    const {dispatch, admin_dict, loading} = this.props
    const {currentItem, childrenAddModelVisible, modalType, parent} = admin_dict

    return {
      parent: parent,
      modalType: modalType,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: childrenAddModelVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_dict/${modalType}`],
      title: `${
        modalType === 'create' ? t`Create` : (modalType === 'update' ? t`Update` : t`Apply`)
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `admin_dict/${modalType}`,
          payload: data,
        }).then(() => {
          // 刷新子列表
          dispatch({
            type: 'admin_dict/queryChildrenList',
            payload: {parentId: parent.id}
          })

          // 关闭添加窗口
          dispatch({
            type: 'admin_dict/hideChildrenAddModal'
          })
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_dict/hideChildrenAddModal',
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <AdminDictModal {...this.modalProps} />
        <ChildrenList {...this.childrenList} />
        <ChildrenAdd {...this.childrenAdd} />
      </Page>
    )
  }
}

AdminDict.propTypes = {
  admin_dict: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default AdminDict
