import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryExampleList, updateExample, createExample,deleteExample,queryExampleCategoryTree,queryExampleOptionOptions,applyExample } = api
/**
 *
   //相关api信息
 queryExampleList: `GET ${prefix}/admin/example/list`,
 updateExample: `PUT ${prefix}/admin/example`,
 createExample: `POST ${prefix}/admin/example`,
 deleteExample: `DELETE ${prefix}/admin/example/:ids`,
 queryExampleDetails: `GET ${prefix}/admin/example/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'example',
  state: {
    options: {
      optionId: {
      },

      applyStatus: {
           '0': '未审批',
           '1': '已审批',
      },

      status: {
           '0': '禁用',
           '1': '活跃',
      },
    },
    trees: {
           categoryId: [],
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/example').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
          dispatch({
            type: 'queryExampleCategoryTree',
            payload: {},
          })
          dispatch({
            type: 'queryExampleOptionOptions',
            payload: {},
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryExampleList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createExample, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    //apply
    *apply({ payload }, { call, put }) {
      const data = yield call(applyExample, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateExample, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteExample, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
   *queryExampleCategoryTree({payload}, {call, put}) {
      const data = yield call(queryExampleCategoryTree, payload);
      if(data.success) {
        yield put({
          type: 'querySuccessTree',
          payload: {
            categoryId: data.payload,
          }
        })
      } else {
        throw data;
      }
   },
    *queryExampleOptionOptions({ payload }, { call, put }) {
      const data = yield call(queryExampleOptionOptions, payload)
      if (data.success) {
        yield put({
          type: 'queryOptions',
          payload: {
              optionId: data.payload
          },
        })
      } else {
        throw data
      }
    },  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})