import React, { PureComponent } from 'react'
import {Table, Avatar, Modal, Tooltip, Typography} from 'antd'
import {t, Trans} from "@lingui/macro"
import { Ellipsis } from 'components'
import {DropOption} from "../../../components";
import PropTypes from "prop-types";
import {getNavigationTreeTitle} from "../../../utils/tree";
const { confirm } = Modal
const { Text } = Typography;

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const { onDeleteItem, onEditItem ,onApplyItem }= this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    } else if (e.key === '3') {
      onApplyItem(record)
    }
  }

  render() {
    const { options,trees,...tableProps } = this.props
    const columns = [
      {
        title: t`Id`,
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: t`Title`,
        dataIndex: 'title',
        key: 'title',
           render:(text, recode) => {
          if(text != null && text.length>60) {
            const start = text.slice(0, 120).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Category`,
        dataIndex: 'categoryId',
        key: 'categoryId',
        render: (text, recode) => {
          return getNavigationTreeTitle(text, trees.categoryId)
        }
      },
      {
        title: t`样例选项`,
        dataIndex: 'optionId',
        key: 'optionId',
        render: (text, recode) => {
          let map = options.optionId;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Updated Time`,
        dataIndex: 'updatedTime',
        key: 'updatedTime',
      },
      {
        title: t`Created Time`,
        dataIndex: 'createdTime',
        key: 'createdTime',
      },
      {
        title: t`Apply Status`,
        dataIndex: 'applyStatus',
        key: 'applyStatus',
        render: (text, recode) => {
          let map = options.applyStatus;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: t`Status`,
        dataIndex: 'status',
        key: 'status',
        render: (text, recode) => {
          let map = options.status;
          return map[text]?map[text]:'未知'
        },
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                { key: '1', name: t`Update` },
                { key: '2', name: t`Delete` },
                { key: '3', name: t`Apply` },              ]}
            />
          )
        },
      },
    ]

    return (
      <Table
        {...tableProps}
        pagination={{
          ...tableProps.pagination,
          showTotal: (total)=>{return t`Total ` + total + t` Items`},
        }}
        bordered
        scroll={{ x: 1200 }}
        columns={columns}
        simple
        rowKey={record => record.id}
      />
    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
}

export default List
