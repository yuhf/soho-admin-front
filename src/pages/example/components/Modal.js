import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import QuillEditor from "../../../components/Editor/QuillEditor";
import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ExampleModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props
    const optionIdOptionData = Object.keys(options.optionId).map((k,index)=>{
      return {value: k, label: options.optionId[k]}
    })
    const applyStatusOptionData = Object.keys(options.applyStatus).map((k,index)=>{
      return {value: parseInt(k), label: options.applyStatus[k]}
    })
    const statusOptionData = Object.keys(options.status).map((k,index)=>{
      return {value: parseInt(k), label: options.status[k]}
    })
    let initData = {
apply_status: 0,
status: 1,
                    ...item};
    //初始化时间处理
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
         <FormItem  name='title' rules={[{ required: 0 }]}            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={45} />
          </FormItem>
         <FormItem  name='categoryId' rules={[{ required: 0 }]}            label={t`Category`} hasFeedback {...formItemLayout}>
       <TreeSelect
             showSearch
             style={{ width: '100%' }}
             // value={value}
             dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
             placeholder="Please select"
             allowClear
             treeDefaultExpandAll
             // onChange={onChange}
             disabled={false}
             treeData={addRootNode(trees?.categoryId)}
           />
          </FormItem>
         <FormItem  name='optionId' rules={[{ required: 0 }]}            label={t`样例选项`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`样例选项`}
             optionFilterProp="children"
             options={optionIdOptionData}
             disabled={false}
             />
          </FormItem>
         <FormItem  name='content' rules={[{ required: 0 }]}           label={t`Content`} hasFeedback {...formItemLayout}>
           <QuillEditor disabled={false} />
          </FormItem>
         <FormItem  name='applyStatus' rules={[{ required: 1 }]}            label={t`Apply Status`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Apply Status`}
             optionFilterProp="children"
             options={applyStatusOptionData}
             disabled={true}
             />
          </FormItem>
         <FormItem  name='status' rules={[{ required: 0 }]}            label={t`Status`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Status`}
             optionFilterProp="children"
             options={statusOptionData}
             disabled={false}
             />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

ExampleModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default ExampleModal
