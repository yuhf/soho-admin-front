import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Input,
  InputNumber,
  Radio,
  Modal,
  Select,
  Upload,
  message,
  Checkbox,
  DatePicker,
  TreeSelect,
  Space, Button
} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, MinusCircleOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import draftToHtml from "draftjs-to-html";
import draft from "draft-js";import DraftEditor from "../../../components/Editor";import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const {ContentState} = draft
const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class CodeTableModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
    optionNames: {},
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
          //时间值处理
        }

        //处理扩展数据
        let comment = "";
        values.optionItems && values.optionItems.map((v,k)=>{
          comment += v.name + ":" + v.value
          if(k != (values.optionItems.length - 1)) {
            comment += ","
          }
        })
        comment = (values.title ? values.title : "") +";;" + comment
        data.comment = comment
        delete data.optionItems
        // console.log("commit ..............")
        // console.log(data)
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  /**
   * 解析扩展数据
   *
   * @param item
   * @returns {[]}
   */
  parseExtendItems = (item) => {
    let comment = item.comment
    let title = item.title;
    let extendOptions = ""
    let extendItems = []
    if(comment) {
      let parts = comment.split(";")
      console.log(parts)
      if(parts.length>0) {
        //   //title
        title = parts[0]
        if(parts.length>=2) {
          extendOptions = parts[1]
        }
        if(parts.length>=3) {
          let extendParts = parts[2].split(",")
          for(let i=0; i<extendParts.length; i++) {
            let tmpParts = extendParts[i].split(":")
            console.log(extendParts[i])
            if(tmpParts.length==2) {
              extendItems.push({name: tmpParts[0], value: tmpParts[1]})
            }
          }
        }
      }
    }
    //检查填充option
    if(extendOptions && extendOptions != "") {
      extendItems["option"] = extendOptions
    }
    return extendItems;
  }


  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props

    let initData = {...item};

    //处理解析扩展项数据
    let extendItems = this.parseExtendItems(item)
    initData['optionItems'] = extendItems
    let optionNames = {}
    for(let i=0; i< extendItems.length; i++) {
      optionNames[i] = extendItems[i].name;
    }
    this.state['optionNames'] = {...optionNames,...this.state.optionNames}
    //初始化title
    if(!item.title) {
      let comment = item.comment;
      if(comment) {
        let parts = comment.split(";")
        if(parts.length>0) {
          initData.title = parts[0]
        }
      }
    }


    //初始化时间处理
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">

         <FormItem  name='name' rules={[{ required: false }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input maxLength={45} />
          </FormItem>
         <FormItem  name='title' rules={[{ required: false }]}
            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input maxLength={45} />
          </FormItem>
         <FormItem  name='comment' rules={[{ required: false }]}
            label={t`Comment`} hasFeedback {...formItemLayout}>
            <Input maxLength={1000} />
          </FormItem>

          <FormItem  name='extend' rules={[{ required: false }]}
                     label={t`Extend Data`} hasFeedback {...formItemLayout}>
            <Form.List name="optionItems" style={{margin: "350px"}} rules={[{required: false}]} hasFeedback >
              {(fields, { add, remove }) => (

                <>
                  {fields.map(({ key, name, ...restField }) => (
                    <Space key={key} style={{ display: 'flex', marginBottom: 8 }} size={"middle"} align="baseline">
                      <Form.Item
                        {...restField}
                        name={[name, 'name']}
                        rules={[{required: false}]} >
                        <Select
                          placeholder={"请选择扩展项名称"}
                          onChange={(e)=>{
                            let optionNames = this.state.optionNames
                            optionNames[key] = e
                            this.setState({optionNames: optionNames})
                          }}>
                          {/*<Select.Option value="frontType">frontType</Select.Option>*/}
                          {/*<Select.Option value="frontName">frontName</Select.Option>*/}
                          {/*<Select.Option value="javaType">javaType</Select.Option>*/}
                          <Select.Option value="frontHome">frontHome</Select.Option>
                          {/*<Select.Option value="uploadCount">uploadCount</Select.Option>*/}
                          {/*<Select.Option value="ignoreInList">ignoreInList</Select.Option>*/}
                          {/*<Select.Option value="foreign">foreign</Select.Option>*/}
                          <Select.Option value="tree">tree</Select.Option>
                          {/*<Select.Option value="parent">parent</Select.Option>*/}
                          <Select.Option value="option">option</Select.Option>
                          <Select.Option value="approval">approval</Select.Option>
                          {/*<Select.Option value="isFilter">isFilter</Select.Option>*/}
                        </Select>
                      </Form.Item>
                      {/*java类型选择*/}
                      {this.state.optionNames[key] && this.state.optionNames[key]=="javaType" && <Form.Item
                        {...restField}
                        name={[name, 'value']}
                        style={{width: 100}}
                        rules={[{required: false}]} >
                        <Select placeholder={"请选择Java类型"}>
                          <Select.Option value={"Integer"}>Integer</Select.Option>
                          <Select.Option value={"String"}>String</Select.Option>
                          <Select.Option value={"Date"}>Date</Select.Option>
                        </Select>
                      </Form.Item>}
                      {/*frontType选择*/}
                      {this.state.optionNames[key] && this.state.optionNames[key]=="frontType" && <Form.Item
                        {...restField}
                        name={[name, 'value']}
                        style={{width: 100}}
                        rules={[{required: false}]} >
                        <Select placeholder={"请选择前端类型"} >
                          <Select.Option value="text">Text</Select.Option>
                          <Select.Option value="select">Select</Select.Option>
                          <Select.Option value="treeSelect">TreeSelect</Select.Option>
                          <Select.Option value="checkbox">Checkbox</Select.Option>
                          <Select.Option value="textarea">Textarea</Select.Option>
                          <Select.Option value="editor">editor</Select.Option>
                          <Select.Option value="datetime">datetime</Select.Option>
                          <Select.Option value="date">date</Select.Option>
                          <Select.Option value="upload">upload</Select.Option>
                          <Select.Option value="number">number</Select.Option>
                          <Select.Option value="password">password</Select.Option>
                        </Select>
                      </Form.Item>}
                      {/*uploadSize  Number*/}
                      {this.state.optionNames[key] && this.state.optionNames[key]=="uploadCount" && <Form.Item
                        {...restField}
                        name={[name, 'value']}
                        style={{width: 100}}
                        rules={[{required: false}]} >
                        <InputNumber defaultValue={1} value={1} />
                      </Form.Item>}
                      {/*Input*/}
                      {this.state.optionNames[key] && (this.state.optionNames[key]=="frontName"
                        // || this.state.optionNames[key]=="foreign"
                        || this.state.optionNames[key]=="tree"
                        || this.state.optionNames[key]=="option"

                      ) && <Form.Item
                        {...restField}
                        name={[name, 'value']}
                        style={{width: 100}}
                        rules={[{required: false}]} >
                        <Input />
                      </Form.Item>}
                      {/*Input*/}
                      {this.state.optionNames[key] && (
                        this.state.optionNames[key]=="foreign"
                        || this.state.optionNames[key]=="parent"

                      ) && <Form.Item tooltip={{trigger: "hover",placement: 'rightTop',color:"red", title:"样例：pay_info.id~name; [表名字].[Key字段]~[显示名字段]"}}
                                      {...restField}
                                      name={[name, 'value']}
                                      style={{width: 100}}
                                      rules={[{required: false}]} >
                        <Input />
                      </Form.Item>}
                      {/*Bool Select*/}
                      {this.state.optionNames[key] && (
                        this.state.optionNames[key]=="isFilter"
                        || this.state.optionNames[key]=="ignoreInList"
                      ) && <Form.Item
                        {...restField}
                        name={[name, 'value']}
                        style={{width: 100}}
                        rules={[{required: false}]} >
                        <Select value={1} placeholder={"请选择"}>
                          <Select.Option value={false}>否</Select.Option>
                          <Select.Option value={true}>是</Select.Option>
                        </Select>
                      </Form.Item>}
                      {/*Action Select*/}
                      {this.state.optionNames[key] && (
                         this.state.optionNames[key]=="approval"

                      ) && <Form.Item
                        {...restField}
                        name={[name, 'value']}
                        style={{width: 100}}
                        rules={[{required: false}]} >
                        <Input />
                      </Form.Item>}
                      {/*Front Home Select*/}
                      {this.state.optionNames[key] && this.state.optionNames[key]=="frontHome" && <Form.Item
                        {...restField}
                        name={[name, 'value']}
                        style={{width: 100}}
                        rules={[{required: false}]} >
                        <Select defaultValue={"list"}>
                          <Select.Option value={"list"}>List</Select.Option>
                          <Select.Option value={"tree"}>Tree</Select.Option>
                        </Select>
                      </Form.Item>}


                      <MinusCircleOutlined onClick={() => remove(name)} />
                    </Space>
                  ))}

                  <Form.Item>
                    <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                      {/*Add Extend Data Item*/}
                      添加扩展项
                    </Button>
                  </Form.Item>
                </>
              )}
            </Form.List>
          </FormItem>

        </Form>
      </Modal>
    )
  }
}

CodeTableModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default CodeTableModal

