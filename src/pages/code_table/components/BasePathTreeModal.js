import React,{PureComponent} from "react";
import {Modal, Tree} from "antd";
import {t} from "@lingui/macro";
// import React from "_@types_react@18.0.25@@types/react";


class BasePathTreeModal extends PureComponent {
  constructor(props) {
    super(props);
    this.setState({basePath: null})
  }

  handleSelect = (e) => {
    console.log(e)
    if(e.length>0) {
      this.setState({basePath: e[0]})
    } else {
      this.setState({basePath: null})
    }
  }

  handleOk = (e) => {
    const {onOk} = this.props;
    onOk(this.state.basePath)
  }

  render() {
    const {code_table, onCancel} = this.props
    const {trees} = code_table
    console.log(trees.baseDir)
    return <Modal
      open={code_table.showBasePath}
      title={t`Select Dir`}
      onOk={this.handleOk}
      onCancel={onCancel}
    >
      <Tree
        showLine={true}
        showIcon={false}
        defaultExpandedKeys={['/media/fang/ssd1t/home/fang/work/html/soho-admin-front']}
        onSelect={this.handleSelect}
        treeData={trees.baseDir == null ? [] : [trees.baseDir]}
      />
    </Modal>;
  }
}

export default BasePathTreeModal;
