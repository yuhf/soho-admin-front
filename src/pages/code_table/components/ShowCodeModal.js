import {PureComponent} from "react";
import {Button, Col, Divider, Input, Modal, Row, Space, Tabs, Tooltip} from 'antd';
import {t} from "@lingui/macro";
import CodeTab from "./CodeTab";


class ShowCodeModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      groupId: 0
    }
  }

  /**
   * 切换分组
   */
  handleChange = (key) => {
    const {queryTemplate} = this.props
    // alert("change")
    queryTemplate({groupId: key})
  }

  handleSaveGroup = () => {
    const {code_table,onSaveCode} = this.props
    const {templateId} = code_table.options
    let templateIds =Object.keys(templateId)
    onSaveCode({id: code_table.codeFile.id, 'templateId': templateIds, path: code_table.selectBasePath, codeNamespace: code_table.codeNamespace})
  }

  handleSaveFile = () => {
    const {code_table,onSaveCode} = this.props
    let templateIds = [];
    templateIds.push(code_table.codeFile.templateId)
    onSaveCode({id: code_table.codeFile.id, 'templateId': templateIds, path: code_table.selectBasePath, codeNamespace: code_table.codeNamespace})
  }

  /**
   * 下载当前文件
   */
  handleDownloadFile = () => {
    const {code_table} = this.props
    let name = code_table.codeFile.fileName;
    let start = name.lastIndexOf("/")
    // name = name.
    var aLink = document.createElement('a');
    var blob = new Blob([code_table.codeFile.body], {
      type: 'text/plain'
    });
    aLink.download = name.substr(start+1);
    aLink.href = URL.createObjectURL(blob);
    aLink.click();
    URL.revokeObjectURL(blob);
  }

  handleDownloadGroup = () => {
    const {code_table,getCodes} = this.props
    const {templateId} = code_table.options
    let templateIds =Object.keys(templateId)
    getCodes({id: code_table.codeFile.id, 'templateId': templateIds, path: code_table.selectBasePath, codeNamespace: code_table.codeNamespace})
  }

  handleCodeNamespaceChange = (e) => {
    const {onChangeCodeNamespace} = this.props;
    onChangeCodeNamespace(e.target.value)
  }

  render() {
    const {showCode,onCancel, options, queryCodeFile, code_table, showSelectDir} = this.props
    let items = []
    let groupId = code_table.options.groupId ? code_table.options.groupId[0].key : 0

    for(let i=0; i<options.groupId.length; i++) {
      items.push({
        label: options.groupId[i].label,
        key: options.groupId[i].value,
        children: <>
            <CodeTab queryCodeFile={queryCodeFile} code_table={code_table} />
          <Divider orientation={"left"}>附加配置</Divider>
            <Row>
              <Col sm={12}><Space align={"center"}><span>设置文件生成目录</span><Input value={code_table.selectBasePath} />
                <Tooltip title={"选择的基本路径请在对应模板组配置"}><Button onClick={showSelectDir}>请选择文件生成目录</Button></Tooltip></Space></Col>
              <Col sm={12}><Space align={"center"}><Tooltip title={"可选；生成java代码的时候需要用到"}>设置namespace</Tooltip><Input onChange={this.handleCodeNamespaceChange} defaultValue={code_table.codeNamespace} /></Space>  </Col>
            </Row>
          </>,
      })
    }

    return (
      <Modal
        open={showCode}
        title={t`Show Code`}
        onCancel={onCancel}
        width={1500}
        style={{height: "800px"}}
        footer={[
          <Button onClick={this.handleSaveFile}>保存当前文件</Button>,
          <Button onClick={this.handleSaveGroup}>保存组文件</Button>,
          <Button onClick={this.handleDownloadFile}>下载当前文件</Button>,
          <Button onClick={this.handleDownloadGroup}>下载当前组文件</Button>,
          <Button onClick={onCancel}>确认</Button>,
        ]}
        >
        <Tabs
          defaultActiveKey={groupId}
          onChange={this.handleChange}
          items={items}
        />

      </Modal>

    )
  }
}

ShowCodeModal.propTypes = {

}

export  default ShowCodeModal;
