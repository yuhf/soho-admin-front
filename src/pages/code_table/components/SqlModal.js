import React, {PureComponent} from "react";
import {Checkbox, Modal, Select} from "antd";
import {t} from "@lingui/macro";

class SqlModal extends PureComponent {
  state = {drop: false}

  handleExec = () => {
    const {onExec} = this.props
    console.log(this.state)
    onExec(this.state.drop)
  }

  onChangeDrop = (value) => {
    this.setState({drop: value})
  }

  render() {
    const {showSql} = this.props
    return (
      <Modal title={t`Create Table Sql`} open={showSql.status} onCancel={this.props.onCancel} onOk={this.handleExec} style={{width: "auto", height: "auto"}} width={"auto"} okText={t`Exec`}>
        <pre>{showSql.sql}</pre>
        <Select onChange={this.onChangeDrop} placeholder={"是否删除数据库原有表"} defaultValue={false}>
          <Select.Option value={true}>删除原表</Select.Option>
          <Select.Option value={false}>不删除表</Select.Option>
        </Select>
      </Modal>
    )
  }
}

SqlModal.propTypes = {

}

export default SqlModal;
