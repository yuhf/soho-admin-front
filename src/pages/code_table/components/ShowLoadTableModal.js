import {PureComponent} from "react";
import {Modal, Transfer} from "antd";
import {t} from "@lingui/macro";


const mockData = Array.from({ length: 20 }).map((_, i) => ({
  key: i.toString(),
  title: `content${i + 1}`,
  description: `description of content${i + 1}`,
}));

class ShowLoadTableModal extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedKeys: [],
      targetKeys: [],
    }
  }


  onChange = (nextTargetKeys, direction, moveKeys) => {
    console.log('targetKeys:', nextTargetKeys);
    console.log('direction:', direction);
    console.log('moveKeys:', moveKeys);
    this.setState({targetKeys: nextTargetKeys})
    //setTargetKeys(nextTargetKeys);
  };

  onSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
    console.log('sourceSelectedKeys:', sourceSelectedKeys);
    console.log('targetSelectedKeys:', targetSelectedKeys);
    //setSelectedKeys([...sourceSelectedKeys, ...targetSelectedKeys]);
    this.setState({selectedKeys: [...sourceSelectedKeys, ...targetSelectedKeys]})
  };

  onScroll = (direction, e) => {
    console.log('direction:', direction);
    console.log('target:', e.target);
  };

  handleOk = () => {
    const {onSaveTables} = this.props
    onSaveTables(this.state.targetKeys)
    // this.setState({targetKeys: []})
  }

  render() {
    const {dbTables} = this.props
    let data = [];
    for(let key in dbTables) {
      data.push({
        key: key,
        title: key,
        description: key
      })
    }

    return <Modal
      {...this.props}
      width={600}
      onOk={this.handleOk}
    >
      <Transfer
        dataSource={data}
        showSearch
        titles={[t`Database Tables`, t`Selected Tables`]}
        targetKeys={this.state.targetKeys}
        selectedKeys={this.state.selecedKeys}
        onScroll={this.onScroll}
        onSelectChange={this.onSelectChange}
        onChange={this.onChange}
        render={(item)=>item.title}
      />
    </Modal>;
  }
}


export default ShowLoadTableModal;
