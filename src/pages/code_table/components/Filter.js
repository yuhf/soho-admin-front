import React, { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { FilterItem } from 'components'
import { Trans,t } from "@lingui/macro"
import { Button, Row, Col, DatePicker, Form, Input, Cascader } from 'antd'
import FilterForm from '../../../components/FilterForm'

const { Search } = Input
const { RangePicker } = DatePicker
const ColProps = {
  xs: 24,
  sm: 12,
  style: {
    marginBottom: 16,
  },
}

const TwoColProps = {
  ...ColProps,
  xl: 96,
}

class Filter extends Component {
  formRef = React.createRef()

  handleFields = fields => {
    const { betweenCreatedTime } = fields
    if (betweenCreatedTime && betweenCreatedTime.length) {
      fields.betweenCreatedTime = [
        moment(betweenCreatedTime[0]).format('YYYY-MM-DD'),
        moment(betweenCreatedTime[1]).format('YYYY-MM-DD'),
      ]
    }
    return fields
  }

  handleSubmit = () => {
    const { onFilterChange } = this.props
    const values = this.formRef.current.getFieldsValue()
    const fields = this.handleFields(values)
    onFilterChange(fields)
  }

  handleReset = () => {
    const fields = this.formRef.current.getFieldsValue()
    for (let item in fields) {
      if ({}.hasOwnProperty.call(fields, item)) {
        if (fields[item] instanceof Array) {
          fields[item] = []
        } else {
          fields[item] = undefined
        }
      }
    }
    this.formRef.current.setFieldsValue(fields)
    this.handleSubmit()
  }
  handleChange = (key, values) => {
    const { onFilterChange } = this.props
    let fields = this.formRef.current.getFieldsValue()
    fields[key] = values
    fields = this.handleFields(fields)
    onFilterChange(fields)
  }

  render() {
    const { onAdd, filter,onLoadTables } = this.props
    const { name } = filter

    let initialCreateTime = []
    if (filter.createTime && filter.createTime[0]) {
      initialCreateTime[0] = moment(filter.createTime[0])
    }
    if (filter.createTime && filter.createTime[1]) {
      initialCreateTime[1] = moment(filter.createTime[1])
    }

    return (
      <FilterForm onAdd={onAdd} onSubmit={this.handleSubmit} ref={this.formRef} name="control-ref" initialValues={{ name, createdTime: initialCreateTime }} actions={<Button type="ghost" onClick={onLoadTables}>
        <Trans>Load Table</Trans>
      </Button>}>
            <Form.Item name="name">
              <Input
                placeholder={t`Name`}
              />
            </Form.Item>
            <Form.Item name="title">
              <Input
                placeholder={t`Title`}
              />
            </Form.Item>
      </FilterForm>
    )
  }
}

Filter.propTypes = {
  onAdd: PropTypes.func,
  filter: PropTypes.object,
  onFilterChange: PropTypes.func,
}

export default Filter

