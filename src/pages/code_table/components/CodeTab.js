import {PureComponent} from "react";
import {Modal, Tabs, notification, Typography} from 'antd';
import {t} from "@lingui/macro";
import TextArea from "antd/es/input/TextArea";
import copy from 'copy-to-clipboard';
const { Title,Paragraph } = Typography;

class CodeTab extends PureComponent {
  constructor(props) {
    super(props);
  }

  //更换tab事件
  handleChange = (key) => {
    const {queryCodeFile, code_table} = this.props
    queryCodeFile({templateId: key, id: code_table.currentItem.id, codeNamespace: code_table.codeNamespace})
  }

  handleDuClickCopy = (e) => {
    copy(e.target.innerText)
    notification.success({message: "已经复制"})
  }

  render() {
    const {code_table} = this.props
    const {options} = code_table
    console.log(options)
    let items = [
    ]
    for (var key in options.templateId) {
      items.push({
        label: options.templateId[key],
        key: key,
        children: <div>
          <Paragraph><Title copyable={{text: code_table.codeFile.fileName}} >{code_table.codeFile.fileName}</Title></Paragraph>
          <Paragraph copyable={{text: code_table.codeFile.body}} ><pre onDoubleClick={this.handleDuClickCopy}>{code_table.codeFile.body}</pre></Paragraph>
        </div>,
      })
    }

    return (
      <Tabs
        tabPosition={"left"}
        items={items}
        onChange={this.handleChange}
        >


      </Tabs>
    );
  }
}
CodeTab.propTypes = {

}
export default CodeTab;
