import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryChatUserList, updateChatUser, createChatUser,deleteChatUser,queryChatUserToken } = api
/**
 *
   //相关api信息
 queryChatUserList: `GET ${prefix}/admin/chatUser/list`,
 updateChatUser: `PUT ${prefix}/admin/chatUser`,
 createChatUser: `POST ${prefix}/admin/chatUser`,
 deleteChatUser: `DELETE ${prefix}/admin/chatUser/:ids`,
 queryChatUserDetails: `GET ${prefix}/admin/chatUser/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'chat_user',
  state: {
    options: {    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/chat_user').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryChatUserList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
    //获取用户token
    *token({ payload }, { call, put }) {
      const data = yield call(queryChatUserToken, payload)
      if (data.success) {
        return data;
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createChatUser, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateChatUser, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteChatUser, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
