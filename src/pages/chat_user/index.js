import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import ChatUserModal from "./components/Modal";
import Filter from "./components/Filter"
import {message, Modal} from "antd"
import {ExclamationCircleOutlined} from "@ant-design/icons";
const { success } = Modal;

@connect(({ chat_user, loading }) => ({ chat_user, loading }))
class ChatUser extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { chat_user, loading, location, dispatch } = this.props
    const { list, pagination } = chat_user
    const { query, pathname } = location
    return {
      options: chat_user.options,
      trees: chat_user.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['chat_user/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'chat_user/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'chat_user/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onApplyItem(item) {
        dispatch({
          type: 'chat_user/showModal',
          payload: {
            modalType: 'apply',
            currentItem: item,
          }
        })
      },
      //创建token
      onCreateToken(item) {
        dispatch({
          type: 'chat_user/token',
          payload: {
            id: item.id,
          }
        }).then(data=>{
          success({
            content: data.payload.token,
          })
        });
      },
      onChildItem(item) {
        dispatch({
          type: 'chat_user/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, chat_user, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = chat_user

    return {
      options: chat_user.options,
      trees: chat_user.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`chat_user/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
          modalType === 'create' ? t`Create` : (modalType === 'update' ? t`Update` : t`Apply`)
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `chat_user/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'chat_user/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, chat_user} = this.props
    const {query,pathname} = location
    return {
      options: chat_user.options,
      trees: chat_user.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'chat_user/showModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <ChatUserModal {...this.modalProps} />
      </Page>
    )
  }
}

ChatUser.propTypes = {
  chat_user: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default ChatUser
