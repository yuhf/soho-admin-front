import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryAdminSmsTemplateList, updateAdminSmsTemplate, createAdminSmsTemplate,deleteAdminSmsTemplate } = api
/**
 *
   //相关api信息
 queryAdminSmsTemplateList: `GET ${prefix}/admin/adminSmsTemplate/list`,
 updateAdminSmsTemplate: `PUT ${prefix}/admin/adminSmsTemplate`,
 createAdminSmsTemplate: `POST ${prefix}/admin/adminSmsTemplate`,
 deleteAdminSmsTemplate: `DELETE ${prefix}/admin/adminSmsTemplate/:ids`,
 queryAdminSmsTemplateDetails: `GET ${prefix}/admin/adminSmsTemplate/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'admin_sms_template',
  state: {
    options: {
      adapterName: {
           'aliyun': 'aliyun',
           'tencent': 'tencent',
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/admin_sms_template').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryAdminSmsTemplateList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createAdminSmsTemplate, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateAdminSmsTemplate, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteAdminSmsTemplate, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})