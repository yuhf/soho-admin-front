import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryChatSessionUserList, updateChatSessionUser, createChatSessionUser,deleteChatSessionUser } = api
/**
 *
   //相关api信息
 queryChatSessionUserList: `GET ${prefix}/admin/chatSessionUser/list`,
 updateChatSessionUser: `PUT ${prefix}/admin/chatSessionUser`,
 createChatSessionUser: `POST ${prefix}/admin/chatSessionUser`,
 deleteChatSessionUser: `DELETE ${prefix}/admin/chatSessionUser/:ids`,
 queryChatSessionUserDetails: `GET ${prefix}/admin/chatSessionUser/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'chat_session_user',
  state: {
    options: {    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/chat_session_user').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryChatSessionUserList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createChatSessionUser, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateChatSessionUser, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteChatSessionUser, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})