import { parse } from 'qs'
import modelExtend from 'dva-model-extend'
import api from 'api'
const { pathToRegexp } = require("path-to-regexp")
import { model } from 'utils/model'

const { queryDashboard, queryWeather,queryDashboardNumberCard } = api
const avatar = '//cdn.antd-admin.zuiidea.com/bc442cf0cc6f7940dcc567e465048d1a8d634493198c4-sPx5BR_fw236.jpeg'

export default modelExtend(model, {
  namespace: 'dashboard',
  state: {
    listCards:[],
    listKVCards:[],
    listUserCard: [],
    numbers: [],
    recentSales: [],
    comments: [],
    completed: [],
    browser: [],
    cpu: {},
    user: {
      avatar,
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(({ pathname }) => {
        if (
          pathToRegexp('/dashboard').exec(pathname) ||
          pathToRegexp('/').exec(pathname)
        ) {
          dispatch({ type: 'query' })
          dispatch({ type: 'queryNumberCard' })

        }
      })
    },
  },
  effects: {
    //查询数字卡片数据
    *queryNumberCard({payload}, {call, put}) {
      const data = yield  call(queryDashboardNumberCard)
      if(data.success) {
        console.log(data)
        yield put({type:"updateState",payload: data.payload})
      }
    },

    *query({ payload }, { call, put }) {
      // const data = yield call(queryDashboard, parse(payload))
      const data = {
        "sales":[
          {"name":2008,"Clothes":361,"Food":335,"Electronics":302},
          {"name":2009,"Clothes":210,"Food":241,"Electronics":488},
          {"name":2010,"Clothes":393,"Food":336,"Electronics":320},
          {"name":2011,"Clothes":206,"Food":192,"Electronics":534},
          {"name":2012,"Clothes":366,"Food":269,"Electronics":458},
          {"name":2013,"Clothes":472,"Food":381,"Electronics":378},
          {"name":2014,"Clothes":406,"Food":277,"Electronics":489},
          {"name":2015,"Clothes":367,"Food":205,"Electronics":363}],
        "cpu":{"usage":562,"space":825,"cpu":47,
          "data":[
            {"cpu":22},
            {"cpu":59},
            {"cpu":52},
            {"cpu":50},{"cpu":67},{"cpu":31},
            {"cpu":69},{"cpu":64},{"cpu":31},
            {"cpu":57},{"cpu":73},{"cpu":35},
            {"cpu":45},{"cpu":24},{"cpu":55},{"cpu":57},
            {"cpu":76},{"cpu":39},{"cpu":77},{"cpu":21}]},
        "browser":[
          {"name":"Google Chrome","percent":43.3,"status":1},
          {"name":"Mozilla Firefox","percent":33.4,"status":2},{"name":"Apple Safari","percent":34.6,"status":3},
          {"name":"Internet Explorer","percent":12.3,"status":4},{"name":"Opera Mini","percent":3.3,"status":1},
          {"name":"Chromium","percent":2.53,"status":1}],
        "user":{"name":"github","sales":3241,"sold":3556},
        "completed":[
          {"name":2008,"Task complete":976,"Cards Complete":573},
          {"name":2009,"Task complete":456,"Cards Complete":761},
          {"name":2010,"Task complete":804,"Cards Complete":461},
          {"name":2011,"Task complete":770,"Cards Complete":942},
          {"name":2012,"Task complete":203,"Cards Complete":723},
          {"name":2013,"Task complete":510,"Cards Complete":580},
          {"name":2014,"Task complete":309,"Cards Complete":749},
          {"name":2015,"Task complete":676,"Cards Complete":867},{"name":2016,"Task complete":631,"Cards Complete":315},
          {"name":2017,"Task complete":773,"Cards Complete":848},{"name":2018,"Task complete":275,"Cards Complete":400},
          {"name":2019,"Task complete":664,"Cards Complete":576}],
        "comments":[{"name":"Harris","status":2,"content":"Owxoweubul tcabtv zfeq hbknbqbmv gjjkuso rthvnvwk suzf cjlt lnhhcdkp jbxkcresd ugbepwl pvjyua qvossxpo town jwg kdjhbsin yeic.","avatar":"http://dummyimage.com/48x48/f279c9/757575.png&text=H","date":"2016-04-08 21:23:00"},
          {"name":"Perez","status":3,"content":"Dblgxglg ibpufngk tndxu yuxskcnl mqhbux exl zfjnju tdiwcgh suspb lumskb ubyopwiad dew inxdu jlvv fblyjl snqpm yrfiplbgn.","avatar":"http://dummyimage.com/48x48/79ecf2/757575.png&text=P","date":"2016-06-06 17:48:27"},
          {"name":"Thomas","status":2,"content":"Feqsvfply ppvt estxf gwpn ubyocoj mcvvhitlx zcdkqrec bfx jdkwpvyw fvuhsdtav iecxqsrte qchl.","avatar":"http://dummyimage.com/48x48/f2d479/757575.png&text=T","date":"2016-10-16 10:22:05"},
          {"name":"Jones","status":3,"content":"Dwdulr dakhsw wglrsxsfr kpsfgskmqe xrakxw uosk rdqompcdn sccpbptg vpavnul qaf dtunouwio gpbnhxi bjypah yybwga hnols rjeoznuvz.","avatar":"http://dummyimage.com/48x48/b179f2/757575.png&text=J","date":"2016-04-15 20:49:23"},
          {"name":"Rodriguez","status":2,"content":"Vjilqyjt gkoe kmbchtvvc usxocpvdr gtu ehdchslt dxcy ajoisrh douuss kxi cahcdbembw dvomiq sfemr jpbpxavgm iqhhfl.","avatar":"http://dummyimage.com/48x48/79f28e/757575.png&text=R","date":"2016-03-19 00:05:53"}],
        "recentSales":[
          {"id":1,"name":"Davis","status":4,"price":148.76,"date":"2016-04-10 07:04:53"},
          {"id":2,"name":"Walker","status":2,"price":40.1,"date":"2015-10-31 19:33:58"},
          {"id":3,"name":"Lopez","status":4,"price":116.84,"date":"2016-05-04 10:10:34"},
          {"id":4,"name":"Allen","status":2,"price":136.43,"date":"2015-01-29 12:09:44"},
          {"id":5,"name":"White","status":2,"price":166.7,"date":"2015-11-24 20:10:47"},
          {"id":6,"name":"Smith","status":3,"price":85.1,"date":"2015-05-14 06:39:07"},
          {"id":7,"name":"White","status":1,"price":195.8,"date":"2016-06-19 04:54:53"},
          {"id":8,"name":"Johnson","status":2,"price":179.24,"date":"2015-08-19 05:22:59"},
          {"id":9,"name":"Clark","status":1,"price":116.54,"date":"2016-05-19 19:24:38"},
          {"id":10,"name":"Williams","status":4,"price":173.3,"date":"2016-05-09 17:39:24"},
          {"id":11,"name":"Clark","status":4,"price":128.5,"date":"2015-06-06 11:20:42"},
          {"id":12,"name":"Allen","status":2,"price":168.25,"date":"2015-07-06 17:22:20"},
          {"id":13,"name":"Williams","status":1,"price":187.3,"date":"2015-01-11 00:06:13"},
          {"id":14,"name":"Wilson","status":2,"price":15.5,"date":"2016-09-23 16:56:36"},{"id":15,"name":"White","status":4,"price":59.65,"date":"2016-07-12 12:26:07"},
          {"id":16,"name":"Young","status":3,"price":171.27,"date":"2016-12-17 04:05:10"},{"id":17,"name":"Moore","status":3,"price":48.16,"date":"2016-03-18 14:52:18"},
          {"id":18,"name":"Williams","status":2,"price":183.51,"date":"2015-09-13 00:03:13"},{"id":19,"name":"Brown","status":3,"price":28.63,"date":"2015-10-02 16:33:10"},
          {"id":20,"name":"Jones","status":1,"price":143.5,"date":"2016-03-30 15:43:21"},{"id":21,"name":"Taylor","status":1,"price":196.9,"date":"2015-07-09 01:01:30"},
          {"id":22,"name":"Davis","status":2,"price":21.7,"date":"2015-03-24 13:39:51"},{"id":23,"name":"Clark","status":3,"price":199.5,"date":"2015-10-23 14:04:06"},
          {"id":24,"name":"Gonzalez","status":2,"price":77.8,"date":"2015-03-01 12:17:10"},{"id":25,"name":"Martin","status":1,"price":90.69,"date":"2015-07-20 10:10:45"},
          {"id":26,"name":"Allen","status":2,"price":173.6,"date":"2015-05-14 18:40:12"},{"id":27,"name":"Martinez","status":2,"price":48.6,"date":"2015-01-11 09:33:08"},
          {"id":28,"name":"Lewis","status":3,"price":68.6,"date":"2015-07-09 20:54:02"},{"id":29,"name":"Clark","status":2,"price":197.25,"date":"2016-09-26 04:08:36"},
          {"id":30,"name":"Thompson","status":4,"price":127.4,"date":"2015-05-27 03:42:21"},{"id":31,"name":"Moore","status":2,"price":100.4,"date":"2015-03-06 12:19:16"},
          {"id":32,"name":"Lewis","status":3,"price":153.8,"date":"2015-12-19 14:49:58"},{"id":33,"name":"Clark","status":1,"price":33.89,"date":"2015-09-22 12:01:37"},
          {"id":34,"name":"Jones","status":3,"price":28.33,"date":"2015-04-27 07:11:06"},{"id":35,"name":"Robinson","status":2,"price":82.7,"date":"2016-11-08 13:29:39"},
          {"id":36,"name":"Taylor","status":2,"price":84.58,"date":"2016-10-07 03:45:31"}],
        "quote":{"name":"Joho Doe","title":"Graphic Designer","content":"I'm selfish, impatient and a little insecure. I make mistakes, I am out of control and at times hard to handle. But if you can't handle me at my worst, then you sure as hell don't deserve me at my best.","avatar":"//cdn.antd-admin.zuiidea.com/bc442cf0cc6f7940dcc567e465048d1a8d634493198c4-sPx5BR_fw236"},
        "numbers":[
          {"icon":"pay-circle-o","color":"#64ea91","title":"Online Review","number":2781},
          {"icon":"team","color":"#8fc9fb","title":"New Customers","number":3241},
          {"icon":"message","color":"#d897eb","title":"Active Projects","number":253},
          {"icon":"shopping-cart","color":"#f69899","title":"Referrals","number":4324}
        ]
      }
      yield put({
        type: 'updateState',
        payload: data,
      })
    },
  },
})
