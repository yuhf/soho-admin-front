import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'umi'
import { Row, Col, Card } from 'antd'
import { Color } from 'utils'
import { Page, ScrollBar } from 'components'
import {
  NumberCard,
  Quote,
  Sales,
  Weather,
  RecentSales,
  Comments,
  Completed,
  Browser,
  Cpu,
  User,
} from './components'
import styles from './index.less'
import store from 'store'

const bodyStyle = {
  bodyStyle: {
    height: 432,
    background: '#fff',
  },
}

@connect(({ app, dashboard, loading }) => ({
  dashboard,
  loading,
}))
class Dashboard extends PureComponent {
  render() {
    const userDetail = store.get('user')
    // const { avatar, username } = userDetail
    const { dashboard, loading } = this.props
    const {
      numbers,
      listCards,
      listKVCards,
      listUserCard,
      recentSales,
      comments,
      completed,
      browser,
      cpu,
      user,
    } = dashboard

    const numberCards = numbers.map((item, key) => (
      <Col key={"1-"+key} lg={6} md={12}>
        <NumberCard {...item} />
      </Col>
    ))
    //列表卡片生成
    const listCardsObject = listCards.map((item, key) => (
      <Col lg={12} md={24} key={"2-"+key}>
        <Card bordered={false} {...bodyStyle}>
          <RecentSales data={item} />
        </Card>
      </Col>
    ))
    //kv数据结构显示
    const listKVCardsObject = listKVCards.map((item, key) => (
      <Col lg={8} md={24} key={"3+"+key}>
        <Card bordered={false} {...bodyStyle}>
          <Browser data={item} />
        </Card>
      </Col>
    ))

    const listUserCardObject  =  listUserCard.map((item,key)=>(
      <Col lg={8} md={24} key={"4-"+key}>
        <Card
          bordered={false}
          bodyStyle={{ ...bodyStyle.bodyStyle, padding: 0 }}
        >
          <User {...item} />
        </Card>
      </Col>
    ))

    return (
      <Page
        // loading={loading.models.dashboard && sales.length === 0}
        className={styles.dashboard}
      >
        <Row gutter={24}>
          {numberCards}
          {listCardsObject}
          {listKVCardsObject}
          {listUserCardObject}
          {/*<Col lg={12} md={24}>*/}
          {/*  <Card bordered={false} {...bodyStyle}>*/}
          {/*    <RecentSales data={recentSales} />*/}
          {/*  </Card>*/}
          {/*</Col>*/}
          {/*<Col lg={12} md={24}>*/}
          {/*  <Card bordered={false} {...bodyStyle}>*/}
          {/*    <ScrollBar>*/}
          {/*      <Comments data={comments} />*/}
          {/*    </ScrollBar>*/}
          {/*  </Card>*/}
          {/*</Col>*/}

          {/*图标；折线图*/}
          {/*<Col lg={24} md={24}>*/}
          {/*  <Card*/}
          {/*    bordered={false}*/}
          {/*    bodyStyle={{*/}
          {/*      padding: '24px 36px 24px 0',*/}
          {/*    }}*/}
          {/*  >*/}
          {/*    <Completed data={completed} />*/}
          {/*  </Card>*/}
          {/*</Col>*/}
          {/*<Col lg={8} md={24}>*/}
          {/*  <Card bordered={false} {...bodyStyle}>*/}
          {/*    <Browser data={browser} />*/}
          {/*  </Card>*/}
          {/*</Col>*/}
          {/*<Col lg={8} md={24}>*/}
          {/*  <Card bordered={false} {...bodyStyle}>*/}
          {/*    <ScrollBar>*/}
          {/*      <Cpu {...cpu} />*/}
          {/*    </ScrollBar>*/}
          {/*  </Card>*/}
          {/*</Col>*/}
          {/*<Col lg={8} md={24}>*/}
          {/*  <Card*/}
          {/*    bordered={false}*/}
          {/*    bodyStyle={{ ...bodyStyle.bodyStyle, padding: 0 }}*/}
          {/*  >*/}
          {/*    <User {...user} avatar={avatar} username={username} />*/}
          {/*  </Card>*/}
          {/*</Col>*/}
        </Row>
      </Page>
    )
  }
}

Dashboard.propTypes = {
  dashboard: PropTypes.object,
  loading: PropTypes.object,
}

export default Dashboard
