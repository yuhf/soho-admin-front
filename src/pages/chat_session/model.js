import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryChatSessionList, updateChatSession, createChatSession,deleteChatSession,sendChatSessionMessage } = api
/**
 *
   //相关api信息
 queryChatSessionList: `GET ${prefix}/admin/chatSession/list`,
 updateChatSession: `PUT ${prefix}/admin/chatSession`,
 createChatSession: `POST ${prefix}/admin/chatSession`,
 deleteChatSession: `DELETE ${prefix}/admin/chatSession/:ids`,
 queryChatSessionDetails: `GET ${prefix}/admin/chatSession/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'chat_session',
  state: {
    options: {
      type: {
           '1': '私聊',
           '2': '群聊',
           '3': '群组',
           '4': '客服'
      },

      status: {
           '1': '活跃',
           '2': '禁用',
           '3': '删除',
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/chat_session').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryChatSessionList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createChatSession, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    //发送消息
   *createSendChatSessionMessage({ payload }, { call, put }) {
      const data = yield call(sendChatSessionMessage, payload)
      if (data.success) {
        yield put({ type: 'hideSendModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateChatSession, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteChatSession, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },

    //显示发送消息框
    showSendModal(state, { payload }) {
      return { ...state, ...payload, sendModalVisible: true }
    },
    //隐藏消息发送框
    hideSendModal(state) {
      return { ...state, sendModalVisible: false }
    },
  },
})
