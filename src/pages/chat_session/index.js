import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import ChatSessionModal from "./components/Modal";
import Filter from "./components/Filter"
import SendModal from "./components/SendModal";


@connect(({ chat_session, loading }) => ({ chat_session, loading }))
class ChatSession extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { chat_session, loading, location, dispatch } = this.props
    const { list, pagination } = chat_session
    const { query, pathname } = location
    return {
      options: chat_session.options,
      trees: chat_session.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['chat_session/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'chat_session/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'chat_session/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onApplyItem(item) {
        dispatch({
          type: 'chat_session/showModal',
          payload: {
            modalType: 'apply',
            currentItem: item,
          }
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'chat_session/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, chat_session, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = chat_session

    return {
      options: chat_session.options,
      trees: chat_session.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`chat_session/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
          modalType === 'create' ? t`Create` : (modalType === 'update' ? t`Update` : t`Apply`)
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `chat_session/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'chat_session/hideModal',
        })
      },
    }
  }

  //获取发送消息弹窗参数
  get sendModalProps() {
    const { dispatch, chat_session, loading } = this.props
    const { currentItem, sendModalVisible, modalType,resourceTree,resourceIds } = chat_session

    return {
      // options: chat_session.options,
      // trees: chat_session.trees,
      // item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: sendModalVisible,
      destroyOnClose: true,
      maskClosable: false,
      // confirmLoading: loading.effects[`chat_session/${modalType}`],
      // resourceTree: resourceTree,
      // resourceIds: resourceIds,
      title: t`Send Message`,
      centered: true,
      onOk: data => {
        // dispatch({
        //   type: `chat_session/${modalType}`,
        //   payload: data,
        // }).then(() => {
        //   this.handleRefresh()
        // })
        console.log(data)
      },
      onSend: data => {
        //TODO 调用接口发送消息
        console.log(data)
        dispatch({
          type: `chat_session/createSendChatSessionMessage`,
          payload: data,
        }).then((res)=>{
          console.log(res)
        });
      },
      onCancel() {
        dispatch({
          type: 'chat_session/hideSendModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, chat_session} = this.props
    const {query,pathname} = location
    return {
      options: chat_session.options,
      trees: chat_session.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'chat_session/showModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
      onSend() {
        dispatch({
          type: 'chat_session/showSendModal',
          payload: {
            // modalType: 'create',
            // currentItem: {},
          },
        })
        console.log("is runing....")
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <ChatSessionModal {...this.modalProps} />
        <SendModal {...this.sendModalProps} />
        {/*<SendModal visible={true} />*/}
      </Page>
    )
  }
}

ChatSession.propTypes = {
  chat_session: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default ChatSession
