import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import {t, Trans} from "@lingui/macro"

const { TextArea } = Input;

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class SendModal extends PureComponent {
  formRef = React.createRef()

  handleOk = () => {
    const { item = {}, onOk,onSend } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          // id: item.id,          //时间值处理
        }
        onSend(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props

    let initData = {}

    return (
      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
          <Form.Item  name='msgType' rules={[{ required: 0 }]}  label={t`Type`} hasFeedback {...formItemLayout} >
            <Select>
              <Select.Option value={1}>系统消息</Select.Option>
              <Select.Option value={2}>指定用户</Select.Option>
            </Select>
          </Form.Item>
          <Form.Item  name='sessionIds' rules={[{ required: 0 }]} label={t`Session ID`} hasFeedback {...formItemLayout} >
            <Select
              showSearch
              mode="tags"
              // size={size}
              placeholder="Please select"
              // defaultValue={['a10', 'c12']}
              // onChange={handleChange}
              style={{ width: '100%' }}
              options={options}
            />
          </Form.Item>
          <Form.Item name='content' rules={[{ required: 0 }]} label={t`Type`} hasFeedback {...formItemLayout} >
            <TextArea />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}

// SendModal.prototype = {
//   type: PropTypes.string,
//   item: PropTypes.object,
//   onOk: PropTypes.func,
// }

export default SendModal;
