import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import QuillEditor from "../../../components/Editor/QuillEditor";
import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class ChatSessionModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props
    const typeOptionData = Object.keys(options.type).map((k,index)=>{
      return {value: parseInt(k), label: options.type[k]}
    })
    const statusOptionData = Object.keys(options.status).map((k,index)=>{
      return {value: parseInt(k), label: options.status[k]}
    })
    let initData = {
                    ...item};
    //初始化时间处理
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
         <FormItem  name='type' rules={[{ required: 0 }]}            label={t`Type`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Type`}
             optionFilterProp="children"
             options={typeOptionData}
             disabled={false}
             />
          </FormItem>
         <FormItem  name='status' rules={[{ required: 0 }]}            label={t`Status`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Status`}
             optionFilterProp="children"
             options={statusOptionData}
             disabled={false}
             />
          </FormItem>
         <FormItem  name='title' rules={[{ required: 0 }]}            label={t`Title`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={45} />
          </FormItem>
         <FormItem  name='avatar' rules={[{ required: 0 }]}            label={t`Avatar`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={255} />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

ChatSessionModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default ChatSessionModal
