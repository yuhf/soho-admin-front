import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryAdminUserLoginLogList, updateAdminUserLoginLog, createAdminUserLoginLog,deleteAdminUserLoginLog,queryAdminUserOptions } = api
/**
 *
   //相关api信息
 queryAdminUserLoginLogList: `GET ${prefix}/admin/adminUserLoginLog/list`,
 updateAdminUserLoginLog: `PUT ${prefix}/admin/adminUserLoginLog`,
 createAdminUserLoginLog: `POST ${prefix}/admin/adminUserLoginLog`,
 deleteAdminUserLoginLog: `DELETE ${prefix}/admin/adminUserLoginLog/:ids`,
 queryAdminUserLoginLogDetails: `GET ${prefix}/admin/adminUserLoginLog/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'admin_user_login_log',
  state: {
    options: {
      adminUserId: {
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/admin_user_login_log').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
          dispatch({
            type: 'queryAdminUserOptions',
            payload: {},
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryAdminUserLoginLogList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createAdminUserLoginLog, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateAdminUserLoginLog, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteAdminUserLoginLog, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
    *queryAdminUserOptions({ payload }, { call, put }) {
      const data = yield call(queryAdminUserOptions, payload)
      if (data.success) {
        let options = {}
        for (let i = 0; i < data.payload.length; i++) {
          let item = data.payload[i];
          options[item.id+""] = item.username;
        }
        yield put({
          type: 'queryOptions',
          payload: {
              adminUserId: options
          },
        })
      } else {
        throw data
      }
    },  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
