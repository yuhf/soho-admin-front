import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'umi'
import { Tabs } from 'antd'
import { history } from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import Modal from "./components/Modal";

const { TabPane } = Tabs

const EnumPostStatus = {
  PUBLIC: "public",
  UNPUBLISH: 1,
  PUBLISHED: 2,
}

@connect(({ config, loading }) => ({ config, loading }))
class Config extends PureComponent {

  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        groupKey: key,
      }),
    })
  }
  refresh = key => {
    const { pathname, query } = this.props.location
    let groupKey = query.groupKey === undefined
      ? String(EnumPostStatus.PUBLIC)
      : query.groupKey;
    history.push({
      pathname,
      search: stringify({
        groupKey: groupKey,
        ...key
      }),
    })
  }

  get listProps() {
    const { dispatch, config, loading, location } = this.props
    const { list, pagination } = config
    const { query, pathname } = location
    const refresh = this.refresh
    return {
      pagination,
      dataSource: list,
      loading: loading.effects['config/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },

      //编辑指定行记录
      onEditItem(recode) {
        dispatch({
          type: 'config/showModal',
          payload: {item: recode, modalType: 'update'},
        })
        .then(() => {
          refresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      //删除指定当记录
      onDeleteItem(id) {
        dispatch({
          type: 'config/delete',
          payload: id,
        }).then(() => {
          refresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, config, loading } = this.props
    const { item, modalVisible, modalType } = config

    return {
      item: modalType === 'create' ? {} : item,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`role/${modalType}`],
      title: `${
        modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `config/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleTabClick(item.groupKey)
        })
      },
      onCancel() {
        dispatch({
          type: 'config/hideModal',
        })
      },
    }
  }

  render() {
    const { location,config } = this.props
    const { query } = location
    const {groupList} = config

    const tabPans = []
    let list = groupList ? groupList : []
    list.forEach((item)=>{
      tabPans.push(
        <TabPane
          tab={item.name}
          key={item.key}
        >
          <List {...this.listProps} />
        </TabPane>
      )
    })

    return (
      <Page inner>
        <Tabs
          activeKey={
            query.groupKey === undefined
              ? String(EnumPostStatus.PUBLIC)
              : query.groupKey
          }
          onTabClick={this.handleTabClick}
        >
          {tabPans}
        </Tabs>
        <Modal {...this.modalProps} />
      </Page>
    )
  }
}

Config.propTypes = {
  config: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default Config
