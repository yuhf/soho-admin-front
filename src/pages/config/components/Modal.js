import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Cascader, TreeSelect, Switch, message, Upload, Button} from 'antd'
import { Trans } from "@lingui/macro"
import city from 'utils/city'
import { t } from "@lingui/macro"
import TextArea from "antd/es/input/TextArea";
import {UploadOutlined} from "@ant-design/icons";
import UploadEditor from "../../../components/Upload/UploadEditor";
import VanillaJSONEditor from "../../../components/JsonEditor/VanillaJSONEditor";

const FormItem = Form.Item

const { SHOW_PARENT } = TreeSelect;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class UserModal extends PureComponent {
  formRef = React.createRef()
  resourceIds = this.props.resourceIds
  state = {
    value: this.resourceIds
  };

  onChange = value => {
    console.log(value);
    this.setState({ value });
  };

  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        console.log("save value.....")
        console.log(values.value)
        const data = {
          ...values,
          id: item.id,
          value: item.type == 2 ? values.value : values.value,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, form, ...modalProps } = this.props

    let content = { text: '[1,2,3]' };


    return (
      <Modal {...modalProps} onOk={this.handleOk} width={1000}>
        <Form ref={this.formRef} name="control-ref" initialValues={{...item, value: (item.type==3 ? item.value == "true" : item.value) }} layout="horizontal">
          <FormItem name='key' rules={[{ required: true }]}
            label={t`Key`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          {/*文本编辑*/}
          {item.type === 1 &&
            <FormItem name='value' rules={[{ required: true }]} label={t`Value`} hasFeedback {...formItemLayout}>
              <TextArea rows={4} />
            </FormItem>
          }
          {/*Json编辑*/}
          {item.type === 2 &&
            <FormItem name='value' rules={[{ required: true }]} label={t`Value`} hasFeedback {...formItemLayout}>
              <VanillaJSONEditor />
            </FormItem>
          }
          {/*bool编辑*/}
          {item.type == 3 &&
            <FormItem name='value' valuePropName='checked' rules={[{ required: true }]} label={t`Value`} hasFeedback {...formItemLayout}>
              <Switch />
            </FormItem>
          }
          {/*upload编辑*/}
          {item.type == 4 &&
            <FormItem name='value' rules={[{ required: true }]} label={t`Value`} hasFeedback {...formItemLayout}>
              <UploadEditor />
            </FormItem>
          }
          <FormItem name='explain' rules={[{ required: true }]}
            label={t`Explain`} hasFeedback {...formItemLayout}>
            <TextArea rows={4} />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

UserModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
  resourceTree: PropTypes.object
}

export default UserModal
