import React, { PureComponent } from 'react'
import {Table, Avatar, Modal, Typography} from 'antd'
import {t, Trans} from "@lingui/macro"
import { I18n } from "@lingui/core";
import { Ellipsis } from 'components'
import styles from './List.less'
import { DropOption } from 'components'
const { Text } = Typography;

const { confirm } = Modal

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const { onDeleteItem, onEditItem } = this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    }
  }


  render() {
    const { ...tableProps } = this.props
    const columns = [
      {
        title: t`ID`,
        dataIndex: 'id',
      },
      {
        title: t`Key`,
        dataIndex: 'key',
      },
      {
        title: t`Value`,
        dataIndex: 'value',
        render:(text, recode) => {
          if(text != null && text.length>30) {
            const start = text.slice(0, 30).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Explain`,
        dataIndex: 'explain',
        width: '20%',
        render: (text, recode) => {
          if(text != null && text.length>30) {
            const start = text.slice(0, 30).trim();
            return <Text style={{ maxWidth: '100%' }} ellipsis={ "..." }>{start}</Text>
          } else {
            return text
          }
        }
      },
      {
        title: t`Updated Time`,
        dataIndex: 'updatedTime',
      },
      {
        title: t`Created Time`,
        dataIndex: 'createdTime',
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                { key: '1', name: t`Update` },
                { key: '2', name: t`Delete` },
              ]}
            />
          )
        },
      },
    ]

    return (
      <Table
        {...tableProps}
        pagination={{
          ...tableProps.pagination,
          showTotal: (total)=>{return t`Total ` + total + t` Items`}
        }}
        bordered
        scroll={{ x: 1200 }}
        className={styles.table}
        columns={columns}
        simple
        rowKey={record => record.id}
      />
    )
  }
}

export default List
