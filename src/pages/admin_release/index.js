import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import AdminReleaseModal from "./components/Modal";
import Filter from "./components/Filter"


@connect(({ admin_release, loading }) => ({ admin_release, loading }))
class AdminRelease extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { admin_release, loading, location, dispatch } = this.props
    const { list, pagination } = admin_release
    const { query, pathname } = location
    return {
      options: admin_release.options,
      trees: admin_release.trees,
      optionValueLabel: admin_release.optionValueLabel,
      pagination,
      dataSource: list,
      loading: loading.effects['admin_release/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'admin_release/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'admin_release/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onApplyItem(item) {
        dispatch({
          type: 'admin_release/showModal',
          payload: {
            modalType: 'apply',
            currentItem: item,
          }
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'admin_release/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, admin_release, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = admin_release

    return {
      options: admin_release.options,
      trees: admin_release.trees,
      optionValueLabel: admin_release.optionValueLabel,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_release/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
          modalType === 'create' ? t`Create` : (modalType === 'update' ? t`Update` : t`Apply`)
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `admin_release/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_release/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch, admin_release} = this.props
    const {query,pathname} = location
    return {
      options: admin_release.options,
      trees: admin_release.trees,
      optionValueLabel: admin_release.optionValueLabel,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'admin_release/showModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <AdminReleaseModal {...this.modalProps} />
      </Page>
    )
  }
}

AdminRelease.propTypes = {
  admin_release: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default AdminRelease
