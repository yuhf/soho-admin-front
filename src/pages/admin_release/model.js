import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryAdminReleaseList, updateAdminRelease, createAdminRelease,deleteAdminRelease,queryUserOptions,queryPlatformTypeEnumOption } = api
/**
 *
   //相关api信息
 queryAdminReleaseList: `GET ${prefix}/admin/adminRelease/list`,
 updateAdminRelease: `PUT ${prefix}/admin/adminRelease`,
 createAdminRelease: `POST ${prefix}/admin/adminRelease`,
 deleteAdminRelease: `DELETE ${prefix}/admin/adminRelease/:ids`,
 queryAdminReleaseDetails: `GET ${prefix}/admin/adminRelease/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'admin_release',
  state: {
    options: {
      platformType: {
           '1': 'Windows',
           '2': 'Linux',
      },
    },
    optionValueLabel: {}, //value,label形式
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/admin_release').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
          dispatch({
            type: 'queryPlatformTypeValueLabel',
            payload: {}
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryAdminReleaseList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
    *queryPlatformTypeValueLabel({ payload }, { call, put }) {
      const data = yield call(queryPlatformTypeEnumOption, payload)
      if (data.success) {
        yield put({
          type: 'queryOptionValueLabel',
          payload: {
            platformType: data.payload,
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createAdminRelease, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateAdminRelease, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteAdminRelease, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
