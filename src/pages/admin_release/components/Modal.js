import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message,Checkbox,DatePicker,TreeSelect} from 'antd'
import TextArea from "antd/es/input/TextArea";
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import moment from 'moment';
import api from '../../../services/api'
import store from "store";
import QuillEditor from "../../../components/Editor/QuillEditor";
import {addRootNode} from "../../../utils/tree";
const {createUserAvatar, createAdminContentImage,createAdminReleaseFile} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class AdminReleaseModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        console.log(values)
        const data = {
          ...values,
          id: item.id,          //时间值处理
          url:((values.url?.fileList || values.url)?.map(value => {
          if(value.url) return value.url;
          if(value.response.payload) return value.response.payload;
         }) || []).join(";"),
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props
    const platformTypeOptionData = Object.keys(options.platformType).map((k,index)=>{
      return {value: parseInt(k), label: options.platformType[k]}
    })
    let initData = {
                    ...item};
    //初始化时间处理
    console.log(initData)
   initData['url'] = (initData.url?.split(';').map((item, key) => {
        return {
          uid: -key,
          key: item,
          name: item,
          url: item
        }
      }) || []);
   console.log("form 数据", initData)
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
         <FormItem  name='version' rules={[{ required: 0 }]}            label={t`Version`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={45} />
          </FormItem>
         <FormItem  name='notes' rules={[{ required: 0 }]}            label={t`Notes`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={1000} />
          </FormItem>
         <FormItem  name='url' rules={[{ required: 0 }]}             valuePropName={"defaultFileList"} label={t`Url`} hasFeedback {...formItemLayout}>
           <Upload listType="picture-card"
                   // defaultFileList={defaultFileList}
                   name="file"
                   action={createAdminReleaseFile}
                   disabled={false}
                   // onChange={this.handleChange}
                   headers={{Authorization:store.get('token')}}
                   maxCount={1}>
             <div>
               <PlusOutlined />
               <div style={{ marginTop: 8 }}>Upload</div>
             </div>
           </Upload>
          </FormItem>
         <FormItem  name='platformType' rules={[{ required: 0 }]}            label={t`Platform Type`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Platform Type`}
             optionFilterProp="children"
             options={platformTypeOptionData}
             disabled={false}
             />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

AdminReleaseModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default AdminReleaseModal
