import React, { PureComponent, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'umi'
import {Button, Row, Input, Form, Image, Col, Alert} from 'antd'
import { GlobalFooter } from 'components'
import { GithubOutlined } from '@ant-design/icons'
import { t, Trans } from "@lingui/macro"
import { setLocale } from 'utils'
import config from 'utils/config'
import styles from './index.less'
import api from "../../services/api";

@connect(({ loading, dispatch,demo }) => ({ loading, dispatch, demo }))
class Login extends PureComponent {

  render() {


    return (
      <>

        <div style={{margin:"auto", width:"1000px", height: "1000px"}}>
          <div className={styles.demoframe}>
            <iframe
              src={"https://nervjs.github.io/taro-ui-theme-preview/h5/index.html"}
              // src={"data:text/html,"+encodeURIComponent("<p>test by fang</p>")}
            >

            </iframe>

            <div className={styles.navbar}>
              <div className={styles.back}>
                <svg t="1548917697699" className="icon" style={{width:"20px", fill: "#333"}} viewBox="0 0 1024 1024" version="1.1">
                  <path
                    d="M348.8 512L742.4 118.4c12.8-12.8 12.8-32 0-44.8s-32-12.8-44.8 0l-416 416c-12.8 12.8-12.8 32 0 44.8l416 416c6.4 6.4 16 9.6 22.4 9.6s16-3.2 22.4-9.6c12.8-12.8 12.8-32 0-44.8L348.8 512z"
                    p-id="4990"></path>
                </svg>
              </div>
              <div className={styles.title}>Demo View</div>
            </div>

            <div className={styles.iphoneframe}></div>


          </div>
        </div>


      </>

    )
  }
}

Login.propTypes = {
  form: PropTypes.object,
  dispatch: PropTypes.func,
  loading: PropTypes.object,
  login: PropTypes.object,
}

export default Login
