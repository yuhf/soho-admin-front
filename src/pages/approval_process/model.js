import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
import {any, object} from "prop-types";
const { pathToRegexp } = require("path-to-regexp")
const { queryApprovalProcessList, updateApprovalProcess, createApprovalProcess,deleteApprovalProcess,queryUserOptions,queryApprovalProcessDetails } = api

export default modelExtend(pageModel, {
  namespace: 'approval_process',

  state: {
    currentItem: {},
    pageNum: 1,
    pageSize: 20,
    modalVisible: false,
    modalType: 'create',
    userOptions: {},
    rejectActions: {1:"不允许拒绝", 10: "结束审核", 20: "打回上一审批人"}
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/approval_process').exec(location.pathname)) {
          dispatch({
            type: 'queryUserOptions',
          })
          dispatch({
            type: 'query',
            payload: {
              status: 2,
              pageNum: 1,
              pageSize: 20,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *queryUserOptions({ payload }, { call, put }) {
      const data = yield call(queryUserOptions, payload)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            userOptions: data.payload,
          },
        })
      } else {
        throw data
      }
    },
    *query({ payload }, { call, put }) {
      const data = yield call(queryApprovalProcessList, payload)
      if (data.success) {
        let payload = data.payload
        yield put({
          type: 'querySuccess',
          payload: {
            list: payload.list,
            pagination: {
              pageNum: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.total,
            },
          },
        })
      } else {
        throw data
      }
    },
    *create({ payload }, { call, put }) {
      const data = yield call(createApprovalProcess, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      // const id = yield select(({ user }) => user.currentItem.id)
      const newUser = { ...payload }
      const data = yield call(updateApprovalProcess, newUser)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteApprovalProcess, {ids:payload})
      if (!data.success) {
        throw data
      }
    },

    //显示编辑框
    *onShowModal({payload},{select, call, put}) {
      const data = yield call(queryApprovalProcessDetails, {id:payload.currentItem.id})
      if(data.success) {
        yield put({type: 'showModal', payload:{modalType:payload.modalType, currentItem: data.payload}})
      }
    }

  },

  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
