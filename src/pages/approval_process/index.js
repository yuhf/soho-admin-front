import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import ApprovalProcessModal from "./components/Modal";
import Filter from "./components/Filter"

@connect(({ approval_process, loading }) => ({ approval_process, loading }))
class ApprovalProcess extends PureComponent {
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { approval_process, loading, location, dispatch } = this.props
    const { list, pagination,rejectActions } = approval_process
    const { query, pathname } = location
    return {
      pagination,
      dataSource: list,
      loading: loading.effects['approval_process/query'],
      rejectActions: rejectActions,
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'approval_process/delete',
          payload: id,
        }).then(() => {
          this.handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'approval_process/onShowModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        }).then(() => {
          this.handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, approval_process, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds,userOptions } = approval_process

    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`approval_process/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      userOptions: userOptions,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `approval_process/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleTabClick({
          })
        })
      },
      onCancel() {
        dispatch({
          type: 'approval_process/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch } = this.props
    const {query, pathname} = location
    return {
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'approval_process/showModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <ApprovalProcessModal {...this.modalProps} />
      </Page>
    )
  }
}

ApprovalProcess.propTypes = {
  approval_process: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default ApprovalProcess

