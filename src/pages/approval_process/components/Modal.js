import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message, Button, Space} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, MinusCircleOutlined, PlusOutlined} from "@ant-design/icons";
import ApprovalProcessDag from "../../../components/ApprovalProcessDag/ApprovalProcessDag";
import TextArea from "antd/es/input/TextArea";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  // labelCol: {
  //   span: 6,
  // },
  // wrapperCol: {
  //   span: 14,
  // },
}

class ApprovalProcessModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
    nodes: []
  };

  onNodeChange = (list) => {
    this.state.nodes = list
  }

  handleOk = () => {
    const { item = {}, onOk } = this.props
    console.log(item)
    this.formRef.current.validateFields()
      .then(values => {
        let data = {
          ...values,
          id: item.id,
          nodes: this.state.nodes
        }
        data['metadata'] = JSON.stringify(values.contentItems)
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form, userOptions, ...modalProps } = this.props
    let data = {...item}
    if(item !=null && item.metadata!=null) {
      data['contentItems'] = JSON.parse(item.metadata)
    }

    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1200}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...data }} layout="horizontal">
         <FormItem name='no' rules={[{ required: true }]}
            label={t`Identification Number`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='name' rules={[{ required: true }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='type' rules={[{ required: true }]}
            label={t`Approval Type`} hasFeedback {...formItemLayout}>
            <Select>
              <Option value={1}>普通审批</Option>
            </Select>
          </FormItem>
         <FormItem name='enable' rules={[{ required: true }]}
            label={t`Enable`} hasFeedback {...formItemLayout}>
            <Select>
              <Option value={true}>启用</Option>
              <Option value={false}>禁用</Option>
            </Select>
          </FormItem>

          <Form.List name="contentItems" rules={[{required: true}]} hasFeedback {...formItemLayout}>
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, ...restField }) => (

                  <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                    <Form.Item
                      {...restField}
                      name={[name, 'key']}
                      rules={[{ required: true, message: 'Missing key name' }]}
                    >
                      <Input placeholder="Please input field key name" />
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, 'title']}
                      rules={[{ required: true, message: 'Missing title' }]}
                    >
                      <Input placeholder="Please input field title" />
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, 'type']}
                      rules={[{required: true, message: t`Please select field type`}]}
                      style={{width: 150}}
                      >
                      <Select placeholder={t`Please select field type`}>
                        <Select.Option value={'textarea'}>文本域</Select.Option>
                        <Select.Option value={'input'}>文本输入</Select.Option>
                        <Select.Option value={'number'}>数字输入</Select.Option>
                        <Select.Option value={'editor'}>富文本输入</Select.Option>
                        {/*<Select.Option value={'select'}>选择输入</Select.Option>*/}
                        <Select.Option value={'datetime'}>时间选择</Select.Option>
                      </Select>
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, 'tips']}
                      rules={[{required: false}]} >
                      <Input placeholder={t`Please input tips`}/>
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(name)} />
                  </Space>
                ))}
                <Form.Item>
                  <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                    Add field
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
         <FormItem name='rejectAction' rules={[{ required: true }]}
            label={t`Reject Action`} hasFeedback {...formItemLayout}>
            <Select>
              <Option value={1}>不允许拒绝</Option>
              <Option value={10}>结束审核</Option>
              <Option value={20}>打回上一审批人</Option>
            </Select>
          </FormItem>
        </Form>
        <div>
          <ApprovalProcessDag nodes={item.nodes} onNodeChange={this.onNodeChange} userOptions={userOptions} width={800} />
        </div>
      </Modal>
    )
  }
}

ApprovalProcessModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default ApprovalProcessModal

