import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Cascader, TreeSelect} from 'antd'
import { Trans } from "@lingui/macro"
import city from 'utils/city'
import { t } from "@lingui/macro"

const FormItem = Form.Item

const { SHOW_PARENT } = TreeSelect;
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class UserModal extends PureComponent {
  formRef = React.createRef()
  resourceIds = this.props.resourceIds
  state = {
    value: this.resourceIds
  };

  onChange = value => {
    console.log(value);
    this.setState({ value });
  };

  handleOk = () => {
    const { item = {}, onOk } = this.props
    console.log(item)
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk,resourceTree,resourceIds, form, ...modalProps } = this.props
    console.log(resourceTree)
    let treeData = resourceTree == undefined ? [] : resourceTree;
    if(treeData.children != undefined) {
      treeData = treeData.children
    }
    const tProps = {
      treeData: treeData,
      value: this.state.value,
      // defaultValue: resourceIds,
      onChange: this.onChange,
      treeCheckable: true,
      showCheckedStrategy: SHOW_PARENT,
      placeholder: 'Please select',
      style: {
        width: '100%',
      },
    };

    return (
      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{...item, resourceIds }} layout="horizontal">
          <FormItem name='name' rules={[{ required: true }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          <FormItem name='remarks' rules={[{ required: true }]}
            label={t`Remarks`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
          <FormItem name='enable' rules={[{ required: true }]}
            label={t`Enable`} hasFeedback {...formItemLayout}>
            <Radio.Group>
              <Radio value={1}>
                {t`Yes`}
              </Radio>
              <Radio value={0}>
                {t`No`}
              </Radio>
            </Radio.Group>
          </FormItem>
          <FormItem name='resourceIds' rules={[{required: false }]}
                    label={t`Select role`} hasFeedback {...formItemLayout}>
            <TreeSelect {...tProps} />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

UserModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
  resourceTree: PropTypes.object
}

export default UserModal
