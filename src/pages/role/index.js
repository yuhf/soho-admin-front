import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'umi'
import { history } from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import Modal from "../role/components/Modal";
import Filter from "../role/components/Filter"

@connect(({ role, loading }) => ({ role, loading }))
class Role extends PureComponent {
  handleRefresh = () => {
    const { location } = this.props
    const { query, pathname } = location
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { role, loading, location, dispatch } = this.props
    const { list, pagination } = role
    const { query, pathname } = location
    return {
      pagination,
      dataSource: list,
      loading: loading.effects['role/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'role/delete',
          payload: id,
        }).then(() => {
          this.handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          // type: 'role/showModal',
          type: 'role/queryResourceTree',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, role, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = role

    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`role/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
        modalType === 'create' ? t`Create Role` : t`Update Role`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `role/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'role/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch } = this.props
    return {
      onAdd() {
        dispatch({
          type: 'role/queryResourceTree',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <Modal {...this.modalProps} />
      </Page>
    )
  }
}

Role.propTypes = {
  role: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default Role
