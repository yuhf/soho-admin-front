import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import AdminNotificationModal from "./components/Modal";
import Filter from "./components/Filter"

@connect(({ admin_notification, loading }) => ({ admin_notification, loading }))
class AdminNotification extends PureComponent {
  handleTabClick = payload => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        ...payload
      }),
    })
  }

  get listProps() {
    const { admin_notification, loading, location, dispatch } = this.props
    const { list, pagination } = admin_notification
    const { query, pathname } = location
    let handleTabClick = this.handleTabClick;
    return {
      pagination,
      dataSource: list,
      loading: loading.effects['admin_notification/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'admin_notification/delete',
          payload: id,
        }).then(() => {
          handleTabClick({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'admin_notification/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
    }
  }

  get modalProps() {
    const { dispatch, admin_notification, loading } = this.props
    const { userOptions, currentItem, modalVisible, modalType } = admin_notification
    return {
      item: modalType === 'create' ? {} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`admin_notification/${modalType}`],
      userOptions: userOptions,
      title: `${
         modalType === 'create' ? t`Create` : t`Update`
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `admin_notification/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'admin_notification/hideModal',
        })
      },
    }
  }

  get filterProps() {
    const { location, dispatch } = this.props
    const {query} = location
    return {
      filter: {
        ...query,
      },
      onAdd() {
        console.log("我要创建消息")
        dispatch({
          type: 'admin_notification/showModal',
          payload: {
            modalType: 'create',
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>
        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <AdminNotificationModal {...this.modalProps} />
      </Page>
    )
  }
}

AdminNotification.propTypes = {
  admin_notification: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default AdminNotification

