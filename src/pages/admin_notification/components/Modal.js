import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import TextArea from "antd/es/input/TextArea";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class AdminNotificationModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          key: item.key,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {},userOptions = [], onOk, form, ...modalProps } = this.props
      //获取用户下拉框数据
    const children = [];
    for (let i = 0; i < userOptions.length; i++) {
      children.push(<Option key={userOptions[i].id}>{userOptions[i].username}</Option>);
    }

    return (
      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

         <FormItem name='title' rules={[{ required: true }]}
            label={t`title`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>

         <FormItem name='content' rules={[{ required: true }]}
            label={t`content`} hasFeedback {...formItemLayout}>
            <TextArea />
          </FormItem>
         <FormItem name='adminUserIds' rules={[{ required: true }]}
            label={t`Admin user`} hasFeedback {...formItemLayout}>
           <Select
             mode="multiple"
             allowClear
             style={{ width: '100%' }}
             placeholder="Please select"
             defaultValue={[]}
             // onChange={handleChange}
           >
             {children}
           </Select>
          </FormItem>

        </Form>
      </Modal>
    )
  }
}

AdminNotificationModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default AdminNotificationModal

