import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryLotModelList, updateLotModel, createLotModel,deleteLotModel,queryLotModelDetails } = api

export default modelExtend(pageModel, {
  namespace: 'lot_model',

  state: {
    currentItem: {}
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/lot_model').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              pageNum: 1,
              status: 2,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryLotModelList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.pageNum) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.total,
            },
          },
        })
      } else {
        throw data
      }
    },
    //获取当前编辑详情
   *queryDetails({ payload }, { call, put }) {
      const data = yield call(queryLotModelDetails, {id:payload.currentItem.id})
      if (data.success) {
        yield put({payload: {currentItem:data.payload, modalType: payload.modalType}, type: 'showModal' })
      } else {
        throw data
      }
    },

   *create({ payload }, { call, put }) {
      const data = yield call(createLotModel, payload)
     console.log(data)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      // const id = yield select(({ user }) => user.currentItem.id)
      const newUser = { ...payload }
      const data = yield call(updateLotModel, newUser)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteLotModel, {ids:payload})
      if (!data.success) {
        throw data
      }
    }
  },
  reducers: {
    showModal(state, { payload }) {
      console.log(payload)
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
