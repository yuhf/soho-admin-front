import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message, Space, Button} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, MinusCircleOutlined, PlusOutlined} from "@ant-design/icons";
import TextArea from "antd/es/input/TextArea";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class LotModelModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    console.log(item);
    this.formRef.current.validateFields()
      .then(values => {
        console.log(values)
        const data = {
          id: item.id,
          ...values,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form, ...modalProps } = this.props
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1200}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

         {/*<FormItem name='id' rules={[{ required: true }]}*/}
         {/*   label={t`id`} hasFeedback {...formItemLayout}>*/}
         {/*   <Input />*/}
         {/* </FormItem>*/}
         <FormItem name='name' rules={[{ required: true }]}
            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='supplierId' rules={[{ required: true }]}
            label={t`Supplier Id`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>



          <Form.List name="itemList" rules={[{required: true}]} hasFeedback {...formItemLayout}>
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, ...restField }) => (

                  <Space key={key} style={{ display: 'flex', marginBottom: 8 }} align="baseline">
                    <Form.Item
                      {...restField}
                      name={[name, 'paramsName']}
                      rules={[{ required: true, message: t`Please input field key name` }]}
                    >
                      <Input placeholder={t`Please input field key name`} />
                    </Form.Item>

                    <Form.Item
                      {...restField}
                      name={[name, 'title']}
                      rules={[{ required: true, message: t`'Please input field title` }]}
                    >
                      <Input placeholder={t`Please input field title`} />
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, 'unit']}
                      rules={[{ required: true, message: t`'Please input field unit` }]}
                    >
                      <Input placeholder={t`Please input field unit`} />
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, 'extendedData']}
                      rules={[{ required: true, message: t`'Please input field extended data` }]}
                    >
                      <TextArea placeholder={t`Please input field extended data`} />
                    </Form.Item>

                    <Form.Item
                      {...restField}
                      name={[name, 'type']}
                      rules={[{required: true, message: t`Please select field type`}]}
                    >
                      <Select placeholder={t`Please select field type`}>
                        <Select.Option value={'bit'}>二进制</Select.Option>
                        <Select.Option value={'string'}>字符串</Select.Option>
                        <Select.Option value={'number'}>数字</Select.Option>
                        <Select.Option value={'base64'}>base64</Select.Option>
                      </Select>
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, 'tips']}
                      rules={[{required: false}]} >
                      <Input placeholder={t`Please input tips`}/>
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(name)} />
                  </Space>
                ))}
                <Form.Item>
                  <Button type="dashed" onClick={() => add()} block icon={<PlusOutlined />}>
                    Add field
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>


        </Form>
      </Modal>
    )
  }
}

LotModelModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default LotModelModal

