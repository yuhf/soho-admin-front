import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  Form,
  Input,
  Modal,
  Select,
  Alert, Divider
} from 'antd'
import {t, Trans} from "@lingui/macro"
import api from '../../../services/api'
import CodeEditor from "../../../components/CodeEditor/CodeEditor";
import {Button} from "_antd@4.24.12@antd";
const { confirm } = Modal
const {createUserAvatar, createAdminContentImage} = api

const { Option } = Select;
const FormItem = Form.Item
//时间格式化
const datetimeFormat = "YYYY-MM-DD HH:mm:ss"
const dateFormat = "YYYY-MM-DD"
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class GroovyInfoModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
    testResult: null, //测试结果
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  handleTestRun = () => {
    const { item = {}, onOk, onTest } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          id: item.id,          //时间值处理
        }
        console.log("testRun", data)
        onTest(data, (d)=>{
          console.log("ooooooooooooooooooooooooookkkkkkkkkkkkkkkkkkkkkkkkk")
          console.log(d)
          this.setState({...this.state, testResult: d});
        })
        //onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  handleCancel = () => {
    const { onCancel } = this.props
    confirm({
      title: t`Are you sure close this editor`,
      onOk() {
        onCancel();
      }
    })
  }

  render() {
    const { item = {}, onOk, form,options,trees, ...modalProps } = this.props
    const groupIdOptionData = Object.keys(options.groupId).map((k,index)=>{
      return {value: parseInt(k), label: options.groupId[k]}
    })
    let initData = {
                    ...item};
    //初始化时间处理
    return (

      <Modal {...modalProps} onOk={this.handleOk} width={1300}
             footer={[
               <Button key='cancel' onClick={(event) => this.handleCancel()} >取消</Button>,
               <Button key='continue' onClick={this.handleTestRun}>测试运行</Button>,
               <Button key='confirm' type="primary" onClick={this.handleOk}>确定</Button>
             ]}

      >
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...initData }} layout="horizontal">
         <FormItem  name='groupId' rules={[{ required: 0 }]}            label={t`Group Id`} hasFeedback {...formItemLayout}>
           <Select
             showSearch
             placeholder={t`Group`}
             optionFilterProp="children"
             options={groupIdOptionData}
             disabled={false}
             />
          </FormItem>
         <FormItem  name='name' rules={[{ required: 0 }]}            label={t`Name`} hasFeedback {...formItemLayout}>
            <Input             disabled={false}
 maxLength={45} />
          </FormItem>
         <FormItem  name='code' rules={[{ required: 0 }]}            label={t`Code`} hasFeedback {...formItemLayout}>
           <CodeEditor width={"900px"}/>
          </FormItem>

         <FormItem  name='testCode' rules={[{ required: 0 }]} tooltip={{title:"可以再这里写测试代码; 这部分代码会拼接到上面的主体代码进行执行，但是并不会进行保存"}}            label={t`Test Code`} hasFeedback {...formItemLayout}>
           <CodeEditor height={60} />
          </FormItem>

          {this.state.testResult && <Divider>测试执行结果</Divider>}
          {this.state.testResult && <Divider orientation={"left"}>标准输出</Divider>}
          {this.state.testResult && this.state.testResult.output && <Alert description={<pre>{this.state.testResult.output}</pre>} type={"info"} /> }
          {this.state.testResult && <Divider orientation={"left"}>调用返回结果</Divider>}
          {this.state.testResult && this.state.testResult.status == "success" && <Alert
            message={t`Success Tips`}
            description={JSON.stringify(this.state.testResult.result)}
            type="success"
            showIcon
          />}
          {this.state.testResult && this.state.testResult.status == "exception" && <Alert
            message={t`Error`}
            description={this.state.testResult.result}
            type="error"
            showIcon
          />}

        </Form>
      </Modal>
    )
  }
}

GroovyInfoModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default GroovyInfoModal
