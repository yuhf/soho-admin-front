import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryGroovyInfoList, updateGroovyInfo, createGroovyInfo,deleteGroovyInfo,queryGroovyGroupOptions,testGroovyInfo } = api
/**
 *
   //相关api信息
 queryGroovyInfoList: `GET ${prefix}/admin/groovyInfo/list`,
 updateGroovyInfo: `PUT ${prefix}/admin/groovyInfo`,
 createGroovyInfo: `POST ${prefix}/admin/groovyInfo`,
 deleteGroovyInfo: `DELETE ${prefix}/admin/groovyInfo/:ids`,
 queryGroovyInfoDetails: `GET ${prefix}/admin/groovyInfo/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'groovy_info',
  state: {
    options: {
      groupId: {
      },
    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/groovy_info').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
          dispatch({
            type: 'queryGroovyGroupOptions',
            payload: {},
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryGroovyInfoList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createGroovyInfo, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
   *test({ payload }, { call, put }) {
      const data = yield call(testGroovyInfo, payload)
      if (data.success) {
        return data;
      } else {
        throw data
      }
    },
    *update({ payload }, { select, call, put }) {
      const data = yield call(updateGroovyInfo, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteGroovyInfo, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
    *queryGroovyGroupOptions({ payload }, { call, put }) {
      const data = yield call(queryGroovyGroupOptions, payload)
      if (data.success) {
        yield put({
          type: 'queryOptions',
          payload: {
              groupId: data.payload
          },
        })
      } else {
        throw data
      }
    },  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
