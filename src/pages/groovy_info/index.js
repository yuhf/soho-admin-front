import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {connect, history} from 'umi'
import { stringify } from 'qs'
import { t } from "@lingui/macro"
import { Page } from 'components'
import List from './components/List'
import GroovyInfoModal from "./components/Modal";
import Filter from "./components/Filter"
import {message} from 'antd'

@connect(({ groovy_info, loading }) => ({ groovy_info, loading }))
class GroovyInfo extends PureComponent {
  handleRefresh = newQuery => {
    const { location } = this.props
    const { query, pathname } = location
    if(newQuery && newQuery.createTime) {
      newQuery.startDate = newQuery.createTime[0]
      newQuery.endDate = newQuery.createTime[1]
      delete newQuery.createTime
    }
    history.push({
      pathname,
      search: stringify(
        {
          ...query,
          ...newQuery,
        },
        { arrayFormat: 'repeat' }
      ),
    })
  }
  handleTabClick = key => {
    const { pathname } = this.props.location

    history.push({
      pathname,
      search: stringify({
        status: key,
      }),
    })
  }

  get listProps() {
    const { groovy_info, loading, location, dispatch } = this.props
    const { list, pagination } = groovy_info
    const { query, pathname } = location
    return {
      options: groovy_info.options,
      trees: groovy_info.trees,
      pagination,
      dataSource: list,
      loading: loading.effects['groovy_info/query'],
      onChange(page) {
        history.push({
          pathname,
          search: stringify({
            ...query,
            page: page.current,
            pageSize: page.pageSize,
          }),
        })
      },
      onDeleteItem: id => {
        dispatch({
          type: 'groovy_info/delete',
          payload: id,
        }).then(() => {
          this.handleRefresh({
            page:
              list.length === 1 && pagination.current > 1
                ? pagination.current - 1
                : pagination.current,
          })
        })
      },
      onEditItem(item) {
        dispatch({
          type: 'groovy_info/showModal',
          payload: {
            modalType: 'update',
            currentItem: item,
          },
        })
      },
      onApplyItem(item) {
        dispatch({
          type: 'groovy_info/showModal',
          payload: {
            modalType: 'apply',
            currentItem: item,
          }
        })
      },
      onChildItem(item) {
        dispatch({
          type: 'groovy_info/showModal',
          payload: {
            modalType: 'create',
            currentItem: {...item},
          },
        })
      },    }
  }

  get modalProps() {
    const { dispatch, groovy_info, loading } = this.props
    const { currentItem, modalVisible, modalType,resourceTree,resourceIds } = groovy_info

    return {
      options: groovy_info.options,
      trees: groovy_info.trees,
      item: modalType === 'create' ? {...currentItem} : currentItem,
      visible: modalVisible,
      destroyOnClose: true,
      maskClosable: false,
      confirmLoading: loading.effects[`groovy_info/${modalType}`],
      resourceTree: resourceTree,
      resourceIds: resourceIds,
      title: `${
        modalType === 'create' ? t`Create` : (modalType === 'update' ? t`Update` : t`Apply`)
      }`,
      centered: true,
      onOk: data => {
        dispatch({
          type: `groovy_info/${modalType}`,
          payload: data,
        }).then(() => {
          this.handleRefresh()
        })
      },
      onCancel() {
        dispatch({
          type: 'groovy_info/hideModal',
        })
      },
      //调用服务器端进行代码测试
      onTest(payload, callback) {
        dispatch({
          type: 'groovy_info/test',
          payload: payload,
        }).then(data => {
          console.log(data);
          // eslint-disable-next-line no-undef
          message.info(data.payload);
          callback(data.payload)
        });
      }
    }
  }

  get filterProps() {
    const { location, dispatch, groovy_info} = this.props
    const {query,pathname} = location
    return {
      options: groovy_info.options,
      trees: groovy_info.trees,
      filter: {
        ...query,
      },
      onFilterChange(values) {
        history.push({
          pathname,
          search: stringify(values),
        })
      },
      onAdd() {
        dispatch({
          type: 'groovy_info/showModal',
          payload: {
            modalType: 'create',
            currentItem: {},
          },
        })
      },
    }
  }

  render() {
    return (
      <Page inner>

        <Filter {...this.filterProps} />
        <List {...this.listProps} />
        <GroovyInfoModal {...this.modalProps} />
      </Page>
    )
  }
}

GroovyInfo.propTypes = {
  groovy_info: PropTypes.object,
  loading: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
}

export default GroovyInfo
