import modelExtend from 'dva-model-extend'
const { pathToRegexp } = require("path-to-regexp")
import api from 'api'
import { pageModel } from 'utils/model'

const {
  queryUserList,
  createUser,
  removeUser,
  updateUser,
  removeUserList,
  queryUserDetails,
  queryRoleOptions,
} = api

export default modelExtend(pageModel, {
  namespace: 'admin_user',

  state: {
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    selectedRowKeys: [],
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/admin_user').exec(location.pathname)) {
          let payload = location.query || { page: 1, pageSize: 10 }
          //查询角色信息
          dispatch({
            type: 'queryRoleOptions',
            payload
          })
          //查询用户数据
          dispatch({
            type: 'query',
            payload,
          })
        }
      })
    },
  },

  effects: {
    *query({ payload = {} }, { call, put }) {
      const data = yield call(queryUserList, payload)
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 20,
              total: data.payload.total,
            },
          },
        })
      }
    },

    //角色信息
    *queryRoleOptions({payload}, {call, put}) {
      const roleOptions = yield  call(queryRoleOptions)
      if(roleOptions.success) {
        yield put({
          type: 'updateState',
          payload: {
            roleOptions:roleOptions.payload
          },
        })
      }
    },

    //查询用户详细信息
    *queryUserDetails({payload},{call, put}) {
      //id payload.currentItem.id
      if(payload.currentItem.id) {
        const data = yield call(queryUserDetails, {id:payload.currentItem.id})
        payload.currentItem = data.payload;
      }
      yield put({ type: 'showModal', payload: payload })
    },

    *delete({ payload }, { call, put, select }) {

      const data = yield call(removeUser, { id: payload })
      const { selectedRowKeys } = yield select(_ => _.user)
      if (data.success) {
        yield put({
          type: 'updateState',
          payload: {
            selectedRowKeys: selectedRowKeys.filter(_ => _ !== payload),
          },
        })
      } else {
        throw data
      }
    },

    *multiDelete({ payload }, { call, put }) {
      const data = yield call(removeUserList, payload)
      if (data.success) {
        yield put({ type: 'updateState', payload: { selectedRowKeys: [] } })
      } else {
        throw data
      }
    },

    *create({ payload }, { call, put }) {
      const data = yield call(createUser, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const id = yield select(({ admin_user }) => admin_user.currentItem.id)
      const newUser = { ...payload, id }
      const data = yield call(updateUser, newUser)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
  },

  reducers: {
    showModal(state, { payload }) {
      console.log(payload)
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})
