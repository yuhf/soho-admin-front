import modelExtend from 'dva-model-extend'
import api from 'api'
import { pageModel } from 'utils/model'
const { pathToRegexp } = require("path-to-regexp")
const { queryAdminEmailTemplateList, updateAdminEmailTemplate, createAdminEmailTemplate,deleteAdminEmailTemplate } = api
/**
 *
   //相关api信息
 queryAdminEmailTemplateList: `GET ${prefix}/admin/adminEmailTemplate/list`,
 updateAdminEmailTemplate: `PUT ${prefix}/admin/adminEmailTemplate`,
 createAdminEmailTemplate: `POST ${prefix}/admin/adminEmailTemplate`,
 deleteAdminEmailTemplate: `DELETE ${prefix}/admin/adminEmailTemplate/:ids`,
 queryAdminEmailTemplateDetails: `GET ${prefix}/admin/adminEmailTemplate/:id`,
 *
 */
export default modelExtend(pageModel, {
  namespace: 'admin_email_template',
  state: {
    options: {    },
    trees: {
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (pathToRegexp('/admin_email_template').exec(location.pathname)) {
          dispatch({
            type: 'query',
            payload: {
              pageSize: 20,
              page: 1,
              ...location.query,
            },
          })
        }
      })
    },
  },

  effects: {
    *query({ payload }, { call, put }) {
      const data = yield call(queryAdminEmailTemplateList, payload)
      if (data.success) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.payload.list,
            pagination: {
              current: Number(payload.page) || 1,
              pageSize: Number(payload.pageSize) || 10,
              total: data.payload.total,
            },
          },
        })
      } else {
        throw data
      }
    },
   *create({ payload }, { call, put }) {
      const data = yield call(createAdminEmailTemplate, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },

    *update({ payload }, { select, call, put }) {
      const data = yield call(updateAdminEmailTemplate, payload)
      if (data.success) {
        yield put({ type: 'hideModal' })
      } else {
        throw data
      }
    },
    *delete({ payload }, { select, call, put }) {
      const data = yield call(deleteAdminEmailTemplate, {ids:payload})
      if (!data.success) {
        throw data
      }
    },
  },
  reducers: {
    showModal(state, { payload }) {
      return { ...state, ...payload, modalVisible: true }
    },

    hideModal(state) {
      return { ...state, modalVisible: false }
    },
  },
})