import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {Form, Input, InputNumber, Radio, Modal, Select, Upload, message} from 'antd'
import {t, Trans} from "@lingui/macro"
import ImgCrop from "antd-img-crop";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";

const { Option } = Select;
const FormItem = Form.Item

const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

class LotModelItemModal extends PureComponent {
  formRef = React.createRef()

  state = {
    value: [],
    loading: false, //上传 loading
  };


  handleOk = () => {
    const { item = {}, onOk } = this.props
    this.formRef.current.validateFields()
      .then(values => {
        const data = {
          ...values,
          key: item.key,
        }
        onOk(data)
      })
      .catch(errorInfo => {
        console.log(errorInfo)
      })
  }

  render() {
    const { item = {}, onOk, form, ...modalProps } = this.props
    return (

      <Modal {...modalProps} onOk={this.handleOk}>
        <Form ref={this.formRef} name="control-ref" initialValues={{ ...item }} layout="horizontal">

         <FormItem name='id' rules={[{ required: true }]}
            label={t`id`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='modelId' rules={[{ required: true }]}
            label={t`modelId`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='title' rules={[{ required: true }]}
            label={t`title`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='unit' rules={[{ required: true }]}
            label={t`unit`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='paramsName' rules={[{ required: true }]}
            label={t`paramsName`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='tips' rules={[{ required: true }]}
            label={t`tips`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='pin' rules={[{ required: true }]}
            label={t`pin`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='type' rules={[{ required: true }]}
            label={t`type`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='updatedTime' rules={[{ required: true }]}
            label={t`updatedTime`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
         <FormItem name='createdTime' rules={[{ required: true }]}
            label={t`createdTime`} hasFeedback {...formItemLayout}>
            <Input />
          </FormItem>
        </Form>
      </Modal>
    )
  }
}

LotModelItemModal.propTypes = {
  type: PropTypes.string,
  item: PropTypes.object,
  onOk: PropTypes.func,
}

export default LotModelItemModal

