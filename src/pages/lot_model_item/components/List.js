import React, { PureComponent } from 'react'
import {Table, Avatar, Modal} from 'antd'
import {t, Trans} from "@lingui/macro"
import { Ellipsis } from 'components'
import {DropOption} from "../../../components";
import PropTypes from "prop-types";
const { confirm } = Modal

class List extends PureComponent {
  handleMenuClick = (record, e) => {
    const { onDeleteItem, onEditItem } = this.props

    if (e.key === '1') {
      onEditItem(record)
    } else if (e.key === '2') {
      confirm({
        title: t`Are you sure delete this record?`,
        onOk() {
          onDeleteItem(record.id)
        },
      })
    }
  }

  render() {
    const { ...tableProps } = this.props
    const columns = [

      {
        title: t`Id`,
        dataIndex: 'id',
      },
      {
        title: t`Model Id`,
        dataIndex: 'modelId',
      },
      {
        title: t`Title`,
        dataIndex: 'title',
      },
      {
        title: t`Unit`,
        dataIndex: 'unit',
      },
      {
        title: t`Params Name`,
        dataIndex: 'paramsName',
      },
      {
        title: t`Tips`,
        dataIndex: 'tips',
      },
      {
        title: t`Pin`,
        dataIndex: 'pin',
      },
      {
        title: t`Type`,
        dataIndex: 'type',
      },
      {
        title: t`Updated Time`,
        dataIndex: 'updatedTime',
      },
      {
        title: t`Created Time`,
        dataIndex: 'createdTime',
      },
      {
        title: <Trans>Operation</Trans>,
        key: 'operation',
        fixed: 'right',
        width: '8%',
        render: (text, record) => {
          return (
            <DropOption
              onMenuClick={e => this.handleMenuClick(record, e)}
              menuOptions={[
                { key: '1', name: t`Update` },
                { key: '2', name: t`Delete` },
              ]}
            />
          )
        },
      },
    ]

    return (
      <Table
        {...tableProps}
        pagination={{
          ...tableProps.pagination,
          showTotal: total => t`Total ${total} Items`,
        }}
        bordered
        scroll={{ x: 1200 }}
        columns={columns}
        simple
        rowKey={record => record.id}
      />
    )
  }
}

List.propTypes = {
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  location: PropTypes.object,
}

export default List

