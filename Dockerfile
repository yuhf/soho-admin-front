FROM nginx
COPY ./docs/nginx/soho-nginx.conf /etc/nginx/conf.d/0-soho-nginx.conf
COPY ./dist/ /usr/share/nginx/html/
